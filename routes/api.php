<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'],function(){
	// forgot password endpoint
	Route::post('forgot_password',['uses'=>'Controller@forgot_password']);
	Route::post('test/email/send',['uses'=>'AdminController@test_send_email']);
	// reset password endpoint
	Route::post('reset_password',['uses'=>'Controller@reset_password']);
	Route::get('state',['uses'=>'Controller@stateGen']);
	Route::get('lgas/{state}',['uses'=>'Controller@lgaGen']);

	// test endpoints
	Route::group(['prefix'=>'test'],function(){
		Route::get('generate-application_no',['uses'=>'Controller@generate_application_no']);
	});

	// application number generator
	Route::get('generate-application_no',['uses'=>'Controller@generate_application_no']);
	// customer portal endpoints
	Route::group(['prefix'=>'customer'],function(){
		Route::post('login',['uses'=>'Controller@auth']);
		Route::post('signup',['uses'=>'CustomerController@signup']);
		Route::group(['middleware'=>['jwt.auth']],function(){
			Route::get('dashboard',['uses'=>'CustomerController@dashboard']);
			Route::get('property/details/{id}',['uses'=>'CustomerController@viewPropertyPics']);
			Route::post('properties/indicate_interest',['uses'=>'CustomerController@indicate_interest']);
			// payment endpoints 
			
			Route::group(['prefix'=>'payments'],function(){
				// tweaked => Route::post('pay',['uses'=>'CustomerController@pay']); to:
				
				Route::get('/datagrabber',['uses'=>'PaymentController@dataGrabber']);
				Route::post('pay',['uses'=>'PaymentController@redirectToGateway']);
				Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');
				Route::post('upgrade',['uses'=>'CustomerController@upgrade']);
				Route::get('history',['uses'=>'CustomerController@history']);
				Route::post('history-interval',['uses'=>'CustomerController@history_interval']);

			});
			// notifications endpoints
			Route::group(['prefix'=>'notifications'],function(){
				Route::get('interest',['uses'=>'CustomerController@interest']);
				Route::get('approved',['uses'=>'CustomerController@approved']);
			});
			Route::post('feedback',['uses'=>'CustomerController@feedback']);
			Route::post('test',['uses'=>'CustomerController@test']);	//testing getting user info from token
		});
	});

	// admin portal endpoints
	Route::group(['prefix'=>'admin'],function(){
		Route::post('login',['uses'=>'Controller@auth']);
		Route::post('jump',['uses'=>'Controller@jumpUser']);
		Route::post('user/create',['uses'=>'Controller@registerUser']);
		Route::post('user/update',['uses'=>'Controller@updateUser']);
		
		// middleware for admin endpoints
		Route::group(['middleware'=>['jwt.auth']],function(){
			//admins endpoint
			Route::group(['prefix'=>'admins'],function(){
				Route::post('add_admin',['uses'=>'AdminController@add_admin']);
				Route::get('delete/{id}',['uses'=>'AdminController@delete_admin']);
				Route::get('view/all',['uses'=>'AdminController@view_admins']);
				Route::get('edit/{id}',['uses'=>'AdminController@edit_admin']);	 // should this be a part of the API ? YES it should.
				Route::post('update/{id}',['uses'=>'AdminController@update_admin']);
			});
			//analytics endpoint
			Route::get('analytics',['uses'=>'AdminController@analytics']);
			Route::get('replied/feedback',['uses'=>'AdminController@replied_feedback']);
			//customers endpoint
			Route::group(['prefix'=>'customers'],function(){
				Route::get('view/all',['uses'=>'AdminController@view_customers']);
				Route::post('add',['uses'=>'AdminController@add_customer']); 
				Route::get('delete/{id}',['uses'=>'AdminController@delete_customer']);
				Route::get('edit/{id}',['uses'=>'AdminController@edit_customer']); // should this be a part of the API ? YES it should.
				Route::post('update/{id}',['uses'=>'AdminController@update_customer']);
			});
			//payments endpoint
			Route::group(['prefix'=>'payments'],function(){
				Route::get('property/{id}',['uses'=>'AdminController@interested_users']);
				Route::get('view/all',['uses'=>'AdminController@view_all_payments']);
				Route::get('view/filter',['uses'=>'AdminController@getPaymentTableFilter']);
				Route::post('create_transaction_record',['uses'=>'AdminController@add_transaction']);
				//Route::post('eligibility/{property_id}/{user_id}',['uses'=>'AdminController@eligibility_check']);
				
				
			});

			//properties endpoint
			Route::group(['prefix'=>'properties'],function(){
				Route::post('/property/add',['uses'=>'AdminController@add_property']);
				Route::get('/view/all',['uses'=>'AdminController@view_properties']);
				Route::get('/property/edit/{id}',['uses'=>'AdminController@edit_property']);
				Route::post('/property/update/{id}',['uses'=>'AdminController@update_property']);
				Route::get('/property/delete/{id}',['uses'=>'AdminController@delete_property']);
				Route::get('/property_pic/delete/{id}',['uses'=>'AdminController@delete_property_pic']);
			});

			//Interest rate endpoint
			Route::group(['prefix'=>'interest_rate'],function(){
				Route::post('set',['uses'=>'AdminController@set_interest_rate']);
			});




			//Feedback endpoint
			Route::group(['prefix'=>'feedback'],function(){
				Route::post('send_email',['uses'=>'AdminController@sendemail']);
				Route::get('outbox',['uses'=>'AdminController@outbox']);
				Route::get('inbox',['uses'=>'AdminController@inbox']);
				Route::get('read',['uses'=>'AdminController@read_feedback']);
				Route::get('unread',['uses'=>'AdminController@unread_feedback']);
				Route::post('replied',['uses'=>'AdminController@replied_feedback']);
			});
			Route::group(['prefix'=>'upgrade/request'],function(){
				Route::post('send_email',['uses'=>'AdminController@sendemail']);
				Route::get('new',['uses'=>'AdminController@new_upgrade_requests']);
				Route::get('all',['uses'=>'AdminController@upgrade_requests']);
				Route::get('approved',['uses'=>'AdminController@upgraded_requests']);
				Route::post('approve',['uses'=>'AdminController@approve_upgrade_request']);
			});
			Route::group(['prefix'=>'info'],function(){
				Route::get('user/{id}',['uses'=>'AdminController@userInfo']);
				Route::get('plan/{id}',['uses'=>'AdminController@planInfo']);
				
			});
			// Upgrade Request endpoints
			Route::group(['prefix'=>'upgrade/request'],function(){
				Route::post('send_email',['uses'=>'AdminController@sendemail']);
				Route::get('new',['uses'=>'AdminController@new_upgrade_requests']);
				Route::get('all',['uses'=>'AdminController@upgrade_requests']);
				Route::get('approved',['uses'=>'AdminController@upgraded_requests']);
				Route::post('approve',['uses'=>'AdminController@approve_upgrade_request']);
			});

			//SMS endpoint
			Route::group(['prefix'=>'sms'],function(){
				Route::post('send',['uses'=>'AdminController@sendsms']);
				Route::get('outbox',['uses'=>'AdminController@outbox_sms']);
				Route::post('bulk',['uses'=>'AdminController@sendBulkSms']);
			});
			//SMS endpoint
			Route::group(['prefix'=>'email'],function(){
				Route::post('send',['uses'=>'AdminController@sendemail']);
				Route::post('bulk',['uses'=>'AdminController@sendBulkEmail']);
			});

			Route::post('filter/date/payment','AdminController@getDateFilterPayment');
		});
		
		
	});

		Route::get('test/','AdminController@sendBulkSms');

});