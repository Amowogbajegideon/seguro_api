-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2017 at 11:06 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seguro`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employment_information`
--

CREATE TABLE `employment_information` (
  `id` int(10) UNSIGNED NOT NULL,
  `employment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_employer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `employment_information`
--

INSERT INTO `employment_information` (`id`, `employment_status`, `name_of_employer`, `designation`, `address`, `created_at`, `updated_at`, `user_id`) VALUES
(2, 'unemployed', 'School', 'Student', '194, Herbet Macaulay way, Makoko, Yaba, Lagos.', '2017-03-25 01:08:45', '2017-03-25 01:08:45', 3),
(7, 'unemployed', 'School', 'Student', '194, Herbet Macaulay way, Makoko, Yaba, Lagos.', '2017-03-26 02:05:41', '2017-03-26 02:05:41', 17),
(8, 'employed', 'iqube', 'software engineer', 'makoko yaba', '2017-04-04 10:14:06', '2017-04-04 10:14:06', 39),
(9, 'employed', 'iqube', 'software engineer', 'makoko yaba', '2017-04-04 10:22:18', '2017-04-04 10:22:18', 57),
(10, 'employed', 'iqube', 'software engineer', 'makoko yaba', '2017-04-05 09:30:51', '2017-04-05 09:30:51', 60),
(11, 'unemployed', 'School', 'Student', '194, Herbet Macaulay way, Makoko, Yaba, Lagos.', '2017-04-06 05:54:03', '2017-04-06 05:54:03', 64);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `interested_users`
--

CREATE TABLE `interested_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `property_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interested_users`
--

INSERT INTO `interested_users` (`id`, `created_at`, `updated_at`, `user_id`, `property_id`) VALUES
(1, '2017-03-27 08:39:45', '2017-03-27 08:39:45', 17, 1),
(2, '2017-03-27 09:11:46', '2017-03-27 09:11:46', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2014_10_12_000000_create_users_table', 1),
(20, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2017_03_16_211833_create_admins_table', 1),
(22, '2017_03_16_211931_create_properties_table', 1),
(23, '2017_03_16_212032_create_payment_plans_table', 1),
(24, '2017_03_16_213107_create_transactions_table', 1),
(25, '2017_03_16_214253_create_feedback_table', 1),
(26, '2017_03_16_220346_create_property_pictures_table', 1),
(27, '2017_03_16_220806_create_sent_emails_table', 1),
(28, '2017_03_16_221332_create_employment_information_table', 1),
(29, '2017_03_16_221614_create_interested_users_table', 1),
(30, '2017_03_16_221905_create_upgrade_requests_table', 1),
(31, '2017_03_16_223159_create_user_payment_plan_table', 1),
(32, '2017_03_17_085601_add_status_to_upgrade_requests_table', 1),
(33, '2017_03_22_143250_create_savings_table', 1),
(34, '2017_03_23_200155_create_notifications_table', 1),
(35, '2017_03_24_235753_create_roles_table', 1),
(36, '2017_03_25_001520_create_role_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `sms` text COLLATE utf8mb4_unicode_ci,
  `dashboard` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sent_email_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_plans`
--

CREATE TABLE `payment_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_plan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_plans`
--

INSERT INTO `payment_plans` (`id`, `payment_plan`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'SEG10', 10000, '2017-03-17 12:24:55', '2017-03-17 12:24:55'),
(2, 'SEG20', 20000, '2017-03-17 12:24:55', '2017-03-17 12:24:55'),
(3, 'SEG30', 30000, '2017-03-17 12:25:50', '2017-03-17 12:25:50'),
(4, 'SEG40', 40000, '2017-03-17 12:25:50', '2017-03-17 12:25:50'),
(5, 'SEG50', 50000, '2017-03-17 12:26:26', '2017-03-17 12:26:26'),
(6, 'SEG60', 60000, '2017-03-17 12:26:26', '2017-03-17 12:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `coordinates` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_bedrooms` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_of_offer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `name`, `address`, `coordinates`, `price`, `area`, `no_of_bedrooms`, `description`, `time_of_offer`, `status`, `created_at`, `updated_at`) VALUES
(1, 'GUBABI SAFE PLAZA', '194, Herbet Macaulay way, Makoko, Yaba, Lagos.', '6.4939764N,3.3815113E', '20000000', '150', 10, 'Its a nice place previous occupants had no complaints.', '2', 'available', '2017-03-17 14:11:35', '2017-03-17 14:11:35'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `property_pictures`
--

CREATE TABLE `property_pictures` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2017-03-25 13:58:31', '2017-03-25 13:58:31'),
(2, 'Admin', '2017-03-25 13:58:31', '2017-03-25 13:58:31'),
(3, 'User', '2017-03-25 14:01:03', '2017-03-25 14:01:03');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`id`, `created_at`, `updated_at`, `user_id`, `role_id`) VALUES
(2, '2017-03-25 01:08:45', '2017-03-25 01:08:45', 3, 3),
(5, '2017-03-25 01:40:12', '2017-03-25 04:17:26', 7, 2),
(7, '2017-03-25 15:09:41', '2017-03-25 15:09:41', 10, 1),
(12, '2017-03-26 02:05:41', '2017-03-26 02:05:41', 17, 3),
(13, '2017-03-28 02:29:00', '2017-03-28 02:29:00', 18, 2),
(14, '2017-03-29 03:54:00', '2017-03-29 03:54:00', 19, 2),
(15, '2017-04-04 10:14:06', '2017-04-04 10:14:06', 39, 3),
(16, '2017-04-04 03:00:59', '2017-04-04 03:00:59', 45, 2),
(18, '2017-04-04 03:11:19', '2017-04-04 03:11:19', 48, 2),
(19, '2017-04-04 07:44:39', '2017-04-04 07:44:39', 49, 2),
(20, '2017-04-04 10:22:18', '2017-04-04 10:22:18', 57, 3),
(21, '2017-04-05 09:30:51', '2017-04-05 09:30:51', 60, 3),
(22, '2017-04-06 05:54:03', '2017-04-06 05:54:03', 64, 3);

-- --------------------------------------------------------

--
-- Table structure for table `savings`
--

CREATE TABLE `savings` (
  `id` int(10) UNSIGNED NOT NULL,
  `month_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principal` double DEFAULT NULL,
  `principal_prime` double DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_plan_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `savings`
--

INSERT INTO `savings` (`id`, `month_paid`, `principal`, `principal_prime`, `interest`, `created_at`, `updated_at`, `user_id`, `payment_plan_id`) VALUES
(1, 'April', 600, 601.5, 1.5, '2017-03-27 01:34:18', '2017-03-27 01:34:18', 3, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sent_emails`
--

CREATE TABLE `sent_emails` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `month_paid_for` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit_credit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `property_id` int(10) UNSIGNED DEFAULT NULL,
  `payment_plan_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `payment_reference`, `amount`, `month_paid_for`, `status`, `verification_status`, `type`, `debit_credit`, `date_paid`, `created_at`, `updated_at`, `user_id`, `property_id`, `payment_plan_id`) VALUES
(1, 'TREF180317SEG601', 60000, 'Mar', 'successful', 'verified', 'offline', 'credit', '2017-03-18 05:43:16', '2017-03-18 05:43:16', '2017-03-18 06:01:43', 3, 1, 6),
(2, 'TREF180317SEG602', 60000, 'Mar', 'successful', 'verified', 'offline', 'credit', '2017-03-18 06:01:21', '2017-03-18 06:01:21', '2017-03-18 06:01:21', 3, 1, 6),
(3, 'dfgsrfg', 60000, 'April', 'successful', 'verified', 'offline', 'credit', '2017-03-26 23:00:00', '2017-03-27 01:16:39', '2017-03-27 01:16:39', 3, 1, 6),
(4, 'dfgsrfg', 60000, 'April', 'successful', 'verified', 'offline', 'credit', '2017-03-26 23:00:00', '2017-03-27 01:31:12', '2017-03-27 01:31:12', 3, 1, 6),
(5, 'dfgsrfg', 60000, 'April', 'successful', 'verified', 'offline', 'credit', '2017-03-26 23:00:00', '2017-03-27 01:34:18', '2017-03-27 01:34:18', 3, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `upgrade_requests`
--

CREATE TABLE `upgrade_requests` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `new_payment_plan_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_of_origin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residential_address` text COLLATE utf8mb4_unicode_ci,
  `postal_address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `gender`, `dob`, `marital_status`, `state_of_origin`, `nationality`, `residential_address`, `postal_address`, `email`, `telephone_no`, `telephone_no_1`, `username`, `password`, `status`, `profile_pic_url`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'Okunade Mubarak Babajide', 'Male', '20/09/1994', 'Single', 'Oyo', 'Nigerian', '11, Fola agoro street, Shomolu, Lagos Nigeria.', '0', 'mubarakokunade@yahoo.com', '08090524193', '07089371233', 'mb2017', 'f30aa7a662c728b7407c54ae6bfd27d1', NULL, 'localhost.com/uploads/images/xyz.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-25 01:08:45', '2017-03-26 07:16:45'),
(7, 'admin2', 'Male', '02/04/1993', NULL, NULL, NULL, NULL, '0', 'admin_2@email.com', '415642516415456', NULL, 'admin2@email.com', 'c84258e9c39059a89ab77d846ddab909', NULL, 'gjgfjgjg', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-25 01:40:12', '2017-03-25 04:17:26'),
(10, 'Admin Super', NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL, 'admin1@email.com', '71e23baee837c5d2952f93a29b113cab', NULL, NULL, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-25 15:05:32', '2017-03-25 15:05:32'),
(17, 'Okunade Mubarak B', 'Male', '20/09/1994', 'Single', 'Oyo', 'Nigerian', '11, Fola agoro street, Shomolu, Lagos Nigeria.', '0', 'mubarak.okunade@yahoo.com', '08090524194', '0708937233', 'mb_2017', 'eb0fbeb8419885b4d75491f6e12f1baf', 'approved', 'localhost.com/uploads/images/xyz.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-26 02:05:41', '2017-03-26 02:05:41'),
(18, 'admin3', 'Male', '02/04/1994', NULL, NULL, NULL, NULL, '0', '02/04/1994', '07089371235', NULL, 'admin3', '84ff2c4b8b18c28f042557c0637c8528', NULL, 'localhost.com/uploads/images/xyz.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-28 02:29:00', '2017-03-28 02:29:00'),
(19, 'admin4', 'Male', '02/04/1994', NULL, NULL, NULL, NULL, '0', 'admin4@email.com', '07089371234', NULL, 'admin4', 'fba54f07332ad667fce71ac27e82b0a7', NULL, 'localhost.com/uploads/images/xyz.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-03-29 03:54:00', '2017-03-29 03:54:00'),
(39, 'oyeleke', 'male', '1/02/2016', 'single', 'osun', 'nigerian', 'mattew falade', '0', 'timileyinogunsola@gmail.com', '08089650859', NULL, NULL, NULL, 'not approved', 'kinkan.com', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-04 10:14:06', '2017-04-04 10:14:06'),
(45, 'admin11', 'Male', '02/04/1988843', NULL, NULL, NULL, NULL, '0', 'admin10@email.com', '034645635', NULL, 'admin11', 'e020590f0e18cd6053d7ae0e0a507609', NULL, 'localhost.com/uploads/images/xyz.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-04 03:00:59', '2017-04-04 03:00:59'),
(48, 'admin16', 'Male', '02/04/199578', NULL, NULL, NULL, NULL, '0', 'admin16@email.com', '32682558', NULL, 'admin16', '9071e0ca7e4964a5cc69201ba2743650', NULL, 'hwefjwef.sdfai.wfwe', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-04 03:11:19', '2017-04-04 03:11:19'),
(49, 'admin17', 'Male', '1/2/3', NULL, NULL, NULL, NULL, '0', 'admin17@email.com', '456123', NULL, 'admin17', 'd5133c970ad3a99c2248fed76970d06c', NULL, 'xyz.com/uploads/images/fwsf.png', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-04 07:44:39', '2017-04-04 07:44:39'),
(57, 'oyeannu', 'male', '1/02/2016', 'single', 'osun', 'nigerian', 'mattew falad', '0', 'timileyinogunso@gmail.com', '08089650858', NULL, NULL, NULL, 'not approved', 'kinka.com', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-04 10:22:18', '2017-04-04 10:22:18'),
(60, 'oyea', 'male', '1/02/2016', 'single', 'osun', 'nigerian', 'mattew falad', '0', 'timileyinoguns@gmail.com', '08089650850', NULL, NULL, NULL, 'not approved', 'kink.com', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9sb2NhbGhvc3RcL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxNDg2OTUyLCJleHAiOjE0OTE1NzMzNTIsIm5iZiI6MTQ5MTQ4Njk1MiwianRpIjoiYjIyMmE4NTBmYzcyYTA2YzU5YmE1MWM2NTQwYzAzMzkifQ.U_Tg2ffmxsb4nQJBHRE3SGJXYKEbx8oVp-pXtG6n6Ww', '2017-04-05 09:30:51', '2017-04-05 09:30:51'),
(64, 'customer3', 'Male', '20/09/94', 'Single', 'Oyo', 'Nigerian', '11, Fola agoro street, Shomolu, Lagos Nigeria.', '11, Fola agoro street, Shomolu, Lagos Nigeria.', 'customer3@email.com', '2541', '01452', 'customer3', '033f7f6121501ae98285ad77f216d5e7', 'approved', 'localhost.com/uploads/images/xyz.png', NULL, '2017-04-06 05:54:03', '2017-04-06 05:54:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_payment_plan`
--

CREATE TABLE `user_payment_plan` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `payment_plan_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_payment_plan`
--

INSERT INTO `user_payment_plan` (`id`, `created_at`, `updated_at`, `user_id`, `payment_plan_id`) VALUES
(3, '2017-03-25 01:08:45', '2017-03-25 01:08:45', 3, 6),
(8, '2017-03-26 02:05:41', '2017-03-26 02:05:41', 17, 6),
(9, '2017-04-04 10:14:06', '2017-04-04 10:14:06', 39, 1),
(10, '2017-04-04 10:22:18', '2017-04-04 10:22:18', 57, 1),
(11, '2017-04-05 09:30:51', '2017-04-05 09:30:51', 60, 1),
(12, '2017-04-06 05:54:03', '2017-04-06 05:54:03', 64, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_telephone_no_unique` (`telephone_no`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indexes for table `employment_information`
--
ALTER TABLE `employment_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employment_information_user_id_foreign` (`user_id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`),
  ADD KEY `feedback_user_id_foreign` (`user_id`);

--
-- Indexes for table `interested_users`
--
ALTER TABLE `interested_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `interested_users_user_id_foreign` (`user_id`),
  ADD KEY `interested_users_property_id_foreign` (`property_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`),
  ADD KEY `notifications_sent_email_id_foreign` (`sent_email_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `payment_plans`
--
ALTER TABLE `payment_plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `property_pictures`
--
ALTER TABLE `property_pictures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `property_pictures_property_id_foreign` (`property_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_user_user_id_foreign` (`user_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `savings`
--
ALTER TABLE `savings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `savings_user_id_foreign` (`user_id`),
  ADD KEY `savings_payment_plan_id_foreign` (`payment_plan_id`);

--
-- Indexes for table `sent_emails`
--
ALTER TABLE `sent_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sent_emails_user_id_foreign` (`user_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_property_id_foreign` (`property_id`),
  ADD KEY `transactions_payment_plan_id_foreign` (`payment_plan_id`);

--
-- Indexes for table `upgrade_requests`
--
ALTER TABLE `upgrade_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `upgrade_requests_user_id_foreign` (`user_id`),
  ADD KEY `upgrade_requests_new_payment_plan_id_foreign` (`new_payment_plan_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_telephone_no_unique` (`telephone_no`),
  ADD UNIQUE KEY `users_telephone_no_1_unique` (`telephone_no_1`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `user_payment_plan`
--
ALTER TABLE `user_payment_plan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_payment_plan_user_id_foreign` (`user_id`),
  ADD KEY `user_payment_plan_payment_plan_id_foreign` (`payment_plan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employment_information`
--
ALTER TABLE `employment_information`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `interested_users`
--
ALTER TABLE `interested_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_plans`
--
ALTER TABLE `payment_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `property_pictures`
--
ALTER TABLE `property_pictures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `savings`
--
ALTER TABLE `savings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sent_emails`
--
ALTER TABLE `sent_emails`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `upgrade_requests`
--
ALTER TABLE `upgrade_requests`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `user_payment_plan`
--
ALTER TABLE `user_payment_plan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `employment_information`
--
ALTER TABLE `employment_information`
  ADD CONSTRAINT `employment_information_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `interested_users`
--
ALTER TABLE `interested_users`
  ADD CONSTRAINT `interested_users_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  ADD CONSTRAINT `interested_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_sent_email_id_foreign` FOREIGN KEY (`sent_email_id`) REFERENCES `sent_emails` (`id`),
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `property_pictures`
--
ALTER TABLE `property_pictures`
  ADD CONSTRAINT `property_pictures_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`);

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `savings`
--
ALTER TABLE `savings`
  ADD CONSTRAINT `savings_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  ADD CONSTRAINT `savings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `sent_emails`
--
ALTER TABLE `sent_emails`
  ADD CONSTRAINT `sent_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  ADD CONSTRAINT `transactions_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `upgrade_requests`
--
ALTER TABLE `upgrade_requests`
  ADD CONSTRAINT `upgrade_requests_new_payment_plan_id_foreign` FOREIGN KEY (`new_payment_plan_id`) REFERENCES `payment_plans` (`id`),
  ADD CONSTRAINT `upgrade_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_payment_plan`
--
ALTER TABLE `user_payment_plan`
  ADD CONSTRAINT `user_payment_plan_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  ADD CONSTRAINT `user_payment_plan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
