<?php
//file : app/config/constants.php

// $SERVERS stores all the urls for hosting
$SERVERS =  [
    'LOCALHOST' => 'http://localhost/seguro_api/storage/app/',
    'TESTSERVER' => 'http://ask.net.ng/gideon/seguro/seguro_api/storage/app/',
    'LIVESERVER' => 'http://ask.net.ng/seguro/seguro_api_v2/storage/app/'
];


// selects the required url and returns. If you want to use another url, use its index in line 14 .
return [
	'SERVER_ADDRESS' => $SERVERS['TESTSERVER']
];