<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('surname')->nullable();
            $table->string('othernames')->nullable();
            $table->text('fullname')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('state_of_origin')->nullable();
            $table->string('nationality')->nullable();
            $table->text('residential_address')->nullable();
            $table->text('postal_address')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('telephone_no')->unique()->nullable();
            $table->string('telephone_no_1')->unique()->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('password')->nullable();
            $table->string('status')->nullable();
            $table->string('profile_pic_url')->nullable();  // admins will now be dumped into this table with roles assigned to them 0 for super admin 1 for admin and 2 for a user
            $table->string('identification_url')->nullable();
            $table->string('application_no')->nullable();
            $table->string('referral_no')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
