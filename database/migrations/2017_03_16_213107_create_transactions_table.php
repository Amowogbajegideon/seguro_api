<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('payment_reference')->nullable();
            $table->integer('amount')->nullable();
            $table->string('month_paid_for')->nullable();
            $table->string('status')->nullable();
            $table->string('verification_status')->nullable();
            $table->string('type')->nullable();
            $table->string('debit_credit')->nullable();
            $table->string('date_paid')->nullable();
            $table->text('description')->nullable();
            $table->double('balance')->nullable();
            $table->timestamps();
        });

        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('payment_plan_id')->unsigned();
            $table->foreign('payment_plan_id')->references('id')->on('payment_plans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
