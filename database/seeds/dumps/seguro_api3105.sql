-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: seguro_api
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_telephone_no_unique` (`telephone_no`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `application_nos`
--

DROP TABLE IF EXISTS `application_nos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_nos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `application_no` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application_nos`
--

LOCK TABLES `application_nos` WRITE;
/*!40000 ALTER TABLE `application_nos` DISABLE KEYS */;
INSERT INTO `application_nos` VALUES (1,'000055','2017-05-31 03:28:08','2017-05-31 03:28:08'),(2,'000056','2017-05-31 03:32:40','2017-05-31 03:32:40'),(3,'000057','2017-05-31 03:38:18','2017-05-31 03:38:18'),(4,'000058','2017-05-31 03:44:52','2017-05-31 03:44:52'),(5,'000059','2017-05-31 03:47:07','2017-05-31 03:47:07'),(6,'000060','2017-05-31 03:50:03','2017-05-31 03:50:03'),(7,'000061','2017-05-31 03:50:38','2017-05-31 03:50:38'),(8,'000062','2017-05-31 04:11:10','2017-05-31 04:11:10'),(9,'000063','2017-05-31 04:11:10','2017-05-31 04:11:10'),(10,'000064','2017-05-31 04:13:06','2017-05-31 04:13:06');
/*!40000 ALTER TABLE `application_nos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employment_information`
--

DROP TABLE IF EXISTS `employment_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employment_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_employer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employment_information_user_id_foreign` (`user_id`),
  CONSTRAINT `employment_information_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employment_information`
--

LOCK TABLES `employment_information` WRITE;
/*!40000 ALTER TABLE `employment_information` DISABLE KEYS */;
INSERT INTO `employment_information` VALUES (2,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-03-25 01:08:45','2017-03-25 01:08:45',3),(7,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-03-26 02:05:41','2017-03-26 02:05:41',17),(8,'employed','dfb gsdndbsd','software engineer','makoko yaba','2017-04-04 10:14:06','2017-04-04 10:14:06',39),(9,'employed','iqube','software engineer','makoko yaba','2017-04-04 10:22:18','2017-04-04 10:22:18',57),(10,'employed','iqube','software engineer','makoko yaba','2017-04-05 09:30:51','2017-04-05 09:30:51',60),(13,'employed','customer1','customer1','customer1','2017-04-06 02:27:32','2017-04-06 02:27:32',85),(14,'employed','iqube','software engineer','makoko yaba','2017-04-15 01:37:41','2017-04-15 01:37:41',87),(15,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-04-18 09:39:43','2017-04-18 09:39:43',89),(16,'employed','customer 3\'s employer','customer 3\'s designation','customer 3\'s place of work','2017-04-18 10:27:19','2017-04-18 10:27:19',91),(17,'employed','customer 4\'s employer','customer 4\'s designation','customer 4\'s employer\'s address','2017-04-19 09:03:30','2017-04-19 09:03:30',108),(18,'unemployed','iqube','software engineer','makoko yaba','2017-04-19 02:04:47','2017-04-19 02:04:47',111),(19,'employed','asrcwecfwa','sdfsdacfs','edfascsd','2017-04-19 09:11:58','2017-04-19 09:11:58',112),(20,'employed',NULL,NULL,'jijjkkjjk','2017-05-03 09:45:12','2017-05-03 09:45:12',119),(21,'employed','ujksdgflkskk',',jvxMAnm',NULL,'2017-05-03 09:59:16','2017-05-03 09:59:16',121),(22,'employed','iQube Labs','Technical Project Manager','GUBABI SAFE PLAZA','2017-05-10 02:14:09','2017-05-10 02:14:09',122),(23,'self_employed','iqube','software engineer','makoko yaba','2017-05-11 04:00:05','2017-05-11 04:00:05',123),(24,'employed','iqube','software engineer','makoko yaba','2017-05-11 04:19:22','2017-05-11 04:19:22',127),(25,'self_employed','rffomnleamsc','relferlmcl','wedvcdxslmc','2017-05-12 04:56:39','2017-05-12 04:56:39',130),(26,'self_employed','edcxaszudhuwrdvc','rewfuchdcds','srgfvbrhbc','2017-05-12 08:22:09','2017-05-12 08:22:09',131),(27,'self_employed','rvukrnksdflcw','wefkvuhjuevnjvn','wefojwerifvorjv','2017-05-12 03:55:47','2017-05-12 03:55:47',134),(28,'employed','The people','Hero','Secret','2017-05-12 04:03:19','2017-05-12 04:03:19',135),(29,'employed','fdfgf','dgfd','gdfd','2017-05-17 01:51:29','2017-05-17 01:51:29',139),(30,'employed','fbvfsddc','sdsadsc','fdbvcdfsdgdf','2017-05-17 01:58:20','2017-05-17 01:58:20',141),(31,'employed','iqube','programmer','lag','2017-05-17 02:37:49','2017-05-17 02:37:49',144),(32,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-05-19 10:48:59','2017-05-19 10:48:59',146),(33,'self_employed','tgbvrsvvt','tbrvsrfgvgf','tbrvsvtbvrb','2017-05-22 05:11:17','2017-05-22 05:11:17',147),(34,'self_employed','fvnmfckmv','rfbvfmclvf','fbmfkckmvf','2017-05-23 07:34:51','2017-05-23 07:34:51',148),(35,'employed','James Fowe','Technical Project Manager','sdgfvhjn','2017-05-25 05:47:28','2017-05-25 05:47:28',149),(36,'employed',NULL,NULL,'hhj','2017-05-26 10:26:45','2017-05-26 10:26:45',150),(37,'employed','ascscssd','sasdcas','asdcasdsdcsdc','2017-05-29 02:22:05','2017-05-29 02:22:05',151),(38,'employed','iqube','LAGOS','195 HEBRAT','2017-05-30 08:51:00','2017-05-30 08:51:00',152);
/*!40000 ALTER TABLE `employment_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feedback_user_id_foreign` (`user_id`),
  CONSTRAINT `feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
INSERT INTO `feedback` VALUES (1,'test','this is a test',NULL,'2017-04-20 02:02:37','2017-04-20 02:02:37',3),(2,'test','this is a test',NULL,'2017-04-20 03:53:51','2017-04-20 03:53:51',3),(3,'this is a test','testing',NULL,'2017-04-25 09:32:13','2017-04-25 09:32:13',3),(4,'heyyyy','whats up',NULL,'2017-05-03 09:54:01','2017-05-03 09:54:01',3),(5,'heyy','whats up',NULL,'2017-05-03 09:55:47','2017-05-03 09:55:47',119),(6,'KBHNOLK','YFUVGBHKJ',NULL,'2017-05-03 10:27:52','2017-05-03 10:27:52',3),(7,'GH','HGHH',NULL,'2017-05-03 10:28:15','2017-05-03 10:28:15',3),(8,'this is a test','testtingg  uinjcfrewocmopcwmpcw,',NULL,'2017-05-04 11:28:41','2017-05-04 11:28:41',3),(9,'tgelmv','jlwrlkvmpwfc,rlpc;lvvmmmv',NULL,'2017-05-04 12:55:22','2017-05-04 12:55:22',3),(10,'yyyy','yyyyy',NULL,'2017-05-13 11:48:06','2017-05-13 11:48:06',122);
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interest_rate`
--

DROP TABLE IF EXISTS `interest_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interest_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `interest_rate` double DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interest_rate`
--

LOCK TABLES `interest_rate` WRITE;
/*!40000 ALTER TABLE `interest_rate` DISABLE KEYS */;
INSERT INTO `interest_rate` VALUES (1,5,10,'2017-04-24 14:57:38','2017-04-24 14:57:38'),(2,3,10,'2017-04-26 09:45:29','2017-04-26 09:45:29'),(3,10,10,NULL,NULL),(4,3,10,NULL,NULL),(5,5,10,NULL,NULL),(6,3,10,NULL,NULL),(7,5,10,NULL,NULL),(8,10,10,NULL,NULL),(9,50,10,NULL,NULL),(10,3,10,NULL,NULL),(11,7,10,NULL,NULL),(12,3,10,NULL,NULL),(13,4,10,NULL,NULL),(14,3,10,NULL,NULL),(15,7,10,NULL,NULL),(16,7,10,NULL,NULL),(17,3,10,NULL,NULL);
/*!40000 ALTER TABLE `interest_rate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interested_users`
--

DROP TABLE IF EXISTS `interested_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interested_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `interested_users_user_id_foreign` (`user_id`),
  KEY `interested_users_property_id_foreign` (`property_id`),
  CONSTRAINT `interested_users_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `interested_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interested_users`
--

LOCK TABLES `interested_users` WRITE;
/*!40000 ALTER TABLE `interested_users` DISABLE KEYS */;
INSERT INTO `interested_users` VALUES (1,'2017-03-27 08:39:45','2017-03-27 08:39:45',17,1),(2,'2017-03-27 09:11:46','2017-03-27 09:11:46',3,1),(3,'2017-05-04 08:43:00','2017-05-04 08:43:00',3,8),(4,'2017-05-04 08:57:55','2017-05-04 08:57:55',3,9),(5,'2017-05-04 10:10:40','2017-05-04 10:10:40',3,8),(6,'2017-05-04 10:13:57','2017-05-04 10:13:57',3,8),(7,'2017-05-04 10:15:00','2017-05-04 10:15:00',3,8),(8,'2017-05-04 10:20:16','2017-05-04 10:20:16',3,9),(9,'2017-05-04 10:20:57','2017-05-04 10:20:57',3,9),(10,'2017-05-04 10:21:00','2017-05-04 10:21:00',3,9),(11,'2017-05-04 10:22:27','2017-05-04 10:22:27',3,9),(12,'2017-05-04 10:22:40','2017-05-04 10:22:40',3,8),(13,'2017-05-04 10:23:12','2017-05-04 10:23:12',3,8),(14,'2017-05-04 10:24:12','2017-05-04 10:24:12',3,8),(15,'2017-05-04 10:24:25','2017-05-04 10:24:25',3,8),(16,'2017-05-04 10:25:19','2017-05-04 10:25:19',3,8),(17,'2017-05-04 10:27:00','2017-05-04 10:27:00',3,9),(18,'2017-05-04 10:28:15','2017-05-04 10:28:15',3,9),(19,'2017-05-04 10:28:19','2017-05-04 10:28:19',3,9),(20,'2017-05-04 10:28:22','2017-05-04 10:28:22',3,9),(21,'2017-05-04 10:31:26','2017-05-04 10:31:26',3,9),(22,'2017-05-04 10:31:32','2017-05-04 10:31:32',3,1),(23,'2017-05-04 10:52:19','2017-05-04 10:52:19',3,9),(24,'2017-05-04 10:59:16','2017-05-04 10:59:16',3,9),(25,'2017-05-04 11:02:11','2017-05-04 11:02:11',3,9),(26,'2017-05-04 11:03:05','2017-05-04 11:03:05',3,9),(27,'2017-05-04 11:09:24','2017-05-04 11:09:24',3,8),(28,'2017-05-04 11:10:06','2017-05-04 11:10:06',3,9),(29,'2017-05-04 11:11:40','2017-05-04 11:11:40',3,9),(30,'2017-05-04 11:48:25','2017-05-04 11:48:25',3,8),(31,'2017-05-09 01:28:11','2017-05-09 01:28:11',3,1),(32,'2017-05-12 08:26:33','2017-05-12 08:26:33',3,9),(33,'2017-05-13 11:52:35','2017-05-13 11:52:35',3,1),(34,'2017-05-13 11:52:36','2017-05-13 11:52:36',3,1);
/*!40000 ALTER TABLE `interested_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (19,'2014_10_12_000000_create_users_table',1),(20,'2014_10_12_100000_create_password_resets_table',1),(21,'2017_03_16_211833_create_admins_table',1),(22,'2017_03_16_211931_create_properties_table',1),(23,'2017_03_16_212032_create_payment_plans_table',1),(24,'2017_03_16_213107_create_transactions_table',1),(25,'2017_03_16_214253_create_feedback_table',1),(26,'2017_03_16_220346_create_property_pictures_table',1),(27,'2017_03_16_220806_create_sent_emails_table',1),(28,'2017_03_16_221332_create_employment_information_table',1),(29,'2017_03_16_221614_create_interested_users_table',1),(30,'2017_03_16_221905_create_upgrade_requests_table',1),(31,'2017_03_16_223159_create_user_payment_plan_table',1),(32,'2017_03_17_085601_add_status_to_upgrade_requests_table',1),(33,'2017_03_22_143250_create_savings_table',1),(34,'2017_03_23_200155_create_notifications_table',1),(35,'2017_03_24_235753_create_roles_table',1),(36,'2017_03_25_001520_create_role_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `next_of_kin_details`
--

DROP TABLE IF EXISTS `next_of_kin_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `next_of_kin_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kin_name` varchar(191) DEFAULT NULL,
  `relationship` varchar(191) DEFAULT NULL,
  `kin_telephone_no` varchar(191) DEFAULT NULL,
  `kin_address` text,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `next_of_kin_details`
--

LOCK TABLES `next_of_kin_details` WRITE;
/*!40000 ALTER TABLE `next_of_kin_details` DISABLE KEYS */;
INSERT INTO `next_of_kin_details` VALUES (1,'Okunade Muritala','Father','08023169887','vdfdddddvds',146,'2017-05-19 10:48:59','2017-05-19 10:48:59'),(2,'seun','brother','08076362636','HERTBER',152,'2017-05-30 08:51:00','2017-05-30 08:51:00');
/*!40000 ALTER TABLE `next_of_kin_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sms` text COLLATE utf8mb4_unicode_ci,
  `dashboard` text COLLATE utf8mb4_unicode_ci,
  `message_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `sent_email_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_foreign` (`user_id`),
  KEY `notifications_sent_email_id_foreign` (`sent_email_id`),
  CONSTRAINT `notifications_sent_email_id_foreign` FOREIGN KEY (`sent_email_id`) REFERENCES `sent_emails` (`id`),
  CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,'Your SEGURO application has been approved. Your username : SEG3072328 and password : 40030.',NULL,404,NULL,NULL,39,NULL),(2,'Your SEGURO application has been approved. Your username : SEG3017209 and password : 40003.',NULL,404,NULL,NULL,39,NULL),(3,'Your SEGURO application has been approved. Your username : 0 and password : 0.',NULL,404,NULL,NULL,39,NULL),(4,'Your SEGURO application has been approved. Your username : 0 and password : 0.',NULL,404,NULL,NULL,39,NULL),(5,'Your SEGURO application has been approved. Your username : 0 and password : 0.',NULL,404,NULL,NULL,39,NULL),(6,'Your SEGURO application has been approved. Your username : SEG3049216 and password : 41248.',NULL,404,NULL,NULL,39,NULL),(7,'Your SEGURO application has been approved. Your username : SEG3064909 and password : 92317.',NULL,404,NULL,NULL,39,NULL),(8,'Your SEGURO application has been approved. Your username : SEG3035938 and password : 54862.',NULL,65732868,NULL,NULL,39,NULL),(9,'Your SEGURO application has been approved. Your username : SEG3091011 and password : 35837.',NULL,65733012,NULL,NULL,39,NULL),(10,'Your SEGURO application has been approved. Your username : SEG309036 and password : 58564.',NULL,65733125,NULL,NULL,39,NULL),(11,'Your SEGURO application has been approved. Your username : SEG1045848 and password : 86176.',NULL,65742211,NULL,NULL,152,NULL),(12,'Your SEGURO application has been approved. Your username : SEG1036792 and password : 19731.',NULL,65742215,NULL,NULL,152,NULL),(13,'Your SEGURO application has been approved. Your username : SEG1064210 and password : 26513.',NULL,65747388,NULL,NULL,152,NULL);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_plans`
--

DROP TABLE IF EXISTS `payment_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_plan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_plans`
--

LOCK TABLES `payment_plans` WRITE;
/*!40000 ALTER TABLE `payment_plans` DISABLE KEYS */;
INSERT INTO `payment_plans` VALUES (1,'SEG10',10000,'2017-03-17 12:24:55','2017-03-17 12:24:55'),(2,'SEG20',20000,'2017-03-17 12:24:55','2017-03-17 12:24:55'),(3,'SEG30',30000,'2017-03-17 12:25:50','2017-03-17 12:25:50'),(4,'SEG40',40000,'2017-03-17 12:25:50','2017-03-17 12:25:50'),(5,'SEG50',50000,'2017-03-17 12:26:26','2017-03-17 12:26:26'),(6,'SEG60',60000,'2017-03-17 12:26:26','2017-03-17 12:26:26'),(7,'SEG70',70000,'2017-05-14 13:41:07','2017-05-14 13:41:07'),(8,'SEG80',80000,'2017-05-14 13:41:07','2017-05-14 13:41:07'),(9,'SEG90',90000,'2017-05-14 13:41:57','2017-05-14 13:41:57'),(10,'SEGPLUS',0,'2017-05-14 13:42:44','2017-05-14 15:42:38');
/*!40000 ALTER TABLE `payment_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `coordinates` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_bedrooms` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_of_offer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (1,'GUBABI SAFE PLAZA','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','6.4939764N,3.3815113E','20000000','150',10,'Its a nice place previous occupants had no complaints.','2','available','2017-03-17 14:11:35','2017-03-17 14:11:35'),(3,'prop1','prop1',NULL,'1000000','100000',5,'prop1',NULL,NULL,'2017-04-06 03:01:59','2017-04-06 03:01:59'),(4,'prop2','prop2',NULL,'1500000','12000',29,'prop2',NULL,NULL,'2017-04-07 08:25:30','2017-04-07 08:25:30'),(5,'PR0P3','prop3',NULL,'123','456',2,'fdhdf',NULL,NULL,'2017-04-07 08:26:50','2017-04-07 10:15:30'),(6,'Hogwarts','Some place in the Scottish highlands',NULL,'200000000000000','1000000',8,'A school of witchcraft and wizadry',NULL,NULL,'2017-04-18 09:46:20','2017-04-18 09:46:20'),(7,'Hagrids cabin','Hogwarts grounds just before the forbidden forest',NULL,'84768545638','5332',1,'A home for Hagrid whilst working as gamekeeper for Hogwarts',NULL,NULL,'2017-04-19 09:57:52','2017-04-19 09:57:52'),(8,'Hogwarts14','Some place in the Scottish highlands',NULL,'200000000000000','1000000',8,'A school of witchcraft and wizadry',NULL,'available','2017-04-29 05:32:33','2017-04-29 05:32:33'),(9,'Property 112','194, herbert macaulay way',NULL,'10000','133',5,'d efg',NULL,'available','2017-05-02 12:18:31','2017-05-02 12:18:31'),(10,'Property 13','194, herbert macaulay way',NULL,'100000','1234',13,'df regtegetg',NULL,'available','2017-05-02 12:19:36','2017-05-02 12:19:36'),(11,'Property 13','194, herbert macaulay way',NULL,'100000','1234',13,'df regtegetg',NULL,'not available','2017-05-02 12:19:51','2017-05-02 12:19:51'),(12,'dsfdc','sdacfsdc',NULL,'23414','2345123432',42314234,'tgvdrgf',NULL,'available','2017-05-12 04:11:38','2017-05-12 04:11:38'),(13,'hrp','ph',NULL,'56700000','34',85,'bedhouse',NULL,'available','2017-05-14 05:37:31','2017-05-14 05:37:31'),(14,'Hogwarts12','Some place in the Scottish highlands',NULL,'200000000000000','1000000',8,'Its a school of witchcraft and wizadry',NULL,'available','2017-05-20 03:27:00','2017-05-20 03:27:00'),(16,'nlsnlkksda','kmsdck',NULL,'854735274141','518514585',88961185,'njlsfvlmilwcilmflas;l',NULL,'available','2017-05-25 01:57:02','2017-05-25 01:57:02'),(17,'ajah','ghhjh',NULL,'4567890','78989',6,'cgvhbjk',NULL,'available','2017-05-26 10:13:50','2017-05-26 10:13:50'),(18,'mini flat','igando',NULL,'2300000','3',4,'ssdfee',NULL,'available','2017-05-31 08:54:08','2017-05-31 08:54:08');
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pictures`
--

DROP TABLE IF EXISTS `property_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_pictures_property_id_foreign` (`property_id`),
  CONSTRAINT `property_pictures_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pictures`
--

LOCK TABLES `property_pictures` WRITE;
/*!40000 ALTER TABLE `property_pictures` DISABLE KEYS */;
INSERT INTO `property_pictures` VALUES (1,'xyz.com/uploads/images/fwsf.png','2017-04-06 03:01:59','2017-04-06 03:01:59',3),(2,'xyz.com/uploads/images/fwsf.png','2017-04-07 08:25:30','2017-04-07 08:25:30',4),(3,'xyz.com/uploads/images/fwsf.png','2017-04-07 08:26:50','2017-04-07 08:26:50',5),(4,'xyz.com/uploads/images/fwsf.png',NULL,'2017-04-07 10:09:11',5),(5,'xyz.com/uploads/images/fwsf.png',NULL,'2017-04-07 10:10:06',5),(6,'xyz.com/uploads/images/fwsf.png',NULL,'2017-04-07 10:15:30',5),(7,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/4Bqb2RVuHb6K6zlSe4tAfKeNJ6A8JfU8nGLW2z6L.jpeg','2017-04-18 09:46:20','2017-04-18 09:46:20',6),(8,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/drIvxS85acYuNNgRErmknqWGWdZfviYhKAGxHZcu.jpeg','2017-04-19 09:57:52','2017-04-19 09:57:52',7),(9,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/fgyE6VYEpqBVV7dUuawUptzDGc9BtHUnDYfIXmjK.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(10,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/o52OzRQanHTOGF9vjI4Sraw5ylUl75tfMHRIS6PO.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(11,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/LydG1wMHyd96FH3V28oVsJREP9elBuLpLkv3vCSi.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(12,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/v2q3dkYeUcWIViM4dJnk9RUy23QfFsDfIg7QUwDu.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(13,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/SRVe6QvV4nm1xJCWNRBHrH9bXiVn6RfbGdfLfvwr.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(14,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/TpAcofwMwstvn5FNYEoNW4cGwXV4wsu7UqZtLm5b.jpeg','2017-04-29 05:32:33','2017-04-29 05:32:33',8),(15,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/kH5t6dBTnqFBtkcNvAV6syGlVJ4ALNzuUIfoPgmI.jpeg','2017-05-20 03:27:01','2017-05-20 03:27:01',14),(16,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/semkeKKonFrtpalcP9n54Xo22nZgGtzpGmJgjjGd.jpeg','2017-05-25 01:57:02','2017-05-25 01:57:02',16),(17,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/U2sony2q8RE0MTjD9Jgzqdb8CBsI24I2tV3fRPse.png','2017-05-26 10:13:50','2017-05-26 10:13:50',17),(18,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/property_pics/7QZwIg0uWvi3uHvkPBSuiS3CvWs06uX2fy0TjG8B.jpeg','2017-05-31 08:54:08','2017-05-31 08:54:08',18);
/*!40000 ALTER TABLE `property_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (2,'2017-03-25 01:08:45','2017-03-25 01:08:45',3,3),(5,'2017-03-25 01:40:12','2017-03-25 04:17:26',7,2),(7,'2017-03-25 15:09:41','2017-03-25 15:09:41',10,1),(12,'2017-03-26 02:05:41','2017-03-26 02:05:41',17,3),(13,'2017-03-28 02:29:00','2017-03-28 02:29:00',18,2),(15,'2017-04-04 10:14:06','2017-04-04 10:14:06',39,3),(20,'2017-04-04 10:22:18','2017-04-04 10:22:18',57,3),(21,'2017-04-05 09:30:51','2017-04-05 09:30:51',60,3),(24,'2017-04-05 02:38:31','2017-04-05 02:38:31',64,2),(28,'2017-04-05 09:05:53','2017-04-06 12:48:39',82,2),(29,'2017-04-06 09:10:11','2017-04-06 09:10:11',83,2),(30,'2017-04-06 01:17:02','2017-04-06 01:17:02',84,2),(31,'2017-04-06 02:27:32','2017-04-06 02:27:32',85,3),(32,'2017-04-12 02:51:25','2017-04-13 04:55:58',86,2),(33,'2017-04-15 01:37:41','2017-04-15 01:37:41',87,3),(34,'2017-04-18 09:22:50','2017-04-18 09:22:50',88,2),(35,'2017-04-18 09:39:43','2017-04-18 09:39:43',89,3),(37,'2017-04-18 10:27:19','2017-04-18 10:27:19',91,3),(39,'2017-04-18 11:04:06','2017-04-18 11:04:06',93,2),(40,'2017-04-18 01:27:08','2017-04-18 01:27:08',103,2),(41,'2017-04-18 01:30:42','2017-04-18 01:30:42',104,2),(42,'2017-04-18 02:37:56','2017-04-18 02:37:56',105,2),(44,'2017-04-19 08:37:43','2017-04-19 08:47:44',107,2),(45,'2017-04-19 09:03:30','2017-04-19 09:03:30',108,3),(46,'2017-04-19 12:09:52','2017-05-08 10:17:16',109,2),(47,'2017-04-19 02:04:47','2017-04-19 02:04:47',111,3),(48,'2017-04-19 09:11:58','2017-04-19 09:11:58',112,3),(49,'2017-05-03 09:33:53','2017-05-03 09:33:53',113,2),(50,'2017-05-03 09:36:18','2017-05-03 09:36:18',116,2),(51,'2017-05-03 09:45:12','2017-05-03 09:45:12',119,3),(52,'2017-05-03 09:54:57','2017-05-03 09:54:57',120,2),(53,'2017-05-03 09:59:16','2017-05-03 09:59:16',121,3),(54,'2017-05-10 02:14:09','2017-05-10 02:14:09',122,3),(55,'2017-05-11 04:00:05','2017-05-11 04:00:05',123,3),(56,'2017-05-11 04:19:22','2017-05-11 04:19:22',127,3),(57,'2017-05-12 04:56:39','2017-05-12 04:56:39',130,3),(58,'2017-05-12 08:22:09','2017-05-12 08:22:09',131,3),(59,'2017-05-12 02:54:11','2017-05-12 02:54:11',133,2),(60,'2017-05-12 03:55:47','2017-05-12 03:55:47',134,3),(61,'2017-05-12 04:03:19','2017-05-12 04:03:19',135,3),(62,'2017-05-14 01:33:00','2017-05-14 01:33:00',137,2),(63,'2017-05-14 01:34:54','2017-05-14 01:34:54',138,2),(64,'2017-05-17 01:51:29','2017-05-17 01:51:29',139,3),(65,'2017-05-17 01:55:53','2017-05-27 04:30:31',140,2),(66,'2017-05-17 01:58:20','2017-05-17 01:58:20',141,3),(67,'2017-05-17 02:37:49','2017-05-17 02:37:49',144,3),(68,'2017-05-17 03:24:18','2017-05-17 03:24:18',145,2),(69,'2017-05-19 10:48:59','2017-05-19 10:48:59',146,3),(70,'2017-05-22 05:11:17','2017-05-22 05:11:17',147,3),(71,'2017-05-23 07:34:51','2017-05-23 07:34:51',148,3),(72,'2017-05-25 05:47:28','2017-05-25 05:47:28',149,3),(73,'2017-05-26 10:26:45','2017-05-26 10:26:45',150,3),(74,'2017-05-29 02:22:05','2017-05-29 02:22:05',151,3),(75,'2017-05-30 08:51:00','2017-05-30 08:51:00',152,3);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Super Admin','2017-03-25 13:58:31','2017-03-25 13:58:31'),(2,'Admin','2017-03-25 13:58:31','2017-03-25 13:58:31'),(3,'User','2017-03-25 14:01:03','2017-03-25 14:01:03');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings`
--

DROP TABLE IF EXISTS `savings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `savings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principal` double DEFAULT NULL,
  `principal_prime` double DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `payment_plan_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `savings_user_id_foreign` (`user_id`),
  KEY `savings_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `savings_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `savings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings`
--

LOCK TABLES `savings` WRITE;
/*!40000 ALTER TABLE `savings` DISABLE KEYS */;
INSERT INTO `savings` VALUES (1,'April',600,601.5,1.5,'2017-03-27 01:34:18','2017-03-27 01:34:18',3,6),(2,'Apr',600,601.5,1.5,'2017-04-06 04:29:35','2017-04-06 04:29:35',17,6),(3,'Apr',500,501.25,1.25,'2017-04-06 04:39:18','2017-04-06 04:39:18',85,1),(4,'Apr',100,100.25,0.25,'2017-05-03 09:46:42','2017-05-03 09:46:42',119,3),(5,'Sep',200,200.5,0.5,'2017-05-06 10:57:10','2017-05-06 10:57:10',121,6),(6,'May',10000,10025,25,'2017-05-06 01:50:27','2017-05-06 01:50:27',39,1),(7,'May',25,NULL,NULL,'2017-05-06 01:52:59','2017-05-06 01:52:59',39,1),(8,'May',10000,10025,25,'2017-05-06 02:16:26','2017-05-06 02:16:26',39,1),(9,'May',20025,20075.0625,50.0625,'2017-05-06 02:17:13','2017-05-06 02:17:13',39,1),(10,'Aug',20000,20116.666666666668,116.66666666666788,'2017-05-08 10:14:41','2017-05-08 10:14:41',111,1),(11,'Aug',50000,50291.666666666664,291.66666666666424,'2017-05-14 06:04:50','2017-05-14 06:04:50',122,5),(12,'Dec',100291.66666666666,100876.70138888888,585.034722222219,'2017-05-14 06:17:40','2017-05-14 06:17:40',122,5),(13,'Dec',50876.701388888876,NULL,NULL,'2017-05-14 06:51:51','2017-05-14 06:51:51',122,5),(14,'May',30075.0625,30150.25015625,75.18765625000015,'2017-05-20 03:12:43','2017-05-20 03:12:43',39,3),(15,'May',40150.25015625,40250.62578164062,100.37562539062492,'2017-05-22 11:21:16','2017-05-22 11:21:16',39,3),(16,'May',10000,10025,25,'2017-05-22 11:37:00','2017-05-22 11:37:00',87,1),(17,'May',20025,20075.0625,50.0625,'2017-05-25 02:21:09','2017-05-25 02:21:09',87,1),(18,'May',40150.25015625,38102.77578164062,100.37562539062492,'2017-05-30 10:09:48','2017-05-30 10:09:48',39,3),(19,'May',40150.25015625,35954.925781640624,100.37562539062492,'2017-05-30 10:10:11','2017-05-30 10:10:11',39,3),(20,'May',40150.25015625,33807.075781640626,100.37562539062492,'2017-05-30 10:16:50','2017-05-30 10:16:50',39,3),(21,'May',40150.25015625,31659.225781640627,100.37562539062492,'2017-05-30 10:17:29','2017-05-30 10:17:29',39,3),(22,'May',40150.25015625,29511.37578164063,100.37562539062492,'2017-05-30 10:18:29','2017-05-30 10:18:29',39,3),(23,'May',40150.25015625,27363.52578164063,100.37562539062492,'2017-05-30 11:41:57','2017-05-30 11:41:57',39,3),(24,'May',40150.25015625,25215.67578164063,100.37562539062492,'2017-05-30 11:57:22','2017-05-30 11:57:22',39,3),(25,'May',40150.25015625,25211.375781640632,100.37562539062492,'2017-05-30 00:01:07','2017-05-30 00:01:07',39,3),(26,'May',40150.25015625,25209.22578164063,100.37562539062492,'2017-05-30 00:06:22','2017-05-30 00:06:22',39,3),(27,'May',40150.25015625,25207.07578164063,100.37562539062492,'2017-05-30 00:10:31','2017-05-30 00:10:31',39,3),(28,'05',-2.15,0,0,'2017-05-30 08:57:14','2017-05-30 08:57:14',152,1),(29,'05',-2.15,-2.15,0,'2017-05-30 08:58:57','2017-05-30 08:58:57',152,1),(30,'05',-2.15,-4.3,0,'2017-05-31 09:06:23','2017-05-31 09:06:23',152,1);
/*!40000 ALTER TABLE `savings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sent_emails`
--

DROP TABLE IF EXISTS `sent_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sent_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bcc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sent_emails_user_id_foreign` (`user_id`),
  CONSTRAINT `sent_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sent_emails`
--

LOCK TABLES `sent_emails` WRITE;
/*!40000 ALTER TABLE `sent_emails` DISABLE KEYS */;
INSERT INTO `sent_emails` VALUES (1,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3072328 and password: 40030.','2017-05-30 10:09:53','2017-05-30 10:09:53',NULL,NULL,'timileyinogunsola@gmail.com','',''),(2,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3017209 and password: 40003.','2017-05-30 10:10:15','2017-05-30 10:10:15',NULL,NULL,'timileyinogunsola@gmail.com','',''),(3,'SEGURO application approval','Your SEGURO application has been approved. Your username: 0 and password: 0.','2017-05-30 10:16:55','2017-05-30 10:16:55',NULL,NULL,'timileyinogunsola@gmail.com','',''),(4,'SEGURO application approval','Your SEGURO application has been approved. Your username: 0 and password: 0.','2017-05-30 10:17:35','2017-05-30 10:17:35',NULL,NULL,'timileyinogunsola@gmail.com','',''),(5,'SEGURO application approval','Your SEGURO application has been approved. Your username: 0 and password: 0.','2017-05-30 10:18:34','2017-05-30 10:18:34',NULL,NULL,'timileyinogunsola@gmail.com','',''),(6,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3049216 and password: 41248.','2017-05-30 11:42:08','2017-05-30 11:42:08',NULL,NULL,'timileyinogunsola@gmail.com','',''),(7,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3064909 and password: 92317.','2017-05-30 11:57:26','2017-05-30 11:57:26',NULL,NULL,'timileyinogunsola@gmail.com','',''),(8,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3035938 and password: 54862.','2017-05-30 00:01:11','2017-05-30 00:01:11',NULL,NULL,'timileyinogunsola@gmail.com','',''),(9,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG3091011 and password: 35837.','2017-05-30 00:06:25','2017-05-30 00:06:25',NULL,NULL,'timileyinogunsola@gmail.com','',''),(10,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG309036 and password: 58564.','2017-05-30 00:10:36','2017-05-30 00:10:36',NULL,NULL,'timileyinogunsola@gmail.com','',''),(11,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG1045848 and password: 86176.','2017-05-30 08:57:16','2017-05-30 08:57:16',NULL,NULL,'ogunmolalamide@gmail.com','',''),(12,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG1036792 and password: 19731.','2017-05-30 08:59:01','2017-05-30 08:59:01',NULL,NULL,'ogunmolalamide@gmail.com','',''),(13,'SEGURO application approval','Your SEGURO application has been approved. Your username: SEG1064210 and password: 26513.','2017-05-31 09:06:23','2017-05-31 09:06:23',NULL,NULL,'ogunmolalamide@gmail.com','','');
/*!40000 ALTER TABLE `sent_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `month_paid_for` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit_credit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `payment_plan_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `balance` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  KEY `transactions_property_id_foreign` (`property_id`),
  KEY `transactions_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `transactions_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `transactions_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,'TREF180317SEG601',60000,'Mar','successful','verified','offline','credit','2017-03-18 06:43:16','2017-03-18 05:43:16','2017-03-18 06:01:43',3,1,6,NULL,NULL),(2,'TREF180317SEG602',60000,'Mar','successful','verified','offline','credit','2017-03-18 07:01:21','2017-03-18 06:01:21','2017-03-18 06:01:21',3,1,6,NULL,NULL),(3,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-27 00:00:00','2017-03-27 01:16:39','2017-03-27 01:16:39',3,1,6,NULL,NULL),(4,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-27 00:00:00','2017-03-27 01:31:12','2017-03-27 01:31:12',3,1,6,NULL,NULL),(5,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-27 00:00:00','2017-03-27 01:34:18','2017-03-27 01:34:18',3,1,6,NULL,NULL),(6,'n nn',60000,'Apr','successful','verified','offline','credit','2017-04-06 01:00:00','2017-04-06 04:29:35','2017-04-06 04:29:35',17,NULL,6,NULL,NULL),(7,'32e32',50000,'Apr','successful','verified','offline','credit','2017-04-06 01:00:00','2017-04-06 04:39:18','2017-04-06 04:39:18',85,NULL,1,NULL,NULL),(8,'677789',10000,'Apr','successful','verified','offline','credit','2017-05-11 01:00:00','2017-05-03 09:46:42','2017-05-03 09:46:42',119,NULL,3,NULL,NULL),(9,'xfgserfgxsdfg',20000,'Sep','successful','verified','offline','credit','2017-05-09 01:00:00','2017-05-06 10:57:10','2017-05-06 10:57:10',121,NULL,6,NULL,NULL),(10,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 01:50:27','2017-05-06 01:50:27',39,NULL,1,NULL,NULL),(11,'CHLOROQUINE',25,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 01:50:27','2017-05-06 01:50:27',39,NULL,1,NULL,NULL),(12,'CHLOROQUINE',10000,'May','successful','verified','offline','debit','2017-03-27 01:00:00','2017-05-06 01:52:59','2017-05-06 01:52:59',39,NULL,1,NULL,NULL),(13,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 02:16:26','2017-05-06 02:16:26',39,NULL,1,NULL,NULL),(14,'CHLOROQUINE',25,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 02:16:26','2017-05-06 02:16:26',39,NULL,1,NULL,NULL),(15,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 02:17:13','2017-05-06 02:17:13',39,NULL,1,NULL,NULL),(16,'CHLOROQUINE',50.0625,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-06 02:17:13','2017-05-06 02:17:13',39,NULL,1,NULL,NULL),(17,'7788999998',20000,'Aug','successful','verified','offline','credit','2017-05-26 01:00:00','2017-05-08 10:14:41','2017-05-08 10:14:41',111,NULL,1,NULL,NULL),(18,'7788999998',116.667,'Aug','successful','verified','offline','credit','2017-05-26 01:00:00','2017-05-08 10:14:41','2017-05-08 10:14:41',111,NULL,1,NULL,NULL),(19,'r53t4',50000,'Aug','successful','verified','offline','credit','2017-05-16 01:00:00','2017-05-14 06:04:50','2017-05-14 06:04:50',122,NULL,5,NULL,NULL),(20,'r53t4',291.667,'Aug','successful','verified','offline','credit','2017-05-16 01:00:00','2017-05-14 06:04:50','2017-05-14 06:04:50',122,NULL,5,NULL,NULL),(21,'12334',50000,'Dec','successful','verified','offline','credit','2017-05-18 01:00:00','2017-05-14 06:17:40','2017-05-14 06:17:40',122,NULL,5,NULL,NULL),(22,'12334',585.035,'Dec','successful','verified','offline','credit','2017-05-18 01:00:00','2017-05-14 06:17:40','2017-05-14 06:17:40',122,NULL,5,NULL,NULL),(23,'65789',50000,'Dec','successful','verified','offline','debit','2017-05-24 01:00:00','2017-05-14 06:51:51','2017-05-14 06:51:51',122,NULL,5,NULL,NULL),(25,'dfgsrfg',10000,'May','successful','verified','offline','credit','2017-03-27 00:00:00','2017-05-20 03:12:43','2017-05-20 03:12:43',39,NULL,3,'Random test :)',NULL),(26,'dfgsrfg',75.1877,'May','successful','verified','offline','credit','2017-03-27 00:00:00','2017-05-20 03:12:43','2017-05-20 03:12:43',39,NULL,3,'Interest',NULL),(27,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-22 11:21:16','2017-05-22 11:21:16',39,NULL,3,'Random test 1 :).',NULL),(28,'CHLOROQUINE',100.376,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-22 11:21:16','2017-05-22 11:21:16',39,NULL,3,'Interest',NULL),(29,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-22 11:37:00','2017-05-22 11:37:00',87,NULL,1,'Random test 1 :).',NULL),(30,'CHLOROQUINE',25,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-22 11:37:00','2017-05-22 11:37:00',87,NULL,1,'Interest',NULL),(31,'CHLOROQUINE',10000,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-25 02:21:09','2017-05-25 02:21:09',87,NULL,1,'Random test 1 :).',NULL),(32,'CHLOROQUINE',50.0625,'May','successful','verified','offline','credit','2017-03-27 01:00:00','2017-05-25 02:21:09','2017-05-25 02:21:09',87,NULL,1,'Interest',NULL),(33,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 10:09:48','2017-05-30 10:09:48',39,NULL,3,'SMS notification charge.',NULL),(34,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 10:10:11','2017-05-30 10:10:11',39,NULL,3,'SMS notification charge.',NULL),(35,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 10:16:50','2017-05-30 10:16:50',39,NULL,3,'SMS notification charge.',NULL),(36,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 10:17:29','2017-05-30 10:17:29',39,NULL,3,'SMS notification charge.',NULL),(37,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 10:18:29','2017-05-30 10:18:29',39,NULL,3,'SMS notification charge.',NULL),(38,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 11:41:57','2017-05-30 11:41:57',39,NULL,3,'SMS notification charge.',NULL),(39,'SEGNOT2017/05/30',2147.85,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 11:57:22','2017-05-30 11:57:22',39,NULL,3,'SMS notification charge.',NULL),(40,'SEGNOT2017/05/30',4.3,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 00:01:07','2017-05-30 00:01:07',39,NULL,3,'SMS notification charge.',NULL),(41,'SEGNOT2017/05/30',2.15,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 00:06:22','2017-05-30 00:06:22',39,NULL,3,'SMS notification charge.',NULL),(42,'SEGNOT2017/05/30',2.15,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 00:10:31','2017-05-30 00:10:31',39,NULL,3,'SMS notification charge.',NULL),(43,'SEGNOT2017/05/30',2.15,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 08:57:14','2017-05-30 08:57:14',152,NULL,1,'SMS notification charge.',NULL),(44,'SEGNOT2017/05/30',2.15,'May','successful','verified','offline','debit','2017-05-30','2017-05-30 08:58:57','2017-05-30 08:58:57',152,NULL,1,'SMS notification charge.',NULL),(45,'SEGNOT2017/05/31',2.15,'May','successful','verified','offline','debit','2017-05-31','2017-05-31 09:06:23','2017-05-31 09:06:23',152,NULL,1,'SMS notification charge.',NULL);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_requests`
--

DROP TABLE IF EXISTS `upgrade_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `new_payment_plan_id` int(10) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upgrade_requests_user_id_foreign` (`user_id`),
  KEY `upgrade_requests_new_payment_plan_id_foreign` (`new_payment_plan_id`),
  CONSTRAINT `upgrade_requests_new_payment_plan_id_foreign` FOREIGN KEY (`new_payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `upgrade_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_requests`
--

LOCK TABLES `upgrade_requests` WRITE;
/*!40000 ALTER TABLE `upgrade_requests` DISABLE KEYS */;
INSERT INTO `upgrade_requests` VALUES (1,'2017-04-20 10:08:16','2017-04-20 10:08:16',3,1,'processing'),(2,'2017-04-20 10:13:07','2017-04-20 10:13:07',3,1,'processing'),(3,'2017-04-20 11:13:16','2017-04-20 11:13:16',3,1,'processing'),(4,'2017-04-20 11:13:37','2017-04-20 11:13:37',3,1,'processing'),(5,'2017-04-20 11:14:09','2017-04-20 11:14:09',3,1,'processing'),(6,'2017-04-20 11:17:34','2017-04-20 11:17:34',3,2,'processing'),(7,'2017-04-20 11:30:14','2017-04-20 11:30:14',3,3,'processing'),(8,'2017-04-20 02:08:53','2017-04-20 02:08:53',3,2,'processing'),(10,'2017-05-03 11:15:14','2017-05-03 11:15:14',3,6,'processing'),(11,'2017-05-03 11:16:06','2017-05-03 11:16:06',3,1,'processing'),(12,'2017-05-03 11:18:10','2017-05-03 11:18:10',3,1,'processing'),(13,'2017-05-12 08:27:11','2017-05-12 08:27:11',3,2,'processing');
/*!40000 ALTER TABLE `upgrade_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_payment_plan`
--

DROP TABLE IF EXISTS `user_payment_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_payment_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `payment_plan_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_payment_plan_user_id_foreign` (`user_id`),
  KEY `user_payment_plan_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `user_payment_plan_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `user_payment_plan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_payment_plan`
--

LOCK TABLES `user_payment_plan` WRITE;
/*!40000 ALTER TABLE `user_payment_plan` DISABLE KEYS */;
INSERT INTO `user_payment_plan` VALUES (3,'2017-03-25 01:08:45','2017-03-25 01:08:45',3,6),(8,'2017-03-26 02:05:41','2017-03-26 02:05:41',17,6),(9,'2017-04-04 10:14:06','2017-05-09 02:28:05',39,3),(10,'2017-04-04 10:22:18','2017-05-08 10:07:07',57,1),(11,'2017-04-05 09:30:51','2017-04-25 10:20:54',60,1),(14,'2017-04-06 02:27:32','2017-04-13 03:57:53',85,5),(15,'2017-04-15 01:37:41','2017-04-15 01:37:41',87,1),(16,'2017-04-18 09:39:43','2017-05-08 04:00:41',89,5),(17,'2017-04-18 10:27:19','2017-04-18 10:27:19',91,4),(18,'2017-04-19 09:03:30','2017-04-19 09:54:06',108,1),(19,'2017-04-19 02:04:47','2017-05-08 10:18:56',111,3),(20,'2017-04-19 09:11:58','2017-04-19 09:11:58',112,1),(21,'2017-05-03 09:45:12','2017-05-03 10:35:56',119,4),(22,'2017-05-03 09:59:16','2017-05-08 03:59:10',121,3),(23,'2017-05-10 02:14:09','2017-05-14 05:27:07',122,5),(24,'2017-05-11 04:00:05','2017-05-11 04:00:05',123,5),(25,'2017-05-11 04:19:22','2017-05-11 04:19:22',127,1),(26,'2017-05-12 04:56:39','2017-05-12 04:56:39',130,4),(27,'2017-05-12 08:22:09','2017-05-12 08:22:09',131,3),(28,'2017-05-12 03:55:47','2017-05-12 03:55:47',134,3),(29,'2017-05-12 04:03:19','2017-05-12 04:03:19',135,5),(30,'2017-05-17 01:51:29','2017-05-17 01:51:29',139,4),(31,'2017-05-17 01:58:20','2017-05-17 01:58:20',141,5),(32,'2017-05-17 02:37:49','2017-05-17 02:37:49',144,9),(33,'2017-05-19 10:48:59','2017-05-19 10:48:59',146,5),(34,'2017-05-22 05:11:17','2017-05-22 05:11:17',147,3),(35,'2017-05-23 07:34:51','2017-05-23 07:34:51',148,3),(36,'2017-05-25 05:47:28','2017-05-25 06:07:15',149,1),(37,'2017-05-26 10:26:45','2017-05-26 10:26:45',150,2),(38,'2017-05-29 02:22:05','2017-05-29 02:22:05',151,5),(39,'2017-05-30 08:51:00','2017-05-30 08:51:00',152,1);
/*!40000 ALTER TABLE `user_payment_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `othernames` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_of_origin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residential_address` text COLLATE utf8mb4_unicode_ci,
  `postal_address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `identification_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `application_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referral_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_telephone_no_unique` (`telephone_no`),
  UNIQUE KEY `users_telephone_no_1_unique` (`telephone_no_1`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=153 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,NULL,NULL,NULL,'Okunade Mubarak Babajide','Male','20/09/1994',NULL,'Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.',NULL,'mubarakokunade@yahoo.com','08090524193','07089371233','mb2017','f30aa7a662c728b7407c54ae6bfd27d1',NULL,'localhost.com/uploads/images/xyz.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-03-25 01:08:45','2017-03-26 07:16:45'),(7,NULL,NULL,NULL,'admin2','Male','02/04/1993',NULL,NULL,NULL,NULL,NULL,NULL,'admin_2@email.com','415642516415456',NULL,'admin2@email.com','c84258e9c39059a89ab77d846ddab909',NULL,'gjgfjgjg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-03-25 01:40:12','2017-03-25 04:17:26'),(10,NULL,NULL,NULL,'Admin Super',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin1@email.com','71e23baee837c5d2952f93a29b113cab',NULL,NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-03-25 15:05:32','2017-03-25 15:05:32'),(17,NULL,NULL,NULL,'Okunade Mubarak B','Male','20/09/1994',NULL,'Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.',NULL,'mubarak.okunade@yahoo.com','08090524194','0708937233','mb_2017','eb0fbeb8419885b4d75491f6e12f1baf','approved','localhost.com/uploads/images/xyz.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-03-26 02:05:41','2017-03-26 02:05:41'),(18,NULL,NULL,NULL,'admin3','Male','02/04/1994',NULL,NULL,NULL,NULL,NULL,NULL,'02/04/1994','07089371235',NULL,'admin3','84ff2c4b8b18c28f042557c0637c8528',NULL,'localhost.com/uploads/images/xyz.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-03-28 02:29:00','2017-03-28 02:29:00'),(39,NULL,'Ogunsolaa','Timilehin Oyelekee','oyeleke','male','1/02/2016',NULL,'single','osun','nigerian','mattew falade',NULL,'timileyinogunsola@gmail.com','08089650859',NULL,'SEG309036','c46cd3dee1a2dd5e4e46e805e2553f63','approved',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-04 10:14:06','2017-05-30 01:07:23'),(57,NULL,NULL,NULL,'oyeannu','male','1/02/2016',NULL,'single','osun','nigerian','mattew falad',NULL,'timileyinogunso@gmail.com','08089650858',NULL,'SEGUSER6924','ab452534c5ce28c4fbb0e102d4a4fb2e','approved','kinka.com',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-04 10:22:18','2017-05-08 10:07:07'),(60,NULL,NULL,NULL,'oyea','male','1/02/2016',NULL,'single','osun','nigerian','mattew falad',NULL,'timileyinoguns@gmail.com','08089650850',NULL,'SEGUSER8828','832688ff5af6c6dfef974773740ef2b5','approved','kink.com',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-05 09:30:51','2017-04-25 10:20:54'),(64,NULL,NULL,NULL,'admin4','Male','14/5/6',NULL,NULL,NULL,NULL,NULL,NULL,'admin4@email.com','456123',NULL,'admin4','fc1ebc848e31e0a68e868432225e3c82',NULL,'xyz.com/uploads/images/fwsf.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-05 02:38:31','2017-04-05 02:38:31'),(82,NULL,NULL,NULL,'admin6','male','2017-04-07',NULL,NULL,NULL,NULL,NULL,NULL,'admin6@email.com','582',NULL,'admin6@email.com','cfeffc883de264920b8277d4aea0cb05',NULL,'xyz.com/uploads/images/fwsf.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-05 09:05:53','2017-04-06 12:48:39'),(83,NULL,NULL,NULL,'admin7','male','2017-04-03',NULL,NULL,NULL,NULL,NULL,NULL,'admin7@email.com','900',NULL,'admin7@email.com','788073cefde4b240873e1f52f5371d7d',NULL,'xyz.com/uploads/images/fwsf.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-06 09:10:11','2017-04-06 09:10:11'),(84,NULL,NULL,NULL,'admin8','male','2017-04-14',NULL,NULL,NULL,NULL,NULL,NULL,'admin8@email.com','457',NULL,'admin8@email.com','8762eb814817cc8dcbb3fb5c5fcd52e0',NULL,'xyz.com/uploads/images/fwsf.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-06 01:17:02','2017-04-06 01:17:02'),(85,NULL,NULL,NULL,'customer1','female','2017-04-08',NULL,'married','Oyo','Nigeria','customer1',NULL,'customer1@email.com','456','654','SEGUSER6488','9327969053c0068dd9e07c529866b94d','approved','xyz.com/uploads/images/fwsf.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-06 02:27:32','2017-04-13 03:57:53'),(86,NULL,NULL,NULL,'admin9','Male','02/04/1984',NULL,NULL,NULL,NULL,NULL,NULL,'admin9@email.com','4648856',NULL,'admin9','eed57216df3731106517ccaf5da2122d',NULL,NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-12 02:51:25','2017-04-13 04:55:58'),(87,NULL,NULL,NULL,'oyeale','male','1/02/2016',NULL,'single','osun','nigerian','mattew falad','mattew falad','timileyinogns@gmail.com','08084650850',NULL,NULL,NULL,'not approved',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-15 01:37:41','2017-04-15 01:37:41'),(88,NULL,NULL,NULL,'admin10','Male','4/7/8',NULL,NULL,NULL,NULL,NULL,NULL,'admin10@email.com','78946454',NULL,'admin10','4fbd41a36dac3cd79aa1041c9648ab89',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/mviuvHa7mlJSVN3sLmcdt2XjBRyableRS8SOxwKe.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 09:22:50','2017-04-18 09:22:50'),(89,NULL,NULL,NULL,'customer2','Male','8/9/4',NULL,'Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.','11, Fola agoro street, Shomolu, Lagos Nigeria.','customer2@email.com','567+8675','25148','SEGUSER3276','ea06ed6e977637a81ea9d22b3090e28a','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/BBzX1dI71oP1OaDTzmTLyXAecxHy4cG4IK6tOz86.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 09:39:43','2017-05-08 04:00:41'),(91,NULL,NULL,NULL,'customer3','male','2017-04-18',NULL,'married','ABIA','NGA','customer 3\'s address','customer 3\'s address','customer3@email.com','48674168416','86741656555','customer3','033f7f6121501ae98285ad77f216d5e7','approved',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 10:27:19','2017-04-18 10:27:19'),(93,NULL,NULL,NULL,'admin11','Male','7/4/1',NULL,NULL,NULL,NULL,NULL,NULL,'admin11@email.com','46458',NULL,'admin11','dcca2ed1630582435afa9d42ce361eb4',NULL,NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 11:04:06','2017-04-18 11:04:06'),(103,NULL,NULL,NULL,'admin12','Male','8/5/2',NULL,NULL,NULL,NULL,NULL,NULL,'admin12@email.com','874516546',NULL,'admin12','1844156d4166d94387f1a4ad031ca5fa',NULL,NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 01:27:08','2017-04-18 01:27:08'),(104,NULL,NULL,NULL,'admin14','Male','7/5/3',NULL,NULL,NULL,NULL,NULL,NULL,'admin14@email.com','567',NULL,'admin14','bdc8341bb7c06ca3a3e9ab7d39ecb789',NULL,NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 01:30:42','2017-04-18 01:30:42'),(105,NULL,NULL,NULL,'admin15','Male','9/5/1',NULL,NULL,NULL,NULL,NULL,NULL,'admin15@email.com','68',NULL,'admin15','b26c077af60ba02d12c8436110256029',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/3MGAlaytvJLAY1foQEpPXECyeqpKKqwL1LF0XA18.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-18 02:37:56','2017-04-18 02:37:56'),(107,NULL,NULL,NULL,'admin16','male','2017-04-04',NULL,NULL,NULL,NULL,NULL,NULL,'admin16@email.com','715415',NULL,'admin16@email.com','9071e0ca7e4964a5cc69201ba2743650',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/JqWg3zI4j8oWXbPJkzjcsXmczfxDUSTUYQzFAurP.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-19 08:37:43','2017-04-19 08:47:44'),(108,NULL,NULL,NULL,'customer4','male','2017-04-13',NULL,'single','OYO','NGA','customer 4\'s residence','customer 4\'s p.o.box','customer4@email.com','84164','7855616','SEGUSER3042','5c8e07660e3000f141dda8b83107ed6e','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/nvGpcWyFNux3wgpicfBPc7E40Nzhuq0oAY4K394A.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-19 09:03:30','2017-04-19 09:54:06'),(109,NULL,NULL,NULL,'admin17','male','2017-04-08',NULL,NULL,NULL,NULL,NULL,NULL,'admin17@email.com','85686',NULL,'admin17@email.com','d5133c970ad3a99c2248fed76970d06c',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/rAhxBqWSoX8G1GdwCKAp3dOcOPigh5V7JnN4YJdX.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-19 12:09:52','2017-05-08 10:17:16'),(111,NULL,NULL,NULL,'temi','female','12/03/2016',NULL,'married','osun','nigerian','mattew falade','mattew falade','oyeogunsola@gmail.com','08089650899',NULL,'SEGUSER3764','06a81a4fb98d149f2d31c68828fa6eb2','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/D8XJJ9srVylcLjBqVj25DlOgG06B2CYxsGNYv9xd.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-19 02:04:47','2017-05-08 10:18:56'),(112,NULL,NULL,NULL,'customer6','male','2017-04-08',NULL,'married','CROSS RIVER','NGA','sdrsev','crfasfs','customer6@email.com','6856','648685','customer16@email.com','537d4b76ba087f39cb33256c09571dab','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/Q5HfSBP5bFbu0ocsOULV4UmtkbuZB67u4jTFRAjQ.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-04-19 09:11:58','2017-04-19 09:11:58'),(113,NULL,NULL,NULL,'temi','male','2017-05-02',NULL,NULL,NULL,NULL,NULL,NULL,'temi@temi.com','56789',NULL,'temi','bc0cbcacee08d345efe88f4e447db6ab',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/kEzjIPrfKPc9D5izLmiBSuI7R0m2Y9WLo2nfxzCK.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-03 09:33:53','2017-05-03 09:33:53'),(116,NULL,NULL,NULL,'yemi','female','2017-05-09',NULL,NULL,NULL,NULL,NULL,NULL,'yemi@yemi.com','465789',NULL,'yemi','050e8f93436b081a95a58a86df825509',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/Z6JcbYsw7ve4gMOEEsKrYVmjFhHZK1hkNGLNNJuV.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-03 09:36:18','2017-05-03 09:36:18'),(119,NULL,NULL,NULL,'timi','female','2017-05-03',NULL,'married','JIGAWA','BGD','hjjj','jjjj','timi@timi.com','543','432','SEGUSER8653','6646b06b90bd13dabc11ddba01270d23','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/085j69zDdsRDa2tu8b6yUek3FPhaSyrWAQb9WfWP.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-03 09:45:12','2017-05-03 10:35:56'),(120,NULL,NULL,NULL,'admin18','male','2017-05-20',NULL,NULL,NULL,NULL,NULL,NULL,'admin18@email.com','5266256',NULL,'admin18@email.com','5007007bf0d84200644731d5d3bf9aff',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/SNZpEv80WUZykRbuxwnl8hEgqOoueXpvFVDnsozM.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-03 09:54:57','2017-05-03 09:54:57'),(121,NULL,NULL,NULL,'customer10','male','2017-05-30',NULL,'married','BORNO','NGA','jkhfnkjdnfxsklrnJKQMJKN7464687',',KDFNHJM,SNDSNDJNSJ5645645','customer10@email.com','85674168541','856584156','SEGUSER8266','56584778d5a8ab88d6393cc4cd11e090','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/0WxaIbhYSf74eAfW7S42A3JX02YyJ7eTQZUbSnvC.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-03 09:59:16','2017-05-08 03:59:10'),(122,NULL,NULL,NULL,'Odetunde Temiloluwa','male','1/2/3',NULL,'single','OSUN','NGA','cdsfcsd','cdsvfsdcf','odetundetemiloluwa@gmail.com','08166287789','452248','SEGUSER59211','cadbf35eab099692d7651ce74fe4522e','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/lNmiq9PDBJpGKzqcZVlcrhT0ukWM1FwwcE0NIASQ.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-10 02:14:09','2017-05-14 05:27:07'),(123,NULL,NULL,NULL,'ogunsola Timileyin','male','2017-12-31',NULL,'radio','osun','nigeria','mattew falade','mattew falade','oy@gmail.com','08023231051',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/BmdNJxJjEMBi1XnDIQgK4dR6wT2HiE5cVmCfhqCt.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-11 04:00:05','2017-05-11 04:00:05'),(127,NULL,NULL,NULL,'oyewale','male','1/02/2016',NULL,'single','osun','nigerian','mattew falad','mattew falad','timileogns@gmail.com','08084650852',NULL,NULL,NULL,'not approved',NULL,NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-11 04:19:22','2017-05-11 04:19:22'),(130,NULL,NULL,NULL,'rvvcfcl,dc','female','2017-05-10',NULL,'radio','rwgfviencfvjdw','nigeria','fwrefkerwmneldm','rwdfjrlmfkwemf','rgvfoetkldsvf','rlcvjfdccnv',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/0OcRIuF8kLLk6OzTqPSlWv4EoM2Ofmm5I4UI1Zs7.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-12 04:56:39','2017-05-12 04:56:39'),(131,NULL,NULL,NULL,'eirobfmlgewfwe','male','2017-05-18',NULL,'radio','wrfdvsrfelcvjer','nigeria','wdfcvwdsjvfhufw','wregfvfcdcijfric','wvsmcwefcwd',';rwfvwregfv',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/LHQFBD6VMpVx360JCiwF5KoluVSRvIiRhw7lcj79.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-12 08:22:09','2017-05-12 08:22:09'),(133,NULL,NULL,NULL,'El- Zoro','male','1/4/9',NULL,NULL,NULL,NULL,NULL,NULL,'el-zoro@email.com','53438486',NULL,'el-zoro@email.com','61b1870018fb107b7ab4d4a14d435c77',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/6CEpGW0dLM3mIHoFtNzq6z5c4Vra3ExRa1funJqO.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-12 02:54:11','2017-05-12 02:54:11'),(134,NULL,NULL,NULL,'rwvunrmdfslx rwgvirwjvjrvmv rfvivueiofureuv','female','2017-05-17',NULL,'radio','rvjnrocvjirewvf','nigeria','rvuwrkvnrwouvrow','rwvigvhworrowrfur','rfcvirodwcirrir','wrfvuhnwuoc',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/YGgwW6BUc74zRxUZiJgPwIbUPo2LgVHsiOFAAhty.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-12 03:55:47','2017-05-12 03:55:47'),(135,NULL,NULL,NULL,'Customer Zoro','male','1/8/9',NULL,'single','Outside Nigeria','ITA','nk tgcn','jnked ,tjced','customerzoro@email.com','45854538412','56854185485','customerzoro@email.com','59d8a60d1aaafc4299fd4d28c0c368f8','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/qMa5cYB3rbZUpUhOr5x5PnSYOdLY97ePtjn2QFMq.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-12 04:03:19','2017-05-12 04:03:19'),(137,NULL,NULL,NULL,'admin 19','male','4/5/6',NULL,NULL,NULL,NULL,NULL,NULL,'admin19@email.com','486668',NULL,'admin19@email.com','fb82367ca705634db5ff2e896a7d0334',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/YY5r7qHkl2ywzreqheKxYq1fU4eTU7PgOVinPuuF.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-14 01:33:00','2017-05-14 01:33:00'),(138,NULL,NULL,NULL,'admin 20','female','7/4/1',NULL,NULL,NULL,NULL,NULL,NULL,'admin20@email.com','1555',NULL,'admin20@email.com','b29b1de827ef626d07210f0a2713f782',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/R1CE2bpSaL1pMMbt3rCGhgKhqjkopzGN8HU8wHjf.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-14 01:34:54','2017-05-14 01:34:54'),(139,NULL,NULL,NULL,'Adebayo Kenny','male','2017-05-03',NULL,'married','Outside Nigeria','AFG','dfdgfdg','gfsd','kenny032002@yahoo.com','08095595595','08173157325','kenny','827ccb0eea8a706c4c34a16891f84e7b','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/gSEaqYNRqse8nWSYqvgEeNgoww2YNWQ5cnCY7rO9.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-17 01:51:29','2017-05-17 01:51:29'),(140,NULL,NULL,NULL,'Admin 21','male','4/3/7',NULL,NULL,NULL,NULL,NULL,NULL,'admin21@email.com','751651',NULL,'admin21@email.com','e4d553122509d4c2d67317f365c7bd83',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/EMhZJiP23ma3F80sVHVr9lLK6MLP7VaWlw7HUrMu.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-17 01:55:53','2017-05-27 04:30:31'),(141,NULL,NULL,NULL,'Customer X','male','7/4/6',NULL,'married','BAUCHI','NGA','hrvsdgsd','dfbsgvd','customerx@email.com','84189616','745848491','sdfsd','33530ebfec6e2826da22b97ea94441a3','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/921kGOXhT3YASZGWFJYbmBDjAdrj53EdRjRMHyWP.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-17 01:58:20','2017-05-17 01:58:20'),(144,NULL,NULL,NULL,'odetunde toluwalashe esther','female','2017-05-03',NULL,'radio','osun','nigeria','lag','8765','odetundelashe@gmail.com','08064476683',NULL,'SEGUSER48927','e09baed818bb105cc3f3451d6aa93f2d','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/VnIK7XYpXeJeFju1q5TUn6Y5qr7oMEUo2h6rOzYj.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-17 02:37:49','2017-05-17 02:39:42'),(145,NULL,NULL,NULL,'Akindiose Omololu','male','1983-01-11',NULL,NULL,NULL,NULL,NULL,NULL,'o.akindiose@segurohousingcooperative.com','08024241544',NULL,'oakindiose','23b1abf2d2108aa91507600ad2babad0',NULL,'http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/c1TvruobpWh38kQeXwigWVCY29OYVZwfzMU9GnW9.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-17 03:24:18','2017-05-17 03:24:18'),(146,'Mr','16','customer',NULL,'Male',NULL,'1/02/8','Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.','11, Fola agoro street, Shomolu, Lagos Nigeria.','customer15@email.com','24510','5987',NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/y6zjlPB5ZcXbDoSvpyoBdZlrTAZdBxCpTXkjQauF.jpeg','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/identification/jm1Z1S5OoPJSdWz7n1mzOw736ijz1Gcbu0iuYKYX.jpeg','1','1','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-19 10:48:59','2017-05-19 10:48:59'),(147,NULL,NULL,NULL,'vbetbv tbvre','female','2017-05-18',NULL,'married','Akwa Ibom','DZ','tgbrgtsvs','tbrvsdcr','tbgr','tgvbte',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/grwVv2RI8PqByRlGdpfrCe6FhuRVvpc8pEoVeILm.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-22 05:11:17','2017-05-22 05:11:17'),(148,NULL,NULL,NULL,'sfboefmvfc ksdnvifvn','female','2017-05-10',NULL,'married','Adamawa','AS','ervmwivmfkv','rwkvmrwoivf','rvjfrvmfs','fvnrwinvrk',NULL,NULL,NULL,'not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/0yEy1OT2VSIrwQYVMyjjs7Os8LEmzPwSheEAx86s.jpeg',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-23 07:34:51','2017-05-23 07:34:51'),(149,NULL,NULL,NULL,'Odetunde Temiloluwa','female','2017-05-19',NULL,'married','Kaduna','AT','House 5, Fidiso Estate,Cletus Adobu Av.','House 5, Fidiso Estate,Cletus Adobu Av.','odetundetem','8166287789',NULL,'SEGUSER23988','125afb06ef365c0991aa632cd76104e1','approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/S4TLT81REycpaucCPLmSyPSD6SVe4ypzUMMLeMl1.png',NULL,NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEyMiwiaXNzIjoiaHR0cDpcL1wvYXNrLm5ldC5uZ1wvc2VndXJvXC9zZWd1cm9fYXBpXC9wdWJsaWNcL2FwaVwvdjFcL2FkbWluXC9sb2dpbiIsImlhdCI6MTQ5NTc5MzkxMiwiZXhwIjoxNDk1ODgwMzEyLCJuYmYiOjE0OTU3OTM5MTIsImp0aSI6IjFjNTk2ZWQ4Mjk2OWEyYzk3OTc4MmVhYzY0ZjJjMDA0In0.Y2O8CEh-gfsPqLbOq3-lBLuGgDh4P_RIR7Rm3bXuAFE','2017-05-25 05:47:28','2017-05-25 06:07:15'),(150,NULL,NULL,NULL,'frg ggyy','female','0008-06-06',NULL,'married','Outside Nigeria','DZA','ghghhghjhjhj','hjhhjjhjh','ghgg@ghhh.com','hghg68778','ghhghg','tolu','e4443c52948edad6132f34b6378a9901','not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/gHjgMF0FP3F7RcHN4PVWFhSpVp3oyauoWZmiwsfj.png',NULL,NULL,NULL,NULL,'2017-05-26 10:26:45','2017-05-26 10:26:45'),(151,NULL,NULL,NULL,'22 customer','male','2017-05-11',NULL,'married','Outside Nigeria','AFG','sdvsdacdcfsdcasdca','dssdcfasascfsdfaseas','customer22@email.com','23666238414','541561035105231','customer22@email.com','89e77d86f3857e7ee9d7366f806cf0ad','not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/h2CZ5lhBIzOtydq4vw6670vjCWrVamXCUayUDhMf.jpeg',NULL,NULL,NULL,NULL,'2017-05-29 02:22:05','2017-05-29 02:22:05'),(152,'mrs','obasoro','toyin',NULL,'female',NULL,'2017-05-02','married','Ekiti','NE','194, hert maculay way','222','ogunmolalamide@gmail.com','08166916554',NULL,'SEG1064210','7774454112c26855582d154a5d5f81c0','not approved','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/profile_pics/rc1ZiBHloEaJVISwWONrwCQQKujUTR33q3h8dFms.jpeg','http://ask.net.ng/seguro/seguro_api/storage/app/public/uploads/identification/spwgyoDY6znaAuK3bgnkTAXTNsBmSPCZxdoKTpYe.jpeg','9999','02',NULL,'2017-05-30 08:51:00','2017-05-31 09:06:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-31  9:43:14
