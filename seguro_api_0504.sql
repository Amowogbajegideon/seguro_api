-- MySQL dump 10.14  Distrib 5.5.44-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: seguro_api
-- ------------------------------------------------------
-- Server version	5.5.44-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`),
  UNIQUE KEY `admins_telephone_no_unique` (`telephone_no`),
  UNIQUE KEY `admins_username_unique` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employment_information`
--

DROP TABLE IF EXISTS `employment_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employment_information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `employment_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_employer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `employment_information_user_id_foreign` (`user_id`),
  CONSTRAINT `employment_information_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employment_information`
--

LOCK TABLES `employment_information` WRITE;
/*!40000 ALTER TABLE `employment_information` DISABLE KEYS */;
INSERT INTO `employment_information` VALUES (2,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-03-25 01:08:45','2017-03-25 01:08:45',3),(7,'unemployed','School','Student','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','2017-03-26 02:05:41','2017-03-26 02:05:41',17),(8,'employed','iqube','software engineer','makoko yaba','2017-04-04 10:14:06','2017-04-04 10:14:06',39),(9,'employed','iqube','software engineer','makoko yaba','2017-04-04 10:22:18','2017-04-04 10:22:18',57),(10,'employed','iqube','software engineer','makoko yaba','2017-04-05 09:30:51','2017-04-05 09:30:51',60);
/*!40000 ALTER TABLE `employment_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feedback_user_id_foreign` (`user_id`),
  CONSTRAINT `feedback_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interested_users`
--

DROP TABLE IF EXISTS `interested_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interested_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `property_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `interested_users_user_id_foreign` (`user_id`),
  KEY `interested_users_property_id_foreign` (`property_id`),
  CONSTRAINT `interested_users_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `interested_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interested_users`
--

LOCK TABLES `interested_users` WRITE;
/*!40000 ALTER TABLE `interested_users` DISABLE KEYS */;
INSERT INTO `interested_users` VALUES (1,'2017-03-27 08:39:45','2017-03-27 08:39:45',17,1),(2,'2017-03-27 09:11:46','2017-03-27 09:11:46',3,1);
/*!40000 ALTER TABLE `interested_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (19,'2014_10_12_000000_create_users_table',1),(20,'2014_10_12_100000_create_password_resets_table',1),(21,'2017_03_16_211833_create_admins_table',1),(22,'2017_03_16_211931_create_properties_table',1),(23,'2017_03_16_212032_create_payment_plans_table',1),(24,'2017_03_16_213107_create_transactions_table',1),(25,'2017_03_16_214253_create_feedback_table',1),(26,'2017_03_16_220346_create_property_pictures_table',1),(27,'2017_03_16_220806_create_sent_emails_table',1),(28,'2017_03_16_221332_create_employment_information_table',1),(29,'2017_03_16_221614_create_interested_users_table',1),(30,'2017_03_16_221905_create_upgrade_requests_table',1),(31,'2017_03_16_223159_create_user_payment_plan_table',1),(32,'2017_03_17_085601_add_status_to_upgrade_requests_table',1),(33,'2017_03_22_143250_create_savings_table',1),(34,'2017_03_23_200155_create_notifications_table',1),(35,'2017_03_24_235753_create_roles_table',1),(36,'2017_03_25_001520_create_role_user_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sms` text COLLATE utf8mb4_unicode_ci,
  `dashboard` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `sent_email_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_user_id_foreign` (`user_id`),
  KEY `notifications_sent_email_id_foreign` (`sent_email_id`),
  CONSTRAINT `notifications_sent_email_id_foreign` FOREIGN KEY (`sent_email_id`) REFERENCES `sent_emails` (`id`),
  CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_plans`
--

DROP TABLE IF EXISTS `payment_plans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_plans` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_plan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_plans`
--

LOCK TABLES `payment_plans` WRITE;
/*!40000 ALTER TABLE `payment_plans` DISABLE KEYS */;
INSERT INTO `payment_plans` VALUES (1,'SEG10',10000,'2017-03-17 12:24:55','2017-03-17 12:24:55'),(2,'SEG20',20000,'2017-03-17 12:24:55','2017-03-17 12:24:55'),(3,'SEG30',30000,'2017-03-17 12:25:50','2017-03-17 12:25:50'),(4,'SEG40',40000,'2017-03-17 12:25:50','2017-03-17 12:25:50'),(5,'SEG50',50000,'2017-03-17 12:26:26','2017-03-17 12:26:26'),(6,'SEG60',60000,'2017-03-17 12:26:26','2017-03-17 12:26:26');
/*!40000 ALTER TABLE `payment_plans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `properties`
--

DROP TABLE IF EXISTS `properties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `coordinates` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_bedrooms` int(11) DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_of_offer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `properties`
--

LOCK TABLES `properties` WRITE;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
INSERT INTO `properties` VALUES (1,'GUBABI SAFE PLAZA','194, Herbet Macaulay way, Makoko, Yaba, Lagos.','6.4939764N,3.3815113E','20000000','150',10,'Its a nice place previous occupants had no complaints.','2','available','2017-03-17 14:11:35','2017-03-17 14:11:35'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `property_pictures`
--

DROP TABLE IF EXISTS `property_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `property_pictures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `property_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `property_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_pictures_property_id_foreign` (`property_id`),
  CONSTRAINT `property_pictures_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `property_pictures`
--

LOCK TABLES `property_pictures` WRITE;
/*!40000 ALTER TABLE `property_pictures` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  KEY `role_user_role_id_foreign` (`role_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_user`
--

LOCK TABLES `role_user` WRITE;
/*!40000 ALTER TABLE `role_user` DISABLE KEYS */;
INSERT INTO `role_user` VALUES (2,'2017-03-25 01:08:45','2017-03-25 01:08:45',3,3),(5,'2017-03-25 01:40:12','2017-03-25 04:17:26',7,2),(7,'2017-03-25 15:09:41','2017-03-25 15:09:41',10,1),(12,'2017-03-26 02:05:41','2017-03-26 02:05:41',17,3),(13,'2017-03-28 02:29:00','2017-03-28 02:29:00',18,2),(14,'2017-03-29 03:54:00','2017-03-29 03:54:00',19,2),(15,'2017-04-04 10:14:06','2017-04-04 10:14:06',39,3),(16,'2017-04-04 03:00:59','2017-04-04 03:00:59',45,2),(17,'2017-04-04 03:02:40','2017-04-04 03:02:40',47,2),(18,'2017-04-04 03:11:19','2017-04-04 03:11:19',48,2),(19,'2017-04-04 07:44:39','2017-04-04 07:44:39',49,2),(20,'2017-04-04 10:22:18','2017-04-04 10:22:18',57,3),(21,'2017-04-05 09:30:51','2017-04-05 09:30:51',60,3);
/*!40000 ALTER TABLE `role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Super Admin','2017-03-25 13:58:31','2017-03-25 13:58:31'),(2,'Admin','2017-03-25 13:58:31','2017-03-25 13:58:31'),(3,'User','2017-03-25 14:01:03','2017-03-25 14:01:03');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `savings`
--

DROP TABLE IF EXISTS `savings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `savings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month_paid` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principal` double DEFAULT NULL,
  `principal_prime` double DEFAULT NULL,
  `interest` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `payment_plan_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `savings_user_id_foreign` (`user_id`),
  KEY `savings_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `savings_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `savings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `savings`
--

LOCK TABLES `savings` WRITE;
/*!40000 ALTER TABLE `savings` DISABLE KEYS */;
INSERT INTO `savings` VALUES (1,'April',600,601.5,1.5,'2017-03-27 01:34:18','2017-03-27 01:34:18',3,6);
/*!40000 ALTER TABLE `savings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sent_emails`
--

DROP TABLE IF EXISTS `sent_emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sent_emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sent_emails_user_id_foreign` (`user_id`),
  CONSTRAINT `sent_emails_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sent_emails`
--

LOCK TABLES `sent_emails` WRITE;
/*!40000 ALTER TABLE `sent_emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `sent_emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payment_reference` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `month_paid_for` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verification_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `debit/credit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_paid` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `property_id` int(10) unsigned DEFAULT NULL,
  `payment_plan_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_user_id_foreign` (`user_id`),
  KEY `transactions_property_id_foreign` (`property_id`),
  KEY `transactions_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `transactions_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `transactions_property_id_foreign` FOREIGN KEY (`property_id`) REFERENCES `properties` (`id`),
  CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` VALUES (1,'TREF180317SEG601',60000,'Mar','successful','verified','offline','credit','2017-03-18 05:43:16','2017-03-18 05:43:16','2017-03-18 06:01:43',3,1,6),(2,'TREF180317SEG602',60000,'Mar','successful','verified','offline','credit','2017-03-18 06:01:21','2017-03-18 06:01:21','2017-03-18 06:01:21',3,1,6),(3,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-26 23:00:00','2017-03-27 01:16:39','2017-03-27 01:16:39',3,1,6),(4,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-26 23:00:00','2017-03-27 01:31:12','2017-03-27 01:31:12',3,1,6),(5,'dfgsrfg',60000,'April','successful','verified','offline','credit','2017-03-26 23:00:00','2017-03-27 01:34:18','2017-03-27 01:34:18',3,1,6);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_requests`
--

DROP TABLE IF EXISTS `upgrade_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `new_payment_plan_id` int(10) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upgrade_requests_user_id_foreign` (`user_id`),
  KEY `upgrade_requests_new_payment_plan_id_foreign` (`new_payment_plan_id`),
  CONSTRAINT `upgrade_requests_new_payment_plan_id_foreign` FOREIGN KEY (`new_payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `upgrade_requests_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_requests`
--

LOCK TABLES `upgrade_requests` WRITE;
/*!40000 ALTER TABLE `upgrade_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `upgrade_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_payment_plan`
--

DROP TABLE IF EXISTS `user_payment_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_payment_plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `payment_plan_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_payment_plan_user_id_foreign` (`user_id`),
  KEY `user_payment_plan_payment_plan_id_foreign` (`payment_plan_id`),
  CONSTRAINT `user_payment_plan_payment_plan_id_foreign` FOREIGN KEY (`payment_plan_id`) REFERENCES `payment_plans` (`id`),
  CONSTRAINT `user_payment_plan_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_payment_plan`
--

LOCK TABLES `user_payment_plan` WRITE;
/*!40000 ALTER TABLE `user_payment_plan` DISABLE KEYS */;
INSERT INTO `user_payment_plan` VALUES (3,'2017-03-25 01:08:45','2017-03-25 01:08:45',3,6),(8,'2017-03-26 02:05:41','2017-03-26 02:05:41',17,6),(9,'2017-04-04 10:14:06','2017-04-04 10:14:06',39,1),(10,'2017-04-04 10:22:18','2017-04-04 10:22:18',57,1),(11,'2017-04-05 09:30:51','2017-04-05 09:30:51',60,1);
/*!40000 ALTER TABLE `user_payment_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fullname` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marital_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state_of_origin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `residential_address` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone_no_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_telephone_no_unique` (`telephone_no`),
  UNIQUE KEY `users_telephone_no_1_unique` (`telephone_no_1`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (3,'Okunade Mubarak Babajide','Male','20/09/1994','Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.','mubarakokunade@yahoo.com','08090524193','07089371233','mb2017','f30aa7a662c728b7407c54ae6bfd27d1',NULL,'localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-25 01:08:45','2017-03-26 07:16:45'),(7,'admin2','Male','02/04/1993',NULL,NULL,NULL,NULL,'admin_2@email.com','415642516415456',NULL,'admin2@email.com','c84258e9c39059a89ab77d846ddab909',NULL,'gjgfjgjg','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-25 01:40:12','2017-03-25 04:17:26'),(10,'Admin Super',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'admin1@email.com','71e23baee837c5d2952f93a29b113cab',NULL,NULL,'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-25 15:05:32','2017-03-25 15:05:32'),(17,'Okunade Mubarak B','Male','20/09/1994','Single','Oyo','Nigerian','11, Fola agoro street, Shomolu, Lagos Nigeria.','mubarak.okunade@yahoo.com','08090524194','0708937233','mb_2017','eb0fbeb8419885b4d75491f6e12f1baf','approved','localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-26 02:05:41','2017-03-26 02:05:41'),(18,'admin3','Male','02/04/1994',NULL,NULL,NULL,NULL,'02/04/1994','07089371235',NULL,'admin3','84ff2c4b8b18c28f042557c0637c8528',NULL,'localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-28 02:29:00','2017-03-28 02:29:00'),(19,'admin4','Male','02/04/1994',NULL,NULL,NULL,NULL,'admin4@email.com','07089371234',NULL,'admin4','fba54f07332ad667fce71ac27e82b0a7',NULL,'localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-03-29 03:54:00','2017-03-29 03:54:00'),(39,'oyeleke','male','1/02/2016','single','osun','nigerian','mattew falade','timileyinogunsola@gmail.com','08089650859',NULL,NULL,NULL,'not approved','kinkan.com','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 10:14:06','2017-04-04 10:14:06'),(45,'admin11','Male','02/04/1988843',NULL,NULL,NULL,NULL,'admin10@email.com','034645635',NULL,'admin11','e020590f0e18cd6053d7ae0e0a507609',NULL,'localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 03:00:59','2017-04-04 03:00:59'),(47,'admin12','Male','02/04/198768445',NULL,NULL,NULL,NULL,'admin12@email.com','45687975647',NULL,'admin12','1844156d4166d94387f1a4ad031ca5fa',NULL,'localhost.com/uploads/images/xyz.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 03:02:40','2017-04-04 03:02:40'),(48,'admin16','Male','02/04/199578',NULL,NULL,NULL,NULL,'admin16@email.com','32682558',NULL,'admin16','9071e0ca7e4964a5cc69201ba2743650',NULL,'hwefjwef.sdfai.wfwe','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 03:11:19','2017-04-04 03:11:19'),(49,'admin17','Male','1/2/3',NULL,NULL,NULL,NULL,'admin17@email.com','456123',NULL,'admin17','d5133c970ad3a99c2248fed76970d06c',NULL,'xyz.com/uploads/images/fwsf.png','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 07:44:39','2017-04-04 07:44:39'),(57,'oyeannu','male','1/02/2016','single','osun','nigerian','mattew falad','timileyinogunso@gmail.com','08089650858',NULL,NULL,NULL,'not approved','kinka.com','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEwLCJpc3MiOiJodHRwOlwvXC9hc2submV0Lm5nXC9zZWd1cm9cL3NlZ3Vyb19hcGlcL3B1YmxpY1wvYXBpXC92MVwvYWRtaW5cL2xvZ2luIiwiaWF0IjoxNDkxMzg0MDI2LCJleHAiOjE0OTE0NzA0MjYsIm5iZiI6MTQ5MTM4NDAyNiwianRpIjoiMDgxZDRlNGNmZjMyOTE5NjZkOGNhNWI1YWQ0N2VkZjkifQ.KWFVB8a1dP1y8iQxeemTQc4u1HkyI0VwjY7mimQAh2c','2017-04-04 10:22:18','2017-04-04 10:22:18'),(60,'oyea','male','1/02/2016','single','osun','nigerian','mattew falad','timileyinoguns@gmail.com','08089650850',NULL,NULL,NULL,'not approved','kink.com',NULL,'2017-04-05 09:30:51','2017-04-05 09:30:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-04-05  9:31:45
