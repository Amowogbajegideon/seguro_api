<?php
/*Pays the users subscription fee online possible integration with paystack?
          Then records transaction details to transactions table*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use Illuminate\Http\Response;
use DB;
use JWTAuth;
class PaymentController extends Controller
{

    /**
     * Redirect the User to Paystack Payment Page
     * @return Url
     */

    public function dataGrabber(){
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $user_n = DB::table('users')->where('id','=',$user_id)->first();
        $payment_plan_id = DB::table('user_payment_plan')->where('user_id','=',$user_id)->value('payment_plan_id');
        $quantity = 1;
        $reference = Paystack::genTranxRef();
        $amount = DB::table('payment_plans')->where('id','=',$payment_plan_id)->value('amount');
        $key = config('paystack.secretKey');
        $status = 'Failed.';
        $comment = 'Got nothing. Are you sure your token is correct?';
        if ((count($amount) > 0) && ($user_n != [])) {
            # code...
            $status = 'Successful.';
            $comment = 'Data grabbed. Over to you!';
        }
        return response()->json([   
                                    'status' => $status,
                                    'comment' => $comment,
                                    'name' => $user_n->fullname,
                                    'email' => $user_n->email,
                                    'amount' => $amount,
                                    'quantity' => $quantity,
                                    'reference' => $reference,
                                    'key' => $key
                                    ]);
    }


    public function redirectToGateway()
    {
        return Paystack::getAuthorizationUrl()->redirectNow();
    }

    /**
     * Obtain Paystack payment information
     * @return void
     */
    public function handleGatewayCallback()
    {
        /*This replaces the pay function in the customer controller. its easier this way:)*/
        $paymentDetails = Paystack::getPaymentData();

        //dd($paymentDetails);
        // Now you have the payment details,
        // you can store the authorization_code in your db to allow for recurrent subscriptions
        // you can then redirect or do whatever you want
        //$user_id = 6;   // hardcoded
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $payment_reference = $paymentDetails['data']['reference'];
        $amount = $paymentDetails['data']['amount'];
        $month_paid_for = date('M');
        if ($paymentDetails['data']['status'] == 'success') {
            # code...
            $status = 'successful';
        }
        else{
            $status = 'failed';
        }
        if ($paymentDetails['message'] == 'Verification successful') {
            # code...
            $verification_status = 'verified';
        }
        else{
            $verification_status = 'unverified';
        }
        $type = 'online';
        $debit_or_credit = 'credit';
        $created_at = date('Y-m-d h:i:s ', time());
        $date_paid = $paymentDetails['data']['transaction_date'];
        $payment_plan_id = DB::table('user_payment_plan')->where('user_id','=',$user_id)->value('payment_plan_id');
        $write_op = DB::table('transactions')->insert([ 'payment_reference' => $payment_reference,
                                                        'amount' => $amount,
                                                        'month_paid_for' => $month_paid_for,
                                                        'status' => $status,
                                                        'verification_status' => $verification_status,
                                                        'type' => $type,
                                                        'debit_credit' => $debit_or_credit,
                                                        'date_paid' => $date_paid,
                                                        'user_id' => $user_id,
                                                        'payment_plan_id' => $payment_plan_id,
                                                        'created_at' => $created_at,
                                                        'updated_at' => $created_at]);
        // retrieving previous records id.
        $record_id = DB::table('transactions')->max('id');
        // calculates interest on a monthly basis where payment is made on a monthly basis.
        $max_id = DB::table('savings')->where('user_id','=',$user_id)->max('id');
        $p_prime = DB::table('savings')->where('id','=',$max_id)->value('principal_prime');
        if (!is_null($p_prime)) {
            # code...
            $principal = $p_prime + $amount/100.0;            
        }
        else{
            $principal = $amount/100.0;
        }
        $r = 0.03;
        // Now relying on interest rate set by admin.
        $rate_id = DB::table('interest_rate')->max('id');
        $rate = DB::table('interest_rate')->where('id','=',$rate_id)->value('interest_rate');
        $r = $rate/100.0;
        $n = 12.0;
        $p_prime = $principal*(1+$r/$n);
        $interest = $p_prime - $principal;
        $write_op_2 = DB::table('savings')->insert(['interest'=>$interest,
                                                    'principal_prime'=>$p_prime,
                                                    'month_paid'=>$month_paid_for,
                                                    'principal'=>$principal,
                                                    'user_id'=>$user_id,
                                                    'created_at' => $created_at,
                                                    'updated_at' => $created_at]);
        // update previously entered transaction record with balance
        $balance = $principal;
        $update_op = DB::table('transactions')->where('id','=',$record_id)->update(['balance'=>$balance]);

        //record interest as well
        $description = "Interest";
        $balance = $p_prime;
        $write_op_3 = DB::table('transactions')->insert([ 
                                                            'payment_reference' => $payment_reference,
                                                            'amount' => $interest,
                                                            'month_paid_for' => $month_paid_for,
                                                            'status' => $status,
                                                            'verification_status' => $verification_status,
                                                            'type' => $type,
                                                            'debit_credit' => $debit_or_credit,
                                                            'date_paid' => $date_paid,
                                                            'user_id' => $user_id,
                                                            'payment_plan_id' => $payment_plan_id,
                                                            'description' => $description,
                                                            'balance' => $balance,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
        if ($write_op && $write_op_2 && $write_op_3 && $update_op) {
            # code...
            return response()->json([
                'status' => 'successful',
                'comment' => 'Payment successful. Transaction details recorded.'
            ]);
        }
        else{
            return response()->json([
                'status' => 'successful',
                'comment' => 'Payment successful. Error recording transaction details.'
            ]);
        }
       
    }
}