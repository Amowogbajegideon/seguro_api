<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Http\Input;
use App\Http\Requests;
use Illuminate\Http\Response;
use DB;
use JWTAuth;

use Illuminate\Support\Facades\Storage;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function auth(Request $request){
        /*
            Authenticates user and grants access
            checks the database for the username and pulls out the associated password
            compares both passwords, if they checkout user is issued a JWT
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User
        */
        $username = $request->username;
        $password = $request->password;
        // $password = "gideon";
        $requires = ['username', 'password'];
        $validate = $this->validater($request, $requires);
        // return $password;

        if($validate['status'] == 'true') {
            $user = DB::table('users')->where('username','=',$username);
            $userExist = ($user->count() >= 1) ? true : false ;
            if($userExist) {
                $userDetails = $user->first();
                $username = $userDetails->username;
                $profile_pic_url = $userDetails->profile_pic_url;
                $stored_password = $userDetails->password;
                // return md5($password)." ".$stored_password;
                if(md5($password) == $stored_password) {
                    $token = JWTAuth::fromUser($userDetails);
                    $role_id = DB::table('role_user')->where('user_id','=',$userDetails->id)->value('role_id');

                    return response()->json([
                        'status' => 'successful',
                        'comment' => 'Authentication successful',
                        'username' => $username,
                        'profile_pic_url' => $profile_pic_url,
                        'token' => $token,
                        'role' => $role_id
                    ]);

                }
                else {
                    return response()->json([
                        'status' => 'failed',
                        'comment' => 'Authentication failed'
                    ]); 
                }

            }
            else {

                return response()->json([
                'status' => 'failed',
                'comment' => 'Authentication failed'
                ]);

            }
        }
        else {
            return response()->json($validate);
        }
        
    }

    public function forgot_password(Request $request){
        $email = $request->email;
        $password_reset_url = $request->password_reset_url;
        $requires = ['email', 'password_reset_url'];
        $validate = $this->validater($request, $requires);

        if($validate['status'] == 'true') {
            //accept users email
            //check if its in the db
            $query = DB::table('users')->where('email','=',$email)->first();
            
            //if its there send a reset url to the user via email
            //if it isn't throw out an error
            if (empty($query)) {
                $status = "Failed";
                $comment = "Invalid email address";
            }
            else{
                $created_at = date('Y-m-d h:i:s ', time());
                $token = bin2hex(random_bytes(20));
                $password_reset_url = $password_reset_url."/".$token;
                $write_op = DB::table('password_resets')->insert([
                                                                'email' => $email,
                                                                'token' => $token,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);
                // send token to users email
                $name = $query->fullname;
                $body = "Hi $name,\n your request to reset your password has been received pls go to ". $password_reset_url."". " <b>$token</b> .\n\nRegards,\nSEGURO. ";
                $subject = "Password reset";
                $from = "info@seguro.com";  // seguro's official email address.
                $to = $email;
                $send_op = AdminController::send_email($from,$to,$body,$subject);
                if ($send_op) {
                    $status = "Successful.";
                    $comment = "Token sent.";
                }
                else{
                    $status = "Successful.";
                    $comment = "Token not sent.";
                }

            }
            
        }
        else {
            return response()->json($validate);
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment]);
    }

    public function reset_password(Request $request){
        /* once user receives password reset token and he/she inputs it
            the function checks for its existence in the db if it does it writes the new password 
            into the db. Afterwards the token is deleted*/


        $requires = ['token','new_password'];
        $token = $request->token;
        $validate = $this->validater($request, $requires);
        if ($validate['status'] == 'true') {
            $user_email = DB::table('password_resets')->where('token','=',$token)->value('email');
            $user_id = DB::table('users')->where('email','=',$user_email)->value('id');
            $password = md5($request->new_password);
            $password_update_op = DB::table('users')->where('id','=',$user_id)
                                                    ->update(['password' => $password]);

            $status = 'failed';
            $comment = 'Password reset failed.';                                       
            if ($password_update_op) {
                $token_delete_op = DB::table('password_resets')->where('email','=',$user_email)
                                                                ->delete();
                if ($token_delete_op) {
                    # code...
                    $status = 'successful';
                    $comment = 'Password reset successful. Token removal successful.';
                }
                else{
                    $status = 'failed';
                    $comment = 'Password reset successful. Token removal failed.';
                }

            }

            return response()->json([
                                        'status' => $status,
                                        'comment' => $comment]);
            
        }
        else {
            return response()->json($validate);
        }

    }

    public static function generate_application_no(){
        do{
            $last_application_no = DB::table('application_nos')->max('application_no');
            if (!empty($last_application_no)) {
                $app_no = $last_application_no + 1;
            }
            else{
                $count = DB::table('users')->count();
                $app_no = $count + 1;
            }
            
            $application_no = sprintf("%06d", $app_no);
            $created_at = date('Y-m-d h:i:s ', time());
            $write_op = DB::table('application_nos')->insert([
                                                                'application_no' => $application_no,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);
            if ($write_op) {
                $flag = false;
            }
            else{
                $flag = true;
            }
        }while($flag);
        
        return response()->json([
                                    'status'=>'Successful',
                                    'application_no'=>$application_no]);
    }

    public function validater(Request $request, $requires)  {
        if (is_array($requires)) {
            $eResponse = array();
        // return $request->username;
            foreach ($requires as $field) {
                if (!isset($request[$field]) || trim($request[$field])== "") {
                    $eResponse[$field] = "$field field cannot be empty";
                    $eResponse['status'] = "false";
                }
                else {
                    $eResponse['status'] = "true";
                }
            }
            return $eResponse;
            # code...
        }
        else {
            return ["message"=>"variable is not an array"];
        }
    }
    
    public function lgaGen($state) {
        $lgas = DB::table('lgas')->where('state', $state)->get();
        return response()->json($lgas);
    }
    public function stateGen() {
        $states = DB::table('states')->get();
        return response()->json($states);
    }

    public function jumpUser(Request $request) {
        $email = $request->email;
        $role_id = $request->role_id;
        $user = DB::table('users')->where('email', $email)->first();
        // return response()->json($user);
        $user_id = $user->id;
        $roleCheck = DB::table('role_user')->where('user_id', $user_id)->get();
        // return $roleCheck;
        if(count($roleCheck) == 0) {
            $jump = DB::table('role_user')->insert(['user_id'=>$user_id,'role_id'=>$role_id]);
        }
        else {
            $jump = DB::table('role_user')->where('user_id', $user_id)->update(['role_id' => $role_id]);
        }
        $eResponse = array();
        if ($jump) {
            $status = 'successful';
            $comment = $user->username.' access level has been changed';
        }
        else {
            $status = 'failed';
            $comment = 'Operation not successful';
        }
        $eResponse['status'] = $status;
        $eResponse['comment'] = $comment;
        return response()->json($eResponse);
        /*$role = $request->role;
        DB::table('users')->insert([]);
        DB::table('role_user')->insert([]);
        DB::table('role_user')->insert(['user_id'=>,'role_id'=>]);*/

    }

    public function registerUser(Request $request) {
        $user = DB::table('users')->insert([
            'username' =>$request->username,
            'password' =>md5($request->password),
            'email' =>$request->email
            ]);
        $eResponse = array();
        if($user) {
            $status = "successful";
            $comment = "User has been successfully registered";
        }
        else {
            $status = "failed";
            $comment = "Operation failed";   
        }
        $eResponse['status'] = $status;
        $eResponse['comment'] = $comment;
        return response()->json($eResponse);


    }

    public function updateUser(Request $request) {
        $user = DB::table('users')->where('email', $request->email)->update([
            'username' =>$request->username,
            'password' =>md5($request->password)
            ]);
        if($user) {
            $status = "successful";
            $comment = "User has been successfully updated";
        }
        else {
            $status = "failed";
            $comment = "Operation failed";   
        }
        $eResponse['status'] = $status;
        $eResponse['comment'] = $comment;
        return response()->json($eResponse);
    }
}
