<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class QueryValidatorController extends Controller
{
    //
     public static function check_operation($condition,$operation){
    	if ($condition) {
    		return 1;
    	}
    	else{
    		return 0;
    	}
    }
    /*
      check_array verifies that the state of any array passed to it
      and issues the appropriate response.
    */
    public static function check_array($array,$array_name){
    	if (count($array)>0) {
    		return 1;
    	}
    	else{
    		return 0;
    	}
    }

    /*
      check_item checks the state of the item passed to it
      and returns the appropriate response.
    */
    public static function check_item($item,$item_name){
    	if ($item != "[]") {
    		return 1;
    	}
    	else{
            return 0;
    	}
    }

    /*
      check_operations checks the state of operations passed to it
      and returns the appropriate responses.
      del_op_0 => deletes L3 categories
      del_op_1 => deletes L2 categories
      del_op_2 => deletes the L1 category itself
    */
    public static function check_operations($conditions,$operation){
    	foreach ($conditions as $key => $condition) {
            $result = QueryValidatorController::check_operation($condition,$operation);
            if ($result) {
                $flag = 1;
            }
            else{
                $flag = 0;
            }
    	}
        return $flag;
    }

}
