<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use DB;
use JWTAuth;
use Mail;
use GuzzleHttp\Client;

class AdminController extends Controller
{
    //
    public function add_admin(Request $request){
        /*
            Accepts admin details and records them in the users table
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User
            by default role_id column is set to 3
        */
        $admin = JWTAuth::parseToken()->authenticate();
        $admin_id = $admin->id;
        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        if ($admin_role == 1) {
            
            $fullname = $request->fullname;
            $gender = $request->gender;
            $dob = $request->dob;
            $email = $request->email;
            $telephone_no = $request->telephone_no;
            $role_id = $request->role_id;
            $username = $request->username;
            $password = $request->password;
            $created_at = date('Y-m-d h:i:s ', time());
            $password = md5($password);
            $write_operation = false;
            $write_operation_2 = false;
            $approval_status = 'Not approved';

            // checks if there is a picture because it's not compulsory
            if ($request->hasFile('profile_pic')) {
                # code...
                $profile_pic_url = $request->file('profile_pic')->store('public/uploads/profile_pics');
                //$local_base_url = 'http://localhost/seguro_api/storage/app/';
                //$live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
                //$profile_pic_url = $local_base_url.$profile_pic_url;
                $profile_pic_url = config('constants.SERVER_ADDRESS').$profile_pic_url;
                $write_operation = DB::table('users')->insert([
                                        'fullname' => $fullname,
                                        'gender' => $gender,
                                        'dob' => $dob,
                                        'email' => $email,
                                        'telephone_no' => $telephone_no,
                                        'profile_pic_url' => $profile_pic_url,
                                        'username' => $username,
                                        'password' => $password,
                                        'created_at' => $created_at,
                                        'updated_at' => $created_at]);
            }
            else{
                $write_operation = DB::table('users')->insert([
                                        'fullname' => $fullname,
                                        'gender' => $gender,
                                        'dob' => $dob,
                                        'email' => $email,
                                        'telephone_no' => $telephone_no,
                                        'username' => $username,
                                        'password' => $password,
                                        'created_at' => $created_at,
                                        'updated_at' => $created_at]);
            }
            
            $user_id= DB::table('users')->max('id');
            $write_operation_2 = DB::table('role_user')->insert([
                                                                    'user_id' => $user_id,
                                                                    'role_id' => $role_id,
                                                                    'created_at' => $created_at,
                                                                    'updated_at' => $created_at
                                                                    ]);
            
            
            $status = 'Failed.';
            $comment = 'Account creation failed';
            if ($write_operation && $write_operation_2) {
                $status = 'Successful';
                $comment = 'Account creation successful.';
            } 

        }
        else{
            $status = 'Failed.';
            $comment = 'Access denied.';
        }
        return response()->json([
            'status' => $status,
            'comment' => $comment
        ]);
        
    }
    
    public function delete_admin($id){
        /*
            Deletes admins. Only the Super Admin has this power
        */
        $admin = JWTAuth::parseToken()->authenticate();
        
        $admin_id = $admin->id;

        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        
        $del_op = false;
        if ($admin_role == 1) {
            # code...
            $del_op = DB::table('role_user')->where('user_id','=',$id)->delete();
            $del_op_1 = DB::table('users')->where('id','=',$id)->delete();
        }
        $status = 'Failed';
        $comment = 'Admin not deleted.';
        if ($del_op && $del_op_1) {
            $status = 'successful';
            $comment = 'Admin deleted';
        }

        return response()->json([
                'status' => $status,
                'comment' => $comment
            ]);
        
    }
    public function edit_admin($id){
        /*
            pulls original data about the admin to be edited. Only the super admin can view this.
        */

        $admin = JWTAuth::parseToken()->authenticate();
        $admin_id = $admin->id;
        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        $status = 'Failed.';
        $admin = DB::table('users')->where('id','=',$id)->first();
        if ($admin_role == 1) {
            # code...
            if ($admin != []) {
                # code...
                $status = 'Successful.';
            }
            else{
                $admin = "Not found";
                $status = 'Failed.';
            }
            return response()->json([
                    'status' => $status,
                    'admin' => $admin
                ]);
        }
        else{
            return response()->json([
                    'status' => $status,
                    'admin' => 'not found. you\'re not allowed to.'
                ]);
        }
    }   
    
    public function update_admin(Request $request,$id){
    	/*
            Updates admin details in the users table
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User
            by default role_id column is set to 3
        */
        $role = DB::table('role_user')->where('user_id','=',$id)->value('role_id');
        if ($role == 2) {
            $fullname = $request->fullname;
            $gender = $request->gender;
            $role_id = $request->role_id;
            $dob = $request->dob;
            $email = $request->email;
            $telephone_no = $request->telephone_no;

            // checks if there is a picture because it won't always be changed
            if ($request->hasFile('profile_pic')) {
                $profile_pic_url = $request->file('profile_pic')->store('public/uploads/profile_pics');
                //$local_base_url = 'http://localhost/seguro_api/storage/app/';
                //$live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
                //$profile_pic_url = $local_base_url.$profile_pic_url;
                $profile_pic_url = config('constants.SERVER_ADDRESS').$profile_pic_url;
                $username = $request->username;
                $password = $request->password;
                $updated_at = date('Y-m-d h:i:s ', time());
                $password = md5($password);
                $write_operation = false;
                $write_operation_2 = false;
                $approval_status = 'Not approved';
                $write_operation = DB::table('users')->where('id','=',$id)->update([
                                            'fullname' => $fullname,
                                            'gender' => $gender,
                                            'dob' => $dob,
                                            'email' => $email,
                                            'telephone_no' => $telephone_no,
                                            'profile_pic_url' => $profile_pic_url,
                                            'username' => $username,
                                            'password' => $password,
                                            'updated_at' => $updated_at]);
            }
            else{
                $username = $request->username;
                $password = $request->password;
                $updated_at = date('Y-m-d h:i:s ', time());
                $password = md5($password);
                $write_operation = false;
                $write_operation_2 = false;
                $approval_status = 'Not approved';
                $write_operation = DB::table('users')->where('id','=',$id)->update([
                                            'fullname' => $fullname,
                                            'gender' => $gender,
                                            'dob' => $dob,
                                            'email' => $email,
                                            'telephone_no' => $telephone_no,
                                            'username' => $username,
                                            'password' => $password,
                                            'updated_at' => $updated_at]);
            }
            
            
            
            
            $write_operation_2 = DB::table('role_user')->where('user_id','=',$id)->update([
                                                                    'role_id' => $role_id,
                                                                    'updated_at' => $updated_at
                                                                    ]);
            
            
            $status = 'Failed.';
            $comment = 'Account update failed';
            if ($write_operation && $write_operation_2) {
                $status = 'Successful';
                $comment = 'Account update successful.';
            }
            return response()->json([
                    'status' => $status,
                    'comment' => $comment,
                    'profile_pic_url' => $profile_pic_url
                ]);
        }
        else{
            return response()->json([
                    'status' => 'Failed',
                    'comment' => 'Sorry you can only update an admins details.'
                ]);
        }
        
    }

    public function view_admins(){
        $ids = DB::table('role_user')->where('role_id','!=',3)->pluck('user_id');
        foreach ($ids as $key => $id) {
            $admin = DB::table('users')->where('id','=',$id)->first();
            $admin->role = DB::table('role_user')->where('user_id', $id)->value('role_id');
            $all_admins[] = $admin;
        }
       
        $status = 'Successful';
        $comment = 'Admins found';
        if ($all_admins == []) {
            $status = 'Failed';
            $comment = 'No admin';
        }
        return response()->json([
                    'status' => $status,
                    'comment' => $comment,
                    'admins' => $all_admins
            ]);
    }
    
    public function analytics(){
        $analytics = array('newly_registeredusers' => 0,
                            'feedback_messages' => 0,
                            'latest_feedback' =>"Lorem ipsum",
                            'interest_rate' => "3");
        $today = date('Y-m-d');
        $users = DB::table('users')->where('created_at','>=',$today)->get();
        $newlyRegisteredusers = 0;
        foreach ($users as $key => $user) {
            # code...
            $newlyRegisteredusers = $newlyRegisteredusers + DB::table('role_user')->where('user_id','=',$user->id)->where('role_id','=',3)->count();
        }
        $analytics['newly_registeredusers'] = $newlyRegisteredusers;
        $analytics['feedback_messages'] = DB::table('feedback')->where('status', '!=', 'replied')->count();
        $lastfeedbackMsg = DB::table('feedback')->max('id');
        $latestfeedbackMsg = DB::table('feedback')->where('id','=',$lastfeedbackMsg)->value('message');
        $analytics['latest_feedback'] = $latestfeedbackMsg;
        $interest_rate_id = DB::table('interest_rate')->max('id');
        $analytics['interest_rate'] = DB::table('interest_rate')->where('id','=',$interest_rate_id)->value('interest_rate');
        return response()->json([
                'status' => 'Successful',
                'analytics' => $analytics
            ]);
        //return $analytics;
        //return $newlyRegisteredusers;
    }
    public function view_customers(){
        

        /*$customers = DB::table('users')
            ->join('user_payment_plan', 'users.id', '=', 'user_payment_plan.user_id')
            ->join('payment_plans','user_payment_plan.payment_plan_id','=','payment_plans.id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_id','=','3')
            ->select('users.*', 'payment_plans.payment_plan')
            ->get();*/

        $customers = DB::table('users')
                     ->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_id','=','3')
                     ->join('user_payment_plan', 'users.id', '=', 'user_payment_plan.user_id')
                     ->select('users.*', 'user_payment_plan.payment_plan_id')
                     ->get();
        foreach ($customers as $key => $customer) {
            $customer_savings[] = DB::table('savings')->where('user_id', '=', $customer->id)->first();
        }
        
        /* previous arrangement
        $ids = DB::table('role_user')->where('role_id','=',3)->pluck('user_id');
        foreach ($ids as $key => $id) {
            # code...
            $all_customers[] = DB::table('users')->where('id','=',$id)->first();
            //$payment_plan_ids[] = DB::table('user_payment_plan')->where('id','=',$id)->pluck('payment_plan_id');
        }*/
        /*
        foreach ($payment_plan_ids as $key => $payment_plan_id) {
            # code...
            if (count($payment_plan_id) < 1) {
                ++$key;
            }
            else{
                $payment_plans[] = DB::table('payment_plans')->where('id','=',$payment_plan_id)->value('payment_plan');
            }
        }*/
        
        $status = 'Successful';
        $comment = 'Customer found';
        
        if ($customers == []) {
            $status = 'Failed';
            $comment = 'No customer found';
        }
    	return response()->json([
                    'status' => $status,
                    'comment' => $comment,
                    'customers' => $customers,
                    'customers_savings' => $customer_savings
                    //'payment_plans' => $payment_plans
            ]);
    }
    
    public function add_customer(Request $request){
        /*
            Accepts user details and records them in the user table
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User
        */
        $requires = ['title' ,'surname' ,'othernames' , 'gender' , 'dob' , 'state_of_origin' , 'lga' , 'marital_status' , 'nationality' , 'residential_address' , 'postal_address' , 'email', 'telephone_no' , 'payment_plan_id' , 'status' ];
        $validate = $this->validater($request, $requires);
        if($validate['status'] == 'true') {
            // Kin details
            $kin_name = $request->kin_name;
            $kin_address = $request->kin_address;
            $kin_telephone_no = $request->kin_telephone_no;
            $relationship = $request->relationship;
            //$fullname = $request->fullname;
            $title = $request->title;
            $surname = $request->surname;
            $othernames = $request->othernames;
            $gender = $request->gender;
            $dob = $request->dob;
            $state_of_origin = $request->state_of_origin;
            $lga = $request->lga;
            $marital_status = $request->marital_status;
            $nationality = $request->nationality;
            $residential_address = $request->residential_address;
            $postal_address = $request->postal_address;
            $email = $request->email;
            $telephone_no = $request->telephone_no;
            $telephone_no_1 = $request->telephone_no_1;
            
            $payment_plan_id = $request->payment_plan_id;
            $approval_status = $request->status;
            $employment_status = $request->employment_status;
            $name_of_employer = $request->name_of_employer;
            $designation = $request->designation;
            $address = $request->address;
            $created_at = date('Y-m-d h:i:s ', time());
            $write_operation = false;
            $write_operation_2 = false;
            $write_operation_3 = false;
            // checks if there is a picture because it not compulsory
            if ($request->hasFile('profile_pic')) {
                $profile_pic_url = $request->file('profile_pic')->store('public/uploads/profile_pics');
                $profile_pic_url = config('constants.SERVER_ADDRESS').$profile_pic_url;
            }
            else{
                $profile_pic_url = NULL;
            }

            if ($request->hasFile('identification')) {
                $identification_url = $request->file('identification')->store('public/uploads/identification');
                $identification_url = config('constants.SERVER_ADDRESS').$identification_url;
            }
            else{
                $identification_url = NULL;
            }
            
            $write_operation = DB::table('users')->insert([
                                                            'title' => $title,
                                                            'surname' => $surname,
                                                            'othernames' => $othernames,
                                                            'fullname' => $surname." ".$othernames,
                                                            'gender' => $gender,
                                                            'dob' => $dob,
                                                            'marital_status' => $marital_status,
                                                            'state_of_origin' => $state_of_origin,
                                                            'lga' => $lga,
                                                            'nationality' => $nationality,
                                                            'residential_address' => $residential_address,
                                                            'postal_address' => $postal_address,
                                                            'email' => $email,
                                                            'telephone_no' => $telephone_no,
                                                            'telephone_no_1' => $telephone_no_1,
                                                            'profile_pic_url' => $profile_pic_url,
                                                            'identification_url' => $identification_url,
                                                            'status' => strtolower($approval_status),
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
            
            
            
            $user_id= DB::table('users')->max('id');
            $write_operation_2 = DB::table('user_payment_plan')->insert([
                                                                    'user_id' => $user_id,
                                                                    'payment_plan_id' => $payment_plan_id,
                                                                    'created_at' => $created_at,
                                                                    'updated_at' => $created_at
                                                                    ]);
            $write_operation_3 = DB::table('employment_information')->insert([
                                                                        'user_id' => $user_id,
                                                                        'employment_status' => strtolower($employment_status),
                                                                        'name_of_employer' => $name_of_employer,
                                                                        'designation' => $designation,
                                                                        'address' => $address,
                                                                        'created_at' => $created_at,
                                                                        'updated_at' => $created_at]);
            $write_operation_4 = DB::table('role_user')->insert(['user_id' => $user_id,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);
            $write_operation_5 = DB::table('next_of_kin_details')->insert([ 
                                                                            'kin_name' => $kin_name,
                                                                            'relationship' => $relationship,
                                                                            'kin_telephone_no' => $kin_telephone_no,
                                                                            'kin_address' => $kin_address,
                                                                            'user_id' => $user_id,
                                                                            'created_at' => $created_at,
                                                                            'updated_at' => $created_at]);
            //,'payment_plan_id' => $payment_plan_id
            $status = "Account creation failed.";
            if ($write_operation && $write_operation_2 && $write_operation_3 && $write_operation_4 && $write_operation_5) {
                $status = "Account creation successful.";
            }
            return response()->json([
                    'status' => $status,
                    'approval_status' => $approval_status
                ]);
        }
        else {
            return response()->json($validate);
        }


    }
    
    public function delete_customer($id){
    	/*
            Deletes customers. All Admins have this power
        */
        $admin = JWTAuth::parseToken()->authenticate();
        
        $admin_id = $admin->id;

        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        
        $del_op = false;
        if ($admin_role == 1 || $admin_role == 2) {
            # code...
            $del_op = DB::table('role_user')->where('user_id','=',$id)->delete();
            $del_op_1 = DB::table('employment_information')->where('user_id','=',$id)->delete();
            $del_op_2 = DB::table('user_payment_plan')->where('user_id','=',$id)->delete();
            $del_op_3 = DB::table('users')->where('id','=',$id)->delete();
        }
        $status = 'Failed';
        $comment = 'Customer not deleted.';
        if ($del_op && $del_op_1 && $del_op_2 && $del_op_3) {
            $status = 'successful';
            $comment = 'Customer deleted';
        }

        return response()->json([
                'status' => $status,
                'comment' => $comment
            ]);
    }
    public function edit_customer($id){
        $customerCount = DB::table('users')->where('id','=',$id)->count();
        $customer = DB::table('users')->where('id','=',$id)->first();
        if($customerCount != 0) {
            $employment_information = DB::table('employment_information')->where('user_id', $id)->first();
            $user_payment_plan = DB::table('user_payment_plan')->where('user_id', $id)->first();
            $payment_plan = DB::table('payment_plans')->where('id', $user_payment_plan->payment_plan_id)->first();
            $next_of_kin_details = DB::table('next_of_kin_details')->where('user_id', $id)->first();
            
        }
        if ($customer != []) {
            # code...
            $status = 'Successful.';
        }
        else{
            $customer = "Not found";
            $employment_information = "null";
            $user_payment_plan = "null";
            $payment_plan = "null";
            $next_of_kin_details = "null";
            $status = 'Failed.';
        }
        return response()->json([
                'status' => $status,     // Admin can set a new username and password multiple times
                'customer' => $customer,
                'employment_information' => $employment_information,
                'user_payment_plan' => $user_payment_plan,
                'payment_plan' => $payment_plan,
                'next_of_kin_details' => $next_of_kin_details
            ]);
    }
    // sms
    public static function send_sms($username,$password,$phone_no){
        //login to sms247live via their API.
        $sms247live_client = new Client();
        $email = "damiperfect@gmail.com";
        $subacct = "SEGURO";
        $subacctpwd = "Micromail01";
        $sender = "SEGURO Admin";
        $cc_phone_no = "08090524193";   // this is just for confirmation
        $phone_nos = array($phone_no);  // add extra phone_nos here.
        $phone_nos = implode(',', $phone_nos);
        $sms247live_response[0] = $sms247live_client->request('POST', "http://www.smslive247.com/http/index.aspx?cmd=login&owneremail=$email&subacct=$subacct&subacctpwd=$subacctpwd");
        $response[0] = $sms247live_response[0]->getBody()->getContents();
        //send sms
        $eresponse0 = explode(":",$response[0]);
        //parsing sessionid
        $sessionid = $eresponse0[1];
        //preparing and sending message
        $message = "Your SEGURO application has been approved. Your username : $username and password : $password.";
        $sms247live_response[1] = $sms247live_client->request('POST', "http://www.smslive247.com/http/index.aspx?cmd=sendmsg&sessionid=$sessionid&message=$message&sender=$sender&sendto=$phone_nos&msgtype=0");
        $response[1] = $sms247live_response[1]->getBody()->getContents();
        $eresponse1 = explode(":",$response[1]);
        $messageid = $eresponse1[1];    // parsing messageid. Needed to check sms charge.

        $sms247live_response[2] = $sms247live_client->request('GET', "http://www.smslive247.com/http/index.aspx?cmd=querymsgcharge&sessionid=$sessionid&messageid=$messageid");
        $response[2] = $sms247live_response[2]->getBody()->getContents();
        $eresponse2 = explode(":",$response[2]);
        $charge = $eresponse2[1];  
        // amount charged is =N= 2.15 assuming SEGURO purchases between 5,000 and 9,999 credits
        // finding the users id
        $user_id = DB::table('users')->where('telephone_no','=',$phone_no)
                                     ->orWhere('telephone_no_1','=',$phone_no)
                                     ->value('id');
        $payment_plan_id = DB::table('user_payment_plan')->where('user_id','=',$user_id)->value('payment_plan_id');

        $write_op_2 = DB::table('notifications')->insert([
                                                         'sms' => $message,
                                                         'message_id' => $messageid,
                                                         'user_id' => $user_id]);
        // recording the amount charged for the user
        $amount = $charge * 2.15;
        $type = "offline";
        $debit_or_credit = "debit";
        $created_at = date('Y-m-d h:i:s ', time());
        $date_paid = date('Y-m-d');
        $write_op = DB::table('transactions')->insert([ 
                                                        'payment_reference' => 'SEGNOT'.date('Y/m/d'),
                                                        'amount' => $amount,
                                                        'month_paid_for' => date('M'),
                                                        'status' => 'successful',
                                                        'verification_status' => 'verified',
                                                        'payment_plan_id' => $payment_plan_id,
                                                        'description' => 'SMS notification charge.',
                                                        'type' => $type,
                                                        'debit_credit' => $debit_or_credit,
                                                        'date_paid' => $date_paid,
                                                        'user_id' => $user_id,
                                                        'created_at' => $created_at,
                                                        'updated_at' => $created_at]);

        // removing sms charges from the users savings
        $max_id = DB::table('savings')->where('user_id','=',$user_id)->max('id');

        if (!empty($max_id)) {
            # code...
            // this block runs if the user has savings
            $savings_data = DB::table('savings')->where('id','=',$max_id)->first();
            $new_principal_prime = $savings_data->principal_prime - $amount; 
            $write_op_1 = DB::table('savings')->insert([
                                                            'principal' => $savings_data->principal,
                                                            'principal_prime' => $new_principal_prime,
                                                            'user_id' => $savings_data->user_id,
                                                            'payment_plan_id' => $savings_data->payment_plan_id,
                                                            'month_paid' => $savings_data->month_paid,
                                                            'interest' => $savings_data->interest,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
        
        }
        else{
            $new_principal = -$amount; 
            $payment_plan_id = DB::table('user_payment_plan')->where('user_id','=',$user_id)->value('payment_plan_id');
            $month_paid = date('m');
            $write_op_1 = DB::table('savings')->insert([
                                                            'principal' => $new_principal,
                                                            'principal_prime' => 0,
                                                            'user_id' => $user_id,
                                                            'payment_plan_id' => $payment_plan_id,
                                                            'month_paid' => $month_paid,
                                                            'interest' => 0,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
        }
        

        if ($eresponse1[0] == 'OK') {
            # code...
            return 1;
        }
        else{
              return 0;
        }

    }

    public function send_other_sms($message,$phone_no){
        //login to sms247live via their API.
        $sms247live_client = new Client();
        $email = "damiperfect@gmail.com";
        $subacct = "SEGURO";
        $subacctpwd = "Micromail01";
        $sender = "SEGURO Admin";
        // $cc_phone_no = "08090524193";   // this is just for confirmation
        $phone_nos = array($phone_no);  // add extra phone_nos here.
        $phone_nos = implode(',', $phone_nos);
        $sms247live_response[0] = $sms247live_client->request('POST', "http://www.smslive247.com/http/index.aspx?cmd=login&owneremail=$email&subacct=$subacct&subacctpwd=$subacctpwd");
        $response[0] = $sms247live_response[0]->getBody()->getContents();
        //send sms
        $eresponse0 = explode(":",$response[0]);
        //parsing sessionid
        $sessionid = $eresponse0[1];
        //preparing and sending message
        $sms247live_response[1] = $sms247live_client->request('POST', "http://www.smslive247.com/http/index.aspx?cmd=sendmsg&sessionid=$sessionid&message=$message&sender=$sender&sendto=$phone_nos&msgtype=0");
        $response[1] = $sms247live_response[1]->getBody()->getContents();
        $eresponse1 = explode(":",$response[1]);
        $messageid = $eresponse1[1];    // parsing messageid. Needed to check sms charge.

        $sms247live_response[2] = $sms247live_client->request('GET', "http://www.smslive247.com/http/index.aspx?cmd=querymsgcharge&sessionid=$sessionid&messageid=$messageid");
        $response[2] = $sms247live_response[2]->getBody()->getContents();
        $eresponse2 = explode(":",$response[2]);
        // $charge = $eresponse2[1];  
        // // amount charged is =N= 2.15 assuming SEGURO purchases between 5,000 and 9,999 credits
        // // finding the users id
        
        
        if ($eresponse1[0] == 'OK') {
            return 1;
        }
        else{
              return 0;
        }
    }

    // email
    public static function send_email($from,$to,$body,$subject,$cc="",$bcc=""){
        
        Mail::raw($body, function ($message) use ($from ,$to ,$body ,$subject , $cc, $bcc) {       
            $message->from($from);
            $message->to($to)->subject($subject);
            if ($cc != ""){
                $message->cc($cc);
            }
            if ($bcc != ""){
                $message->bcc($bcc);
            }       
        }); 
        $send_op = Mail::failures();        
        
        if (count($send_op)==0){
            return 1;
        }
        else{
            return 0;
        }

    }

    public function penetrate(){
        DB::table('users')->insert(['email'=>'amowogbajegideon@gmail.com']);
        return DB::table('users')->get();
    }

    // wrapper function fro testing send_email
    public function sendemail(Request $request){
        // needs to body and subject
        $from = "info@seguro.com";  // seguro's official email address.
        $to = $request->to;
        $body = $request->body;
        $subject = $request->subject;
        $email_sender = AdminController::send_email($from,$to,$body,$subject); 
        if ($email_sender) {
            $status = 'Successful.';
            $comment = 'Email sent';
        }
        else{
            $status = 'Failed.';
            $comment = 'Email not sent';
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment]);
        
    }


    public function outbox(){
        $admin = JWTAuth::parseToken()->authenticate();
        $admin_id = $admin->id;
        $messages = DB::table('sent_emails')->where('user_id','=',$admin_id)->get();

        if (count($messages)>0) {
            # code...
            $status = "Successful.";
            $comment = "Outbox pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No outbox for you.";
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'messages' => $messages]);
    }
    public function inbox(){
        $messages = DB::table('feedback')->get();

        if (count($messages)>0) {
            # code...
            $status = "Successful.";
            $comment = "Feedback pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No feedback for you.";
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'messages' => $messages]);
    }

    public function unread_feedback(){
        $messages = DB::table('feedback')->where('status', NULL)->get();

        if (count($messages)>0) {
            # code...
            $status = "Successful.";
            $comment = "Feedback pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No feedback for you.";
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'messages' => $messages]);
    }
    public function read_feedback(){
        $messages = DB::table('feedback')->where('status', 'replied')->get();

        if (count($messages)>0) {
            # code...
            $status = "Successful.";
            $comment = "Feedback pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No feedback for you.";
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'messages' => $messages]);
    }

    // wrapper function for testing send_sms
    public function sendsms(Request $request){
        $username = $request->username;
        $password = $request->password;
        $phone_no = $request->telephone_no;
        $smssender = AdminController::send_sms($username,$password,$phone_no);
        return $smssender;
    }
    public function update_customer(Request $request, $id){
    	/*
            Updates customers details in the users table
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User aka customer
            by default role_id column is set to 3
                                    
        */
        $role = DB::table('role_user')->where('user_id','=',$id)->value('role_id');
        $payment_plan = DB::table('user_payment_plan')
                                ->join('payment_plans','payment_plans.id','=','user_payment_plan.payment_plan_id')
                                ->where('user_id','=',$id)
                                ->select('payment_plan')
                                ->value('payment_plan');
        
        $username = 0;
        $password = 0;
        
        if ($role == 3) {
            $updated_at = date('Y-m-d h:i:s ', time());
            $update_ops[] = DB::table('users')->where('id','=',$id)->update(['updated_at' => $updated_at]);
            if (isset($request->title)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['title' => $request->title]);
            }

            if (isset($request->surname)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['surname' => $request->surname]);
            }
            
            if (isset($request->othernames)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['othernames' => $request->othernames]);
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['fullname' => $request->surname." ".$request->othernames]);
            }

            if (isset($request->email)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['email' => $request->email]);
            }
            
            if (isset($request->telephone_no)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['telephone_no' => $request->telephone_no]);
            }

            if (isset($request->telephone_no_1)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['telephone_no_1' => $request->telephone_no_1]);
            }
            if (isset($request->gender)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['gender' => $request->gender]);
            }
            if (isset($request->dob)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['dob' => $request->dob]);
            }
            if (isset($request->marital_status)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['marital_status' => $request->marital_status]);
            }
            if (isset($request->state_of_origin)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['state_of_origin' => $request->state_of_origin]);
            }
            if (isset($request->lga)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['lga' => $request->lga]);
            }
            if (isset($request->nationality)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['nationality' => $request->nationality]);
            }
            if (isset($request->residential_address)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['residential_address' => $request->residential_address]);
            }
            if (isset($request->postal_address)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['postal_address' => $request->postal_address]);
            }
            if (isset($request->status)) {
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['status' => $request->status]);
            }
            // employment data
            if (isset($request->designation)) {
                $update_ops[] = DB::table('employment_information')->where('user_id','=',$id)->update(['designation' => $request->designation]);
            }
            if (isset($request->employment_status)) {
                $update_ops[] = DB::table('employment_information')->where('user_id','=',$id)->update(['employment_status' => $request->employment_status]);
            }
            if (isset($request->name_of_employer)) {
                $update_ops[] = DB::table('employment_information')->where('user_id','=',$id)->update(['name_of_employer' => $request->name_of_employer]);
            }
            if (isset($request->address)) {
                $update_ops[] = DB::table('employment_information')->where('user_id','=',$id)->update(['address' => $request->address]);
            }
            if (isset($request->payment_plan_id)) {
                $update_ops[] = DB::table('user_payment_plan')->where('user_id','=',$id)->update(['payment_plan_id' => $request->payment_plan_id]);
            }
            // next of kin information
            if (isset($request->kin_name)) {
                $update_ops[] = DB::table('next_of_kin_details')->where('user_id','=',$id)->update(['kin_name' => $request->kin_name]);
            }
            if (isset($request->kin_address)) {
                $update_ops[] = DB::table('next_of_kin_details')->where('user_id','=',$id)->update(['kin_address' => $request->kin_address]);
            }
            if (isset($request->kin_telephone_no)) {
                $update_ops[] = DB::table('next_of_kin_details')->where('user_id','=',$id)->update(['kin_telephone_no' => $request->kin_telephone_no]);
            }
            if (isset($request->relationship)) {
                $update_ops[] = DB::table('next_of_kin_details')->where('user_id','=',$id)->update(['relationship' => $request->relationship]);
            }
            if (isset($request->referral_no)) {
                $update_ops[] = DB::table('users')->where('user_id','=',$id)->update(['referral_no' => $request->referral_no]);
            }
            if ($request->hasFile('profile_pic')) {
                $profile_pic_url = $request->file('profile_pic')->store('public/uploads/profile_pics');
                $profile_pic_url = config('constants.SERVER_ADDRESS').$profile_pic_url;
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['profile_pic_url' => $profile_pic_url]);
            }
            if ($request->hasFile('identification')) {
                $identification_url = $request->file('identification')->store('public/uploads/identification');
                $identification_url = config('constants.SERVER_ADDRESS').$identification_url;
                $update_ops[] = DB::table('users')->where('id','=',$id)->update(['identification_url' => $identification_url]);
            }
            
            $update_ops_stat = QueryValidatorController::check_operations($update_ops,"Update operation");
            //$username = $request->username;
            //$password = $request->password;
            // Automatic username and password SEGUSER*****
            
            
            $change = 0;
            //if change is 0 it means you don't need a new username and password generated
            $change = $request->change;
            $usernames = DB::table('users')->pluck('username');
            if ($change == 1) {
                # code...
                $ncode = rand(0,100001);
                $username = $payment_plan.$ncode;
                $pcode = rand(0,100001);
                $password = $pcode;
                $password_n = md5($password);
                $write_ops[0] = DB::table('users')->where('id','=',$id)
                                                     ->update([
                                                                'username' => $username,
                                                                'password' => $password_n,
                                                                'updated_at' => $updated_at]);
                $user = DB::table('users')->where('id','=',$id)->first();
                $phone_no = $user->telephone_no;
                $sms_notification = AdminController::send_sms($username,$password,$phone_no);
                $from = "info@segurohousingcooperative.com"; //SEGURO's email address
                $to = $user->email;
                $body = "Your SEGURO application has been approved. Your username: $username and password: $password.";
                $subject = "SEGURO application approval";
                $email_notification = AdminController::send_email($from,$to,$body,$subject);
                
                if ($write_ops[0] && $update_ops_stat) {
                    $status = 'Successful';
                    $comment = 'Customers details updated successfuly.';
                }
                elseif ($write_ops[0]) {
                    $status = 'Successful';
                    $comment = 'Customers username and password generated. Other customer details not updated'; 
                }
                else{
                    $status = 'Failed.';
                    $comment = 'Customers details update failed';
                }
                
                if ($sms_notification) {
                    # code...
                    $sms_notification_stat = 'Sent.';
                }
                else{
                    $sms_notification_stat = 'Not sent.';
                }
            
                if ($email_notification) {
                    # code...
                    $email_notification_stat = 'Sent.';
                    $created_at = date('Y-m-d h:i:s ', time());
                    $admin = JWTAuth::parseToken()->toUser();
                    $admin_id = $admin->id;   //the email address of the person sending the email. Note what will appear in the actual email is SEGURO's actual email.
                    $admin_email = DB::table('users')->where('id','=',$admin_id)->value('email');
                    $write_ops[1] = DB::table('sent_emails')->insert([
                                                                    'sender'=>$admin_email,
                                                                    'receiver'=>$to,
                                                                    'subject'=>$subject,
                                                                    'cc'=>'',
                                                                    'bcc'=>'',
                                                                    'body'=>$body,
                                                                    'created_at' => $created_at,
                                                                    'updated_at' => $created_at]);
                }
                else{
                    $email_notification_stat = 'Not sent.';
                }
            }                                                 
            else{
                //check update_ops_stat
                if ($update_ops_stat) {
                    $status = 'successful.';
                    $comment = 'Customers username and password not generated. Other customer details updated'; 
                    $sms_notification_stat = 'Not sent.';
                    $email_notification_stat = 'Not sent.';
                }
                else{
                    $status = 'failed.';
                    $comment = 'Customers details update failed.'; 
                    $sms_notification_stat = 'Not sent.';
                    $email_notification_stat = 'Not sent.';
                }
                
            }
            
        }
        
        else{
            $status = 'Failed';
            $comment = 'Sorry you can only update a customers details.';       
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'email_notification_stat' => $email_notification_stat,
                                    'sms_notification_stat' => $sms_notification_stat]);
        
    }
    
    public function interested_users($id){
        
    	//$interested_users_id = DB::table('interested_users')->where('property_id','=',$id)->pluck('user_id');
        $interested_users = DB::table('users')
            ->join('interested_users','users.id','=','interested_users.user_id')->where('property_id','=',$id)
            ->join('role_user', 'users.id', '=', 'role_user.user_id')->where('role_id','=',3)
            ->join('user_payment_plan', 'users.id', '=', 'user_payment_plan.user_id')
            ->join('payment_plans','user_payment_plan.payment_plan_id','=','payment_plans.id')
            ->join('savings','users.id','=','savings.user_id')
            ->select('users.fullname', 'payment_plans.payment_plan','savings.principal_prime')
            ->get();

        
        $status = 'Failed';
        $comment = 'No customers interested in this property/this property is not in the database';
        
        /*
        if (count($interested_users_id)>0) {
            foreach ($interested_users_id as $key => $interested_user_id) {
                $interested_users[] = DB::table('users')->where('id','=',$interested_user_id)->first();

            }

            $status = 'Successful';
            $comment = 'Found interested customers in this property';
            
        }*/
        if (count($interested_users)>0) {
            # code...
            $status = 'Successful';
            $comment = 'Found interested customers in this property';
        }
        
        return response()->json([
                'status' => $status,
                'comment' => $comment,
                'interested_users' => $interested_users
            ]);
    }
    
    public function view_all_payments(){
        $status = 'Failed';
        $comment = 'No payments made.';
        //$payments = DB::table('transactions')->where('debit_credit','=','credit')->get();

        $payments = DB::table('transactions')
            ->join('users', 'users.id', '=', 'transactions.user_id')
            ->join('properties', 'transactions.property_id', '=', 'properties.id')
            ->select('transactions.*', 'users.fullname', 'properties.name')
            ->get();

        if (count($payments)>0) {
            $status = 'Successful';
            $comment = 'Found payments';
        }
        
        return response()->json([
                'status' => $status,
                'comment' => $comment,
                'payments' => $payments
            ]);

    }
    public function getPaymentTableFilter(Request $request) {
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $format = $request->format;
        if(!isset($fromDate) || !isset($toDate)) {
            $payments = DB::table('transactions')
            ->join('users', 'users.id', '=', 'transactions.user_id')
            ->join('properties', 'transactions.property_id', '=', 'properties.id')
            ->select('transactions.*', 'users.fullname', 'properties.name')
            ->get();
        }
        else {
            $fromDate = $this->formatDateFrom($fromDate, $format);
            $toDate = $this->formatDateFrom($toDate, $format);
            $payments = DB::table('transactions')
                ->join('users', 'users.id', '=', 'transactions.user_id')
                ->join('properties', 'transactions.property_id', '=', 'properties.id')
                ->select('transactions.*', 'users.fullname', 'properties.name')
                ->where('transactions.created_at', '>=', $fromDate)->where('transactions.created_at','<=', $toDate)
                ->get();
            
        }
        if (count($payments)>0) {
            $status = 'Successful';
            $comment = 'Found payments';
        }
        else{
            $status = "failed";
            $comment = 'No Payment Found';
        }
        
        return response()->json([
                'status' => $status,
                'comment' => $comment,
                'payments' => $payments
            ]);
    }
    
    public function add_transaction(Request $request){
        
        if (isset($request->property_id)) {
            # code...
            $property_id = $request->property_id;
            
        }
        else{
            $property_id = NULL;
            
        }
        $user_id = $request->user_id;
        $PreviouslyPaid = DB::table('transactions')
                                 ->where('user_id', $user_id)
                                 ->where('property_id', $property_id)
                                 ->orderBy('id', 'desc')
                                 ->first();
        if(count($PreviouslyPaid) > 0) {
            $currentBalance = $PreviouslyPaid->balance;
        }
        else {
            $currentBalance = DB::table('properties')->where('id', $property_id)->orderBy('id', 'desc')->first()->price;  
        }
        $currentBalance = (double) $currentBalance; // this convert the initial variable type to an integer;
        // return response()->json($currentBalance);

        $payment_reference = $request->payment_reference;
        $amount =  $request->amount;
        $month_paid_for = $request->month_paid_for;
        $status = 'successful';
        $verification_status = 'verified';
        $type = 'offline';
        $debit_or_credit = $request->debit_credit;

        if(trim($debit_or_credit) == 'credit') {
            $newBalance = $currentBalance - (double) $amount;
        }

        else if(trim($debit_or_credit) == 'debit') {
            $newBalance = $currentBalance + (double) $amount;
        }
        $newBalance = number_format($newBalance, 0, '.', '');
        // return $newBalance;
        $created_at = date('Y-m-d h:i:s ', time());
        $date_paid = $request->date_paid;
        $description = $request->description;

        $payment_plan_id = DB::table('user_payment_plan')->where('user_id','=',$user_id)->value('payment_plan_id');
        
        // if its a credit transaction being recorded it'll add to the savings. If its debit it deducts from the savings.
        // for the credit transaction it assumes that its the reqd. monthly payment so it add interest to it.
        if ($debit_or_credit == 'credit') {
            $write_op = DB::table('transactions')->insert([ 
                                                            'payment_reference' => $payment_reference,
                                                            'amount' => $amount,
                                                            'balance' => $newBalance,
                                                            'month_paid_for' => $month_paid_for,
                                                            'status' => $status,
                                                            'verification_status' => $verification_status,
                                                            'type' => $type,
                                                            'debit_credit' => $debit_or_credit,
                                                            'date_paid' => $date_paid,
                                                            'user_id' => $user_id,
                                                            'payment_plan_id' => $payment_plan_id,
                                                            'property_id' => $property_id,
                                                            'description' => $description,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);

            // retrieving previous records id.
            $record_id = DB::table('transactions')->max('id');

            // credit transaction
            // calculates interest on a monthly basis where payment is made on a monthly basis.
            $max_id = DB::table('savings')->where('user_id','=',$user_id)->max('id');
            $p_prime = DB::table('savings')->where('id','=',$max_id)->value('principal_prime');
            // return $p_prime;
            if (!is_null($p_prime)) {
                # code...
                $principal = $p_prime + $amount;            
            }
            else{
                $principal = $amount;
            }
            $r = 0.03;
            // Now relying on interest rate set by admin.
            $rate_id = DB::table('interest_rate')->max('id');
            $rate = DB::table('interest_rate')->where('id','=',$rate_id)->value('interest_rate');
            $r = $rate/100.0;
            $n = 12.0;
            $p_prime = $principal*(1+$r/$n);
            $interest = $p_prime - $principal;
            $write_op_2 = DB::table('savings')->insert([
                                                            'interest'=>$interest,
                                                            'principal_prime'=>$p_prime,
                                                            'month_paid'=>$month_paid_for,
                                                            'principal'=>$principal,
                                                            'user_id'=>$user_id,
                                                            'payment_plan_id'=>$payment_plan_id,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
            // update previously entered transaction record with balance
            $balance = $principal;
            $update_op = DB::table('transactions')->where('id','=',$record_id)->update(['balance'=>$balance]);
            $description = "Interest";
            $balance = $p_prime;
            $write_op_3 = DB::table('transactions')->insert([ 
                                                                'payment_reference' => $payment_reference,
                                                                'amount' => $interest,
                                                                'month_paid_for' => $month_paid_for,
                                                                'status' => $status,
                                                                'verification_status' => $verification_status,
                                                                'type' => $type,
                                                                'debit_credit' => $debit_or_credit,
                                                                'date_paid' => $date_paid,
                                                                'user_id' => $user_id,
                                                                'payment_plan_id' => $payment_plan_id,
                                                                'description' => $description,
                                                                'property_id' => $property_id,
                                                                'balance' => $newBalance,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);
        }
        elseif ($debit_or_credit == 'debit') {
            // debit transaction -- you can't debit an empty account
            
            $max_id = DB::table('savings')->where('user_id','=',$user_id)->max('id');
            $p_prime = DB::table('savings')->where('id','=',$max_id)->value('principal_prime');
            if (!is_null($p_prime) && ($p_prime >= $amount)) {
                $write_op = DB::table('transactions')->insert([ 'payment_reference' => $payment_reference,
                                                                'amount' => $amount,
                                                                'month_paid_for' => $month_paid_for,
                                                                'status' => $status,
                                                                'verification_status' => $verification_status,
                                                                'type' => $type,
                                                                'debit_credit' => $debit_or_credit,
                                                                'balance' => $newBalance,
                                                                'date_paid' => $date_paid,
                                                                'user_id' => $user_id,
                                                                'payment_plan_id' => $payment_plan_id,
                                                                'property_id' => $property_id,
                                                                'description' => $description,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);

                // retrieving previous records id.
                $record_id = DB::table('transactions')->max('id');
                $principal = $p_prime - $amount;   
                $write_op_2 = DB::table('savings')->insert([
                                                                'month_paid'=>$month_paid_for,
                                                                'principal'=>$principal,
                                                                'user_id'=>$user_id,
                                                                'payment_plan_id'=>$payment_plan_id,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]); 
                // update previously entered transaction record with balance
                $balance = $principal;
                $update_op = DB::table('transactions')->where('id','=',$record_id)->update(['balance'=>$newBalance]);    
                $write_op_3 = true;    
            }
            else{
                $write_op = false;
                $write_op_2 = false;
                $comment = 'Account balance low. ';
            }
            
        }
        else{
            $write_op = false;
            $write_op_2 = false;
            $write_op_3 = false;
        }



       
        if ($write_op && $write_op_2 && $write_op_3 && $update_op) {
            $from = "info@seguro.com";
            $subject = "Seguro Transactions";
            $userInfo = $this->findUserInfo($request->user_id);
            // return json_encode($userInfo, JSON_PRETTY_PRINT);
            $to = $userInfo->email;
            $body = "Hi $userInfo->fullname, your transaction has been processed. REF NO: $payment_reference";
            $email_sender = AdminController::send_email($from,$to,$body,$subject);
            return response()->json([
                'status' => 'Successful.',
                'comment' => 'Transaction details recorded.'
            ]);
        }
        else{
            return response()->json([
                'status' => 'Failed.',
                'comment' => $comment.'Error recording transaction details.'
            ]);
        }
       
    }


    public function add_property(Request $request){
        
        $name = $request->name;
        $address = $request->address;
        $price = $request->price;
        $area = $request->area;
        $no_of_bedrooms = $request->no_of_bedrooms;
        $description = $request->description;
        $time_of_offer = $request->time_of_offer;
        $status = $request->status;
        //$coordinates = $request->coordinates;
        
        // $property_pics = $request->property_pic;
        $created_at = date('Y-m-d h:i:s ', time());
        $write_op = DB::table('properties')->insert(['name'=>$name,
                                                    'address'=>$address,
                                                    'price'=>$price,
                                                    'area'=>$area,
                                                    //'coordinates'=>$coordinates,
                                                    'no_of_bedrooms'=>$no_of_bedrooms,
                                                    'description'=>$description,
                                                    'time_of_offer'=>$time_of_offer,
                                                    'status' => $status,
                                                    'created_at' => $created_at,
                                                    'updated_at' => $created_at]);
        $property_id = DB::table('properties')->max('id');  // to be modified later
                                            
        $property_pics = $request->file('property_pic');
        // return response()->json(array($property_pics));

        foreach ($property_pics as $key => $property_pic) {
            # code...
            $property_pic_url = $property_pic->store('public/uploads/property_pics');
            //$local_base_url = 'http://localhost/seguro_api/storage/app/';
            //$live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
            //$property_pic_url = $local_base_url.$property_pic_url;
            $property_pic_url = config('constants.SERVER_ADDRESS').$property_pic_url;
            $created_at = date('Y-m-d h:i:s ', time());
            $write_op_2[] = DB::table('property_pictures')->insert([
                                                                    'property_pic_url'=> $property_pic_url,
                                                                    'property_id'=> $property_id,
                                                                    'created_at' => $created_at,
                                                                    'updated_at' => $created_at]);
        }
        
        /*
        $property_pic_url = $request->file('property_pic')->store('public/uploads/profile_pics');
        //$local_base_url = 'http://localhost/seguro_api/storage/app/';
        $live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
        //$property_pic_url = $local_base_url.$property_pic_url;
        $property_pic_url = $live_base_url.$property_pic_url;*/
        
        /*
        $write_op_2 = DB::table('property_pictures')->insert([
                                                                'property_pic_url'=>$property_pic_url,
                                                                'property_id'=>$property_id,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at]);*/
        $write_op_2_flag = false;
        foreach ($write_op_2 as $key => $write_op_2) {
            if (!$write_op_2) {
                # code...
                $write_op_2_flag = true;
            }
            
        }
        $status = 'failed';
        $comment = 'Property not added';
        if ($write_op && (!$write_op_2_flag)) {
            # code...
            $status = 'successful';
            $comment = 'Property and property pic added';
        }
        elseif ($write_op_2_flag && $write_op) {
            # code...
            $comment = 'Property pic not added, property details added';
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment
                                ]);
    }
    public function view_properties(){
        $all_properties = DB::table('properties')->orderBy('created_at', 'desc')->get();
        $all_properties_pics = DB::table('property_pictures')->get();
        $status = 'Successful.';
        $comment = 'Properties found.';
        if ($all_properties == []) {
            $status = 'Failed.';
            $comment = 'No property found.';
        }
        return response()->json([
                    'status' => $status,
                    'comment' => $comment,
                    'properties' => $all_properties,
                    'property_pictures' => $all_properties_pics
            ]);
    }
    public function edit_property($id){
        /*
        $properties = DB::table('properties')
            ->join('property_pictures', 'properties.id', '=', 'property_pictures.property_id')
            ->select('properties.*', 'property_pictures.property_pic_url')->where('properties.id','=',$id)
            ->get();

        return $properties;*/

        
        $property = DB::table('properties')->where('id','=',$id)->first();
        $property_pics = DB::table('property_pictures')->where('property_id','=',$id)->get();
        if ($property != [] && $property_pics != []) {
            # code...
            $status = 'Successful';
        }
        else{
            $property = "Not found";
            $property_pics = $property;
            $status = 'Failed';
        }
        return response()->json([
                'status' => $status,
                'property' => $property,
                'property_pictures' => $property_pics
            ]);
    }
    public function update_property(Request $request,$id){
        $name = $request->name;
        $address = $request->address;
        $price = $request->price;
        $area = $request->area;
        $no_of_bedrooms = $request->no_of_bedrooms;
        $description = $request->description;
        $time_of_offer = $request->time_of_offer;
        $status = $request->status;
        $updated_at = date('Y-m-d h:i:s ', time());
        $write_op_2 = false;
        $write_op = $write_op_2;
        // check if there\'s no picture as long as a pic was addded the first time there is no problem
        // assign received files into an array. This caters for the multiple pics required for the properties
        if ($request->hasFile('property_pic')) {
            # code...
            $property_pic = $request->file('property_pic');
            // foreach ($property_pics as $key => $property_pic) {
                # code...
                $property_pic_url = $property_pic->store('public/uploads/property_pics');
                //$local_base_url = 'http://localhost/seguro_api/storage/app/';
                //$live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
                //$property_pic_url = $local_base_url.$property_pic_url;
                $property_pic_url = config('constants.SERVER_ADDRESS').$property_pic_url;
                $created_at = date('Y-m-d h:i:s ', time());
                $write_op_2[] = DB::table('property_pictures')->insert([
                                                                        'property_pic_url'=> $property_pic_url,
                                                                        'property_id'=> $id,
                                                                        'created_at' => $created_at,
                                                                        'updated_at' => $created_at]);
            // }
        }
        
        

        /*
        if ($request->hasFile('property_pic')) {
            # code...
            $property_pic_url = $request->file('property_pic')->store('public/uploads/profile_pics');
            //$local_base_url = 'http://localhost/seguro_api/storage/app/';
            $live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
            //$property_pic_url = $local_base_url.$property_pic_url;
            $property_pic_url = $live_base_url.$property_pic_url;
            $write_op_2 = DB::table('property_pictures')->insert([
                                                                'property_pic_url'=>$property_pic_url,
                                                                'property_id'=>$id,
                                                                'updated_at' => $updated_at]);  // inserting so you can have multiple pics of the same property ??? yes! but no editing the previous one have to find a way to do that.

        }*/
        
        
        $write_op = DB::table('properties')->where('id','=',$id)->update(['name'=>$name,
                                                    'address'=>$address,
                                                    'price'=>$price,
                                                    'area'=>$area,
                                                    'no_of_bedrooms'=>$no_of_bedrooms,
                                                    'description'=>$description,
                                                    'updated_at' => $updated_at]);
        
        $write_op_2_flag = false;
        // foreach ($write_op_2 as $key => $write_op_2) {
            if (!$write_op_2) {
                # code...
                $write_op_2_flag = true;
            }
            
        // }
        $status = 'failed';
        $comment = 'Error updating property';
        if ($write_op && (!$write_op_2_flag)) {
            # code...
            $status = 'successful';
            $comment = 'Property updated and property pic added';
        }
        elseif ($write_op_2_flag && $write_op) {
            # code...
            $comment = 'Property pic not added, property details updated';
        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment
                                ]);
        /*if ($write_op && $write_op_2) {
            # code...
            return response()->json([
                'status' => 'successful',
                'comment' => 'Property updated.'
            ]);
        }
        else{
            return response()->json([
                'status' => 'failed',
                'comment' => 'Error updating property.'
            ]);
        }*/
    }
    public function delete_property_pic($id){
        /*
            Deletes property pictures. All Admins have this power
        */
        $admin = JWTAuth::parseToken()->authenticate();
        $admin_id = $admin->id;
        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        $del_op = false;
        if ($admin_role != 3) {
            # code...
            $del_op = DB::table('property_pictures')->where('id','=',$id)->delete();
        }
        $status = 'Failed.';
        $comment = 'Property picture not deleted.';
        if ($del_op || $del_op_1) {
            $status = 'Successful.';
            $comment = 'Property picture deleted.';
        }
        return response()->json([
                'status' => $status,
                'comment' => $comment
            ]);
    }
    public function delete_property($id){
        /*
            Deletes properties. All Admins have this power
        */
        $admin = JWTAuth::parseToken()->authenticate();
        
        $admin_id = $admin->id;

        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        
        $del_op = false;
        if ($admin_role != 3) {
            # code...
            $del_op = DB::table('property_pictures')->where('property_id','=',$id)->delete();
            $del_op_1 = DB::table('properties')->where('id','=',$id)->delete();
        }
        $status = 'Failed.';
        $comment = 'Property not deleted.';
        if ($del_op || $del_op_1) {
            $status = 'Successful.';
            $comment = 'Property deleted.';
        }

        return response()->json([
                'status' => $status,
                'comment' => $comment
            ]);
    }

    public function set_interest_rate(Request $request){
        /*
            sets the interest rate. Only the super admin has this power
        */
        $admin = JWTAuth::parseToken()->authenticate();
        
        $admin_id = $admin->id;

        $admin_role = DB::table('role_user')->where('user_id','=',$admin_id)->value('role_id');
        
        $write_op = false;
        $status = 'Failed.';
        $comment = 'Interest rate not set.';
        if ($admin_role == 1) {
            # code...
            $write_op = DB::table('interest_rate')->insert([
                                                             'interest_rate' => $request->interest_rate,
                                                             'user_id' => $admin_id]) ;
            if ($write_op) {
                $status = 'Successful.';
                $comment = 'Interest rate set.';
            }

        }
        else{
            $status = 'Failed.';
            $comment = 'Access denied.';
        }
        return response()->json([
                'status' => $status,
                'comment' => $comment
            ]);
    }

    public function view_email($id){
        $sent_email = DB::table('sent_emails')->where('id','=',$id)->get();
        $status = 'Failed.';
        $comment = 'Empty item.';
        if (count($sent_email)>0) {
            # code...
            $status = 'Successful.';
            $comment = 'There\'s something in your outbox go check it out.';
        }
        return repsonse()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    '$sent_email' => $sent_email]);
    }
    
    
    /*
    public function eligibility_check($property_id,$user_id){
    	return 'eligibility_check';
    }*/

    public function validater(Request $request, $requires)  {
        if (is_array($requires)) {
            $eResponse = array();
        // return $request->username;
            foreach ($requires as $field) {
                if (!isset($request[$field]) || trim($request[$field])== "") {
                    $eResponse[$field] = "$field field cannot be empty";
                    $eResponse['status'] = "false";
                }
                else {
                    $eResponse['status'] = "true";
                }
            }
            return $eResponse;
            # code...
        }
        else {
            return ["message"=>"variable is not an array"];
        }
    }

    public function replied_feedback(Request $request) {
        $feedback_id = $request->id;
        $answer = $request->answer;
        $write_op = DB::table('feedback')->where('id', $feedback_id)->update([
            'status' => "replied",
            'answer' => $answer
            ]);
        if($write_op) {
            $status = 'success';
            $comment = "Feedback has been replied";
        }
        else {
            $status = "failed";
            $comment = "There has been an error";
        }

        return response()->json([
            'status' => $status,
            'comment' => $comment
            ]);
    }

    public function new_upgrade_requests() {
        $messages = DB::table('upgrade_requests')->where('status', 'processing')->get();
        if (count($messages)>0) {
            foreach ($messages as $message) {
                $message->username = $this->userInfo($message->user_id)->original;
                $message->current_plan = $this->planInfo($this->findCurrentPlan($message->user_id))->original;
                $message->new_plan = $this->planInfo($message->new_payment_plan_id)->original;
            }
            $status = "Successful.";
            $comment = "Upgrade requests pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No Upgrade requests for you.";
        }
        return response()->json([
                                'status' => $status,
                                'comment' => $comment,
                                'messages' => $messages]);

    }

    public function upgrade_requests() {
        $messages = DB::table('upgrade_requests')->get();
        if (count($messages)>0) {
            foreach ($messages as $message) {
                $message->username = $this->userInfo($message->user_id)->original;
                $message->new_plan = $this->planInfo($message->new_payment_plan_id)->original;
                $message->current_plan = $this->PlanInfo($this->findCurrentPlan($message->user_id))->original;
            }
            $status = "Successful.";
            $comment = "Upgrade requests pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No Upgrade requests for you.";
        }
        return response()->json([
                                'status' => $status,
                                'comment' => $comment,
                                'messages' => $messages]);
    }

    public function approve_upgrade_request(Request $request) {
        $request_id = $request->id;
        $payment_plan_id = $request->payment_plan_id;
        $user_id = $request->user_id;
        $current_payment_plan_id = $this->findCurrentPlan($user_id);
        $current_payment_plan = DB::table('payment_plans')->where('id', $current_payment_plan_id)->first()->payment_plan;
        $new_payment_plan = DB::table('payment_plans')->where('id', $payment_plan_id)->first()->payment_plan;
        $currentUsername = DB::table('users')->where('id', $user_id)->first()->username;

        $toBeReplaced = $this->getStringtoReplace($new_payment_plan, $currentUsername);
        // $toBeReplaced = $this->getStringtoReplace($current_payment_plan, $currentUsername);
        $newUsername = str_replace($toBeReplaced, $new_payment_plan, $currentUsername);

        $write_op = DB::table('upgrade_requests')->where('id', $request_id)->update([
            'status' => "approved"
            ]);
        $write_op_2 = DB::table('user_payment_plan')->where('user_id', $user_id)->update([
            'payment_plan_id' => $payment_plan_id
            ]);
        $write_op_3 = DB::table('users')->where('id', $user_id)->update([
            'username' => $newUsername
            ]);
        //
        if($write_op || $write_op_2 || $write_op_3) {
            $status = 'success';
            $comment = "Upgrade request approved";
        }
        else {
            $status = "failed";
            $comment = "There has been an error";
        }

        return response()->json([
            'status' => $status,
            'comment' => $comment
            ]);
    }
    public function upgraded_requests() {
        $messages = DB::table('upgrade_requests')->where('status', 'approved')->get();
        if (count($messages)>0) {
            foreach ($messages as $message) {
                $message->username = $this->userInfo($message->user_id)->original;
                $message->new_plan = $this->planInfo($message->new_payment_plan_id)->original;
                $message->current_plan = $this->planInfo($this->currentPlanInfo($message->user_id))->original;
            }
            $status = "Successful.";
            $comment = "Upgrade requests pulled successfully";
        }
        else{
            $status = "Failed.";
            $comment = "No Upgrade requests for you.";
        }
        return response()->json([
                                'status' => $status,
                                'comment' => $comment,
                                'messages' => $messages]);
    }
    public function userInfo($id) {
        $userInfo = DB::table('users')->where('id', $id)->first();
        return response()->json($userInfo);
    }
    public function planInfo($id) {
        $planInfo = DB::table('payment_plans')->where('id', $id)->first();
        return response()->json($planInfo);
    }

    public function findUserInfo($id) {
        $userInfo = DB::table('users')->where('id', $id)->first();
        return $userInfo;
    }

    public function findCurrentPlan($user_id) {
        $currentPlanInfo = DB::table('user_payment_plan')->where('user_id', $user_id)->first();
        $currentPlan = $currentPlanInfo->payment_plan_id;
        return $currentPlan;
    }
    public function getStringtoReplace($replaceString, $string) {
        $countString = strlen($replaceString);
        $stringArray = str_split($string);
        $nstring = "";
        for ($i=0; $i < $countString; $i++) { 
            $nstring =  $nstring. "". $stringArray[$i];
        }
        return $nstring;
    }

    public function getDateFilter(Request $request) {
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $format = $request->format;
        $fromDate = $this->formatDateFrom($fromDate, $format);
        $toDate = $this->formatDateFrom($toDate, $format);
        $this->filterByDate($tableName, $fromDate, $toDate);
    }

    public function getAnyTableDateFilter(Request $request) {
        $fromDate = $request->from_date;
        $toDate = $request->to_date;
        $format = $request->format;
        $tableName = $request->tableName;
        $fromDate = $this->formatDateFrom($fromDate, $format);
        return $toDate = $this->formatDateFrom($toDate, $format);
        $this->filterByDate($tableName, $fromDate, $toDate);
    }

    public function filterByDate($tableName, $fromDate, $toDate) {
        $dateFilter = DB::table($tableName)->where('created_at', '>=', $fromDate)->where('created_at','<=', $toDate)->get();
        return $dateFilter;
    }

    public function formatDateFrom($date, $format) {
        // $possibleDateDelimeters = array("\\", "/", "-");
        // $date = date()
        $datetime = new \DateTime();
        $getTimeFromFormat = $datetime->createFromFormat($format, $date);
        $getDate = $getTimeFromFormat->format("Y-m-d h:i:s");

        return $getDate;

    }

    public function test() {
        $user = DB::table('users')->get();
        return $this->getBulkContact(0,0,0);
        return response()->json($user);
        return md5('gideon');
        return ($this->formatDateFrom("01/03/2017", "m/d/Y"));
    }

    public function getBulkContact($user_cat, $plan_id, $property_id) {
        $getUserIds = array();
        $email = array();
        $phone = array();
        $phone_1 = array();
        if($user_cat == 0) {
            if ($plan_id == 0) {
                $getUserIds = DB::table('user_payment_plan')->pluck('user_id');
            }
            else {
                $getUserIds = DB::table('user_payment_plan')->where('payment_plan_id', $plan_id)->pluck('user_id');
            }
        }
        else {
            if($property_id == 0) {
                $interested_users = DB::table('interested_users');
            }
            else {
                $interested_users = DB::table('interested_users')->where('property_id', $property_id);
            }
            $getUserIdsFromInterestedOrUsers = $interested_users->pluck('user_id');
            if ($plan_id == 0) {
                foreach ($getUserIdsFromInterestedOrUsers as $i => $userId) {
                    $getUserIds[$i] = DB::table('user_payment_plan')->where('user_id', $userId)->value('user_id');
                }
            }
            else {
                foreach ($getUserIdsFromInterestedOrUsers as $i => $userId) {
                    $getUserIds[$i] = DB::table('user_payment_plan')->where('user_id', $userId)->where('payment_plan_id', $plan_id)->value('user_id');
                }
            }
        }
        foreach ($getUserIds as $getUserId) {
            $email[] = DB::table('users')->where('id', $getUserId)->value('email');
            $phone[] = DB::table('users')->where('id', $getUserId)->value('telephone_no');
            $phone_1[] = DB::table('users')->where('id', $getUserId)->value('telephone_no_1');
        }
        $contact = array('email' => $email,'phone' => $phone,'phone_1' => $phone_1);
            
        return $contact;
        // $bulkEmail = DB::table('user');
    }

    public function sendBulkEmail(Request $request) {
        $user_cat = $request->user_cat;
        $plan_id = $request->plan_id;
        $property_id = $request->property_id;
        $message = $request->message;
        $contacts = $this->getBulkContact($user_cat, $plan_id, $property_id);
        $emails = $contacts['email']; 
        $subject = "Seguro Customer Care";
        $from = "info@seguro.com";  // seguro's official email address.
        $to = $request->to;
        $subject = $request->subject;
        $countSentEmails = 0;
        $successfulEmails = "";
        if(count($emails>0)) {
            foreach ($emails as $to) {
                $email_sender = AdminController::send_email($from,$to,$message,$subject); 
                if ($email_sender) {
                    $countSentEmails++;
                    $successfulEmails = $successfulEmails. ", $to";
                }
            }
            if ($countSentEmails <= 0) {
                $status = 'failed';
                $comment = 'No Email sent';
                $listOfMailSent = "";
                $totalNoOfEmails = count($emails);
                $totalNoOfEmailSent = 0;
            }
            else if ($countSentEmails == 1) {
                $status = 'successful';
                $comment = $countSentEmails. 'out of '.count($emails). ' email sent.';
                $listOfMailSent = $successfulEmails;
            }
            else{
                $status = 'successful';
                $comment = $countSentEmails. ' out of '.count($emails). ' emails sent.';
                $listOfMailSent = $successfulEmails;
                $totalNoOfEmails = count($emails);
                $totalNoOfEmailSent = $countSentEmails;
            }
            
        }
        else {
            $status = 'failed';
            $comment = "This Filter doesn't contain any email";
            $listOfMailSent = "";
            $totalNoOfEmails = count($emails);
            $totalNoOfEmailSent = 0;

        }
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'listOfMailSent' => $listOfMailSent,
                                    'totalNoOfEmails'=> $totalNoOfEmails,
                                    'totalNoOfEmailSent'=> $totalNoOfEmailSent]);
    }
    public function sendBulkSms(Request $request) {
        $user_cat = $request->user_cat;
        $plan_id = $request->plan_id;
        $property_id = $request->property_id;
        $message = $request->message;
        $contacts = $this->getBulkContact($user_cat, $plan_id, $property_id);
        $phones = $contacts['phone']; 
        $countSentSms = 0;
        $successfulSms = "";
        if(count($phones) > 0) {
            foreach ($phones as $phone) {
                $phone_sender = $this->send_other_sms($message,$phone); 
                if ($phone_sender == 1) {
                    $countSentSms++;
                    $successfulSms = $successfulSms. ", $phone";
                }
            }
            if ($countSentSms <= 0) {
                $status = 'failed';
                $comment = 'No Sms sent';
                $listOfSmsSent = " ";
                $totalNoOfPhones = count($phones);
                $totalNoOfSmsSent = 0;
            }
            else {
                $status = 'successful';
                $comment = $countSentSms. ' out of '.count($phones).' Sms sent.';
                $listOfSmsSent = $successfulSms;
                $totalNoOfPhones = count($phones);
                $totalNoOfSmsSent = $countSentSms;

            }
        }
        else {
            $status = 'failed';
            $comment = "This Filter doesn't contain any phone number";
            $listOfSmsSent = "";
            $totalNoOfPhones = count($phones);
            $totalNoOfSmsSent = 0;
        }
       
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'listOfSmsSent' => $listOfSmsSent,
                                    'totalNoOfPhones' => $totalNoOfPhones,
                                    'totalNoOfSmsSent' => $totalNoOfSmsSent]);
    }

}
