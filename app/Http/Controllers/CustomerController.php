<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use DB;
use JWTAuth;
class CustomerController extends Controller
{
    //
    
    public function signup(Request $request){
        /*
            Accepts user details and records them in the user table
            Types of users 
            1 => SuperAdmin
            2 => Admin
            3 => User
        */
        
        //$fullname = $request->fullname;
        // new changes
        $title = $request->title;
        $surname = $request->surname;
        $othernames = $request->othernames;
        $fullname = ucwords($surname)." ".ucfirst($othernames);
        $gender = $request->gender;
        //$dob = $request->dob;
        $date_of_birth = $request->date_of_birth;
        $state_of_origin = $request->state_of_origin;
        $lga = $request->lga;
        $marital_status = $request->marital_status;
        $nationality = $request->nationality;
        $residential_address = $request->residential_address;
        $postal_address = $request->postal_address;
        $email = $request->email;
        $telephone_no = $request->telephone_no;
        $telephone_no_1 = $request->telephone_no_1;
        $referral_no = $request->referral_no;
        $application_no = $request->application_no;
        
        // next of kin details
        $kin_name = $request->kin_name;
        $kin_address = $request->kin_address;
        $kin_telephone_no = $request->kin_telephone_no;
        $relationship = $request->relationship;
    
        $write_operation = false;
        $write_operation_2 = false;
        $write_operation_3 = false;
        $write_operation_4 = false;
        $write_operation_5 = false;
        $created_at = date('Y-m-d h:i:s ', time());
        $approval_status = 'Not approved';
        // checks if there is a picture because it's not compulsory
        if ($request->hasFile('profile_pic')) {
            # code...
            $profile_pic_url = $request->file('profile_pic')->store('public/uploads/profile_pics');
            //$local_base_url = 'http://localhost/seguro_api/storage/app/';
            $live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
            //$profile_pic_url = $local_base_url.$profile_pic_url;
            //$profile_pic_url = $live_base_url.$profile_pic_url; config('constants.SERVER_ADDRESS').
            $profile_pic_url = config('constants.SERVER_ADDRESS').$profile_pic_url;
        }
        else{
            $profile_pic_url = NULL;
        }
        if ($request->hasFile('identification')) {
            $identification_url = $request->file('identification')->store('public/uploads/identification');
            //$local_base_url = 'http://localhost/seguro_api/storage/app/';
            $live_base_url = 'http://ask.net.ng/seguro/seguro_api/storage/app/';
            //$profile_pic_url = $local_base_url.$identification_url;
            $identification_url = $live_base_url.$identification_url;

        }
        else{
            $identification_url = NULL;
        }
        $write_operation = DB::table('users')->insert([
                                                        'title' => $title,
                                                        'surname' => $surname,
                                                        'othernames' => $othernames,
                                                        'fullname' => $fullname,
                                                        'gender' => $gender,
                                                        //'dob' => $dob,
                                                        'date_of_birth' => $date_of_birth,
                                                        'marital_status' => $marital_status,
                                                        'state_of_origin' => $state_of_origin,
                                                        'lga' => $lga,
                                                        'nationality' => $nationality,
                                                        'residential_address' => $residential_address,
                                                        'postal_address' => $postal_address,
                                                        'email' => $email,
                                                        'telephone_no' => $telephone_no,
                                                        'telephone_no_1' => $telephone_no_1,
                                                        'profile_pic_url' => $profile_pic_url,
                                                        'identification_url' => $identification_url,
                                                        'status' => strtolower($approval_status),
                                                        'application_no' => $application_no,
                                                        'referral_no' => $referral_no,
                                                        'created_at' => $created_at,
                                                        'updated_at' => $created_at]);
        
        $payment_plan_id = $request->payment_plan_id;
        
        $employment_status = $request->employment_status;
        $name_of_employer = $request->name_of_employer;
        $designation = $request->designation;
        $address = $request->address;
        
        
        $user_id= DB::table('users')->max('id');
        $write_operation_2 = DB::table('user_payment_plan')->insert([
                                                                'user_id' => $user_id,
                                                                'payment_plan_id' => $payment_plan_id,
                                                                'created_at' => $created_at,
                                                                'updated_at' => $created_at
                                                                ]);
        $write_operation_3 = DB::table('employment_information')->insert([
                                                                    'user_id' => $user_id,
                                                                    'employment_status' => strtolower($employment_status),
                                                                    'name_of_employer' => $name_of_employer,
                                                                    'designation' => $designation,
                                                                    'address' => $address,
                                                                    'created_at' => $created_at,
                                                                    'updated_at' => $created_at]);
        $write_operation_4 = DB::table('role_user')->insert(['user_id' => $user_id,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);

        $write_operation_5 = DB::table('next_of_kin_details')->insert([ 
                                                                        'kin_name' => $kin_name,
                                                                        'relationship' => $relationship,
                                                                        'kin_telephone_no' => $kin_telephone_no,
                                                                        'kin_address' => $kin_address,
                                                                        'user_id' => $user_id,
                                                                        'created_at' => $created_at,
                                                                        'updated_at' => $created_at]);
        //,'payment_plan_id' => $payment_plan_id
        $status = "failed";
        $comment = "Account creation failed.";
        if ($write_operation && $write_operation_2 && $write_operation_3 && $write_operation_4 && $write_operation_5) {
            $status = "successful";
            $comment = "Account creation successful.";
        }
        return response()->json([
                'status' => $status,
                'comment' => $comment,
                'approval_status' => $approval_status
            ]);
    }
  
    public function dashboard(){
        /*
            reads the db and returns the ffg:
            users current payment plan
            the number of ponths paid for
            total amount saved 
            properties available
            Testing not complete
        */

        //$user_id = 6;  //hardcoded for now later I'll change it find the id of the logged in user
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $user_payment_plan = 1;
        $amount_saved = 20;
        $available_properties = 30; // IMPLEMENT CHECKING IF TABLES EXIST BEFORE RUNNING QUERY
        
        $user_payment_plan_id = DB::table('user_payment_plan')->where('id','=',$user_id)->value('payment_plan_id');
        $user_payment_plan = DB::table('payment_plans')->where('id','=',$user_payment_plan_id)->value('payment_plan');
        $amount_saved = DB::table('transactions')->where([['user_id','=',$user_id],['debit_credit','=','credit']])->sum('amount');
        $no_of_months_paid_for = DB::table('transactions')->where([['user_id','=',$user_id],['debit_credit','=','credit']])->count();   // a new roqw reps a month being paid for. I'll provide a constraint in the admin portal API.
        $username = DB::table('users')->where('id','=',$user_id)->value('username');
        //brb

        // modifying property details retrieval
        $ids = DB::table('properties')->where('status','=',"available")->orderBy('created_at', 'desc')->pluck('id');

        $properties = array();

        foreach ($ids as $key => $id) {
            # code...
            $property = DB::table('properties')->where('id','=',$id)->first();
            $property_pic = DB::table('property_pictures')->where('property_id','=',$id)->value('property_pic_url');
            $property_pics = DB::table('property_pictures')->where('property_id','=',$id)->get();
            $properties['id'] = $property->id;
            $properties['name'] = $property->name;
            $properties['address'] = $property->address;
            $properties['coordinates'] = $property->coordinates;
            $properties['price'] = $property->price;
            $properties['area'] = $property->area;
            $properties['no_of_bedrooms'] = $property->no_of_bedrooms;
            $properties['description'] = $property->description;
            $properties['time_of_offer'] = $property->time_of_offer;
            $properties['status'] = $property->status;
            $properties['property_pic_url'] = $property_pic;
            $properties['property_pics'] = $property_pics;
            $properties['properties'][] = $properties;

        }
        /*
        $available_properties = DB::table('properties')->where('status','=',"available")->get();
        $available_properties_id = DB::table('properties')->where('status','=',"available")->pluck('id');
        foreach ($available_properties_id as $key => $id) {
            # code...
            $available_properties_pictures[] = DB::table('property_pictures')->where('property_id','=',$id)->first();
        }*/
        $profile_pic_url = DB::table('users')->where('id','=',$user_id)->value('profile_pic_url');
        $interest_rate_id = DB::table('interest_rate')->max('id');
        $interest_rate = DB::table('interest_rate')->where('id','=',$interest_rate_id)->value('interest_rate');
        
        return response()->json([
            'interest_rate' => $interest_rate,
            'current_payment_plan' => $user_payment_plan,
            'no_of_months_paid_for' => $no_of_months_paid_for,
            'total_amount_saved' => $amount_saved,
            'available_properties' => $properties,
            'username' => $username,
            'profile_pic_url' => $profile_pic_url
        ]);
    	
    }
    public function indicate_interest(Request $request){
        /*For users who indicate interest in a specific property
            Checks details and records in the interested_users table
            Add a status column to interested users stating if he/she's interest led to a purchase ???
        */
        
        $property_id = $request->property_id;
        //$user_id = 6; // hardcoded will be automated later
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $write_op = false;
        $created_at = date('Y-m-d h:i:s ', time());
        $updated_at = date('Y-m-d h:i:s ', time());
        $write_op = DB::table('interested_users')->insert([
                                                            'user_id' => $user_id,
                                                            'property_id' => $property_id,
                                                            'created_at' => $created_at,
                                                            'updated_at' => $created_at]);
        $status = "failed";
        if ($write_op) {
            $status = "successful";
            $comment = "Your interest in this property has been noted. The admin will take action ASAP.";
        }            
        return response()->json([
            'status' => $status,
            'user_id' => $user_id,
            'property_id' => $property_id,
            'comment' => $comment]);
    	//return $property_id; 
    }
    public function upgrade(Request $request){
        /*Sends request to the admin for payment plan upgrade*/
        $new_payment_plan_id = $request->new_payment_plan_id;
        //$user_id = 6; //hradcoded
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $upgrade_status = 'processing';
        $created_at = date('Y-m-d h:i:s ', time());
        $write_op = DB::table('upgrade_requests')->insert(['user_id' => $user_id,
                                                           'new_payment_plan_id' => $new_payment_plan_id,
                                                           'status' => $upgrade_status,
                                                           'created_at' => $created_at,
                                                           'updated_at' => $created_at]);
        if ($write_op) {
            # code...
            $status = 'successful';
            $comment = 'Your payment plan upgrade request has been recorded. Rest assured our admin will work on it ASAP';
        }
        return response()->json([
                                    'status' => $status,
                                    'user_id' => $user_id,
                                    'new_payment_plan_id' => $new_payment_plan_id,
                                    'upgrade_status' => $upgrade_status,
                                    'comment' => $comment]);
    	
    }
    public function history(){
        /*
            Gets users payment details from transactions database
        */
        //$user_id = 6;
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $payment_history = DB::table('transactions')->where([['user_id','=',$user_id],['debit_credit','=','credit']])->get();
    	return response()->json([
                                'status' => 'successful',
                                'user_id' => $user_id,
                                'payment_history' => $payment_history]);
    }
    public function history_interval(Request $request){
        //parse range input
        $interval = $request->interval;
        $parsed_interval = explode(' - ',$interval);
        $beginning = Date_format(Date_create($parsed_interval[0]." 00:00:00"), "Y-m-d");
        $end = Date_format(Date_create($parsed_interval[1]." 23:59:59"), "Y-m-d");
        return array($beginning,$end);
        /*
        //search db based on range 
        $payment_history_in_interval = DB::table('transactions')->whereBetween('created_at',[$beginning,$end])
                                                                ->get();

        if (count($payment_history_in_interval)>0) {
            $status = 'Successful.';
            $comment = 'History within the required interval found';
        }
        else{
            $status = 'Failed';
            $comment = 'Nothing found';
        }
        //return results
        return response()->json([
                                    'status' => $status,
                                    'comment' => $comment,
                                    'payment_history_in_interval' => $payment_history_in_interval]);*/

    }
    public function interest(){
        /*
            calculates interest on users previous month payment
        */
        /*$user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $annual_interest_rate = 0;
        $principal = 0;
        $compounding_freq = 12; // interest is added monthly
        $principal = DB::table('savings')->where('user_id','=',$user_id)->value('principal');
        $principal_prime = $principal*(1 + $annual_interest_rate/($compounding_freq*1.0));
        $month_paid_for = date('M');
        $write_op = DB::table('savings');
    	return $principal_prime;*/
    }
    public function approved(){
        /**
            Notifies a user if he/she has been approved
        */
        //$user_id = 6;
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $approval_status = "approved";
        $approval_status = DB::table('users')->where('id','=',$user_id)->value('status');
        if ($approval_status != 'approved') {
            return;
        }
        return response()->json([
                                'approval_status' => $approval_status]);
    	
    }
    public function feedback(Request $request){
     /*
        Handles feedback from users
     */
        //$user_id = 6; //hardcoded
        $user = JWTAuth::parseToken()->authenticate();
        $user_id = $user->id;
        $status = 'failed';
        $subject = $request->subject;
        $message = $request->message;
        $created_at = date('Y-m-d h:i:s ', time());
        $updated_at = date('Y-m-d h:i:s ', time());
        $write_op = DB::table('feedback')->insert(['user_id' => $user_id,
                                                   'subject' => $subject,
                                                   'message' => $message,
                                                   'created_at' => $created_at,
                                                   'updated_at' => $created_at]);
        if ($write_op) {
            $status = 'successful';
        }
        return response()->json(['status' => $status]);
    }
    public function test(){
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(['user' => $user]);
    }
    public function viewPropertyPics($id){
        $property = DB::table('properties')->where('id', $id)->first();
        $property_pics = DB::table('property_pictures')->where('property_id', $id)->get();
        $property->property_pics = $property_pics;
        $eResponse = array();
        if($property_pics == []) {
            $status = 'failed';
            $comment = "No pictures to show";
        }
        else {
            $status = 'successful';
            $comment = "Pictures Successful Loaded";
        }
        return response()->json([
            'status' => $status,
            'comment' =>$comment,
            'property'=>$property]);

    }
}
